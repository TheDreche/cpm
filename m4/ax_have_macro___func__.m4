# AX_CXX_HAVE_MACRO___func__
# -------------------------------
AC_DEFUN([AX_CXX_HAVE_MACRO___func__],
[AC_CACHE_CHECK([whether the macro __func__ is availible], ax_cv_cxx_have_macro___func__,
[AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],
[[
const char* some_function_name() {
	return __func__;
}

int main() {
	(void) some_function_name();
}
]])],
		   [ax_cv_cxx_have_macro___func__=no],
		   [ax_cv_cxx_have_macro___func__=yes])])
if test $ax_cv_cxx_have_macro___func__ = yes; then
  AC_DEFINE(HAVE_MACRO___func__, 1,
	    [Define to 1 if __func__ is availible])
fi
])# AX_CXX_HAVE_MACRO___func__

