#!/bin/sh

for f in $srcdir/tests/cfg_filetypes/*.cfg; do
	echo ">>> Trying to parse $f ..."
	if ! ./built/cpm-conf -f "$f"; then
		exit 1
	fi
done

exit 0
