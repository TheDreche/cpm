ACLOCAL_AMFLAGS = -I m4

EXTRA_DIST = \
	README.md

cpmpkgincludedir = $(pkgincludedir)/cpm

SUBDIRS = po
AM_CPPFLAGS = '-I$(top_builddir)' '-I$(top_srcdir)' '-I$(top_srcdir)/include'

bin_PROGRAMS = built/cpm built/cpm-sh built/cpm-conf

built_cpm_SOURCES = include/gettext.h src/cpm/main.cpp
built_cpm_LDADD = built/libcpm.la

built_cpm_sh_CPPFLAGS = $(AM_CPPFLAGS) '-DLOCALEDIR="$(localedir)"'
built_cpm_sh_CFLAGS = $(READLINE_CFLAGS) $(NCURSES_CFLAGS)
built_cpm_sh_LDADD = built/libcpm.la $(READLINE_LIBS) $(NCURSES_LIBS) @LIBINTL@
built_cpm_sh_SOURCES = \
	include/config.hpp \
	src/cpm-sh/main.cpp

built_cpm_conf_CPPFLAGS = $(AM_CPPFLAGS) '-DLOCALEDIR="$(localedir)"'
built_cpm_conf_CFLAGS = $(READLINE_CFLAGS) $(NCURSES_CFLAGS)
built_cpm_conf_LDADD = built/libcpm.la $(READLINE_LIBS) $(NCURSES_LIBS) @LIBINTL@
built_cpm_conf_SOURCES = \
	include/config.hpp \
	include/cpm.hpp \
	include/cpm/flags.hpp \
	include/cpm/logging_message.hpp \
	include/cpm/pkgnode.hpp \
	include/cpm/logging_level.hpp \
	include/cpm/file_parser_correctness.hpp \
	include/cpm/backtrace.hpp \
	include/cpm/file_parser.hpp \
	include/cpm/matchable_expression.hpp \
	include/cpm/importance.hpp \
	include/cpm/name.hpp \
	include/cpm/package.hpp \
	include/cpm/upstream_object.hpp \
	include/cpm/abstract_object.hpp \
	include/cpm/configuration.hpp \
	include/cpm/context.hpp \
	include/cpm/pass_log.hpp \
	include/cpm/version.hpp \
	include/cpm/abstract.hpp \
	include/cpm/repository.hpp \
	include/cpm/log.hpp \
	include/cpm/errors.hpp \
	include/cpm/logging_levels.hpp \
	include/cpm/export.hpp \
	include/cpm/standardized.hpp \
	include/cpm/callback.hpp \
	include/cpm/matchable_expression_any.hpp \
	src/cpm-conf/cmdlinetexts.cpp \
	src/cpm-conf/cmdlinetexts.hpp \
	src/cpm-conf/command.cpp \
	src/cpm-conf/command.hpp \
	src/cpm-conf/command_enum.cpp \
	src/cpm-conf/command_enum.hpp \
	src/cpm-conf/command_file_existing.cpp \
	src/cpm-conf/command_file_existing.hpp \
	src/cpm-conf/command_listable.cpp \
	src/cpm-conf/command_listable.hpp \
	src/cpm-conf/command_log_mask_changes.cpp \
	src/cpm-conf/command_log_mask_changes.hpp \
	src/cpm-conf/command_util.cpp \
	src/cpm-conf/command_util.hpp \
	src/cpm-conf/common.hpp \
	src/cpm-conf/configuration.cpp \
	src/cpm-conf/configuration.hpp \
	src/cpm-conf/configuration_option.cpp \
	src/cpm-conf/configuration_option.hpp \
	src/cpm-conf/errors.hpp \
	src/cpm-conf/getconf.cpp \
	src/cpm-conf/getconf.hpp \
	src/cpm-conf/interactive.cpp \
	src/cpm-conf/interactive.hpp \
	src/cpm-conf/listables.hpp \
	src/cpm-conf/logging.cpp \
	src/cpm-conf/logging.hpp \
	src/cpm-conf/main.cpp \
	src/cpm-conf/passwdentry.cpp \
	src/cpm-conf/passwdentry.hpp \
	src/cpm-conf/tree.hpp \
	src/cpm-conf/userline.cpp \
	src/cpm-conf/userline.hpp \
	src/cpm-conf/util.cpp \
	src/cpm-conf/util.hpp

if WITH_READLINE
built_cpm_conf_SOURCES += \
	src/cpm-conf/completion.cpp \
	src/cpm-conf/completion.hpp
endif

if WITH_NCURSES
built_cpm_conf_SOURCES += \
	src/cpm-conf/addcursesstr.cpp \
	src/cpm-conf/addcursesstr.hpp
endif

lib_LTLIBRARIES = built/libcpm.la

built_libcpm_la_CPPFLAGS = $(CFLAG_VISIBILITY) $(AM_CPPFLAGS) '-I$(top_srcdir)/include/cpm' '-I$(top_srcdir)/src/libcpm' '-DBUILDING_LIBCPM=1'
built_libcpm_la_CFLAGS = $(GMPXX_CFLAGS)
built_libcpm_la_LIBADD = $(GMPXX_LIBS)
built_libcpm_la_SOURCES = \
	include/config.hpp \
	include/cpm.hpp \
	include/cpm/flags.hpp \
	include/cpm/logging_message.hpp \
	include/cpm/pkgnode.hpp \
	include/cpm/logging_level.hpp \
	include/cpm/file_parser_correctness.hpp \
	include/cpm/backtrace.hpp \
	include/cpm/file_parser.hpp \
	include/cpm/matchable_expression.hpp \
	include/cpm/importance.hpp \
	include/cpm/name.hpp \
	include/cpm/package.hpp \
	include/cpm/upstream_object.hpp \
	include/cpm/abstract_object.hpp \
	include/cpm/configuration.hpp \
	include/cpm/context.hpp \
	include/cpm/pass_log.hpp \
	include/cpm/version.hpp \
	include/cpm/abstract.hpp \
	include/cpm/repository.hpp \
	include/cpm/log.hpp \
	include/cpm/logging.hpp \
	include/cpm/errors.hpp \
	include/cpm/logging_levels.hpp \
	include/cpm/export.hpp \
	include/cpm/standardized.hpp \
	include/cpm/callback.hpp \
	include/cpm/matchable_expression_any.hpp \
	src/libcpm/backtrace.cpp \
	src/libcpm/charset.cpp \
	src/libcpm/charset.hpp \
	src/libcpm/error.cpp \
	src/libcpm/file_parser.cpp \
	src/libcpm/importance.cpp \
	src/libcpm/init.cpp \
	src/libcpm/joinstrings.cpp \
	src/libcpm/joinstrings.hpp \
	src/libcpm/logging.cpp \
	src/libcpm/matchable_expression_any.cpp \
	src/libcpm/matchable_expression_exact.cpp \
	src/libcpm/matchable_expression_exact.hpp \
	src/libcpm/name.cpp \
	src/libcpm/new.cpp \
	src/libcpm/new.hpp \
	src/libcpm/package.cpp \
	src/libcpm/parsers/all.cpp \
	src/libcpm/parsers/all.hpp \
	src/libcpm/parsers/ini.cpp \
	src/libcpm/parsers/ini.hpp \
	src/libcpm/pkgnode.cpp \
	src/libcpm/repository.cpp \
	src/libcpm/standards/common_name.cpp \
	src/libcpm/standards/common_name.hpp \
	src/libcpm/standards/common_pkgnode.cpp \
	src/libcpm/standards/common_pkgnode.hpp \
	src/libcpm/standards/quick_name.cpp \
	src/libcpm/standards/quick_name.hpp \
	src/libcpm/standards/semver2.cpp \
	src/libcpm/standards/semver2.hpp \
	src/libcpm/standards.cpp \
	src/libcpm/string.cpp \
	src/libcpm/string.hpp \
	src/libcpm/upstream_object.cpp \
	src/libcpm/util.cpp \
	src/libcpm/util.hpp \
	src/libcpm/version.cpp

if ENABLE_STD_EAPI
built_libcpm_la_SOURCES += \
	src/libcpm/parsers/portage/repos.cpp \
	src/libcpm/parsers/portage/repos.hpp \
	src/libcpm/standards/eapi8/names.cpp \
	src/libcpm/standards/eapi8/names.hpp \
	src/libcpm/standards/eapi8/pkgnodes.cpp \
	src/libcpm/standards/eapi8/pkgnodes.hpp \
	src/libcpm/standards/eapi8/versions.cpp \
	src/libcpm/standards/eapi8/versions.hpp
endif

pkgconfig_DATA = pkgconfig/libcpm.pc
pkginclude_HEADERS = include/cpm.hpp
cpmpkginclude_HEADERS = \
	include/cpm/flags.hpp \
	include/cpm/logging_message.hpp \
	include/cpm/pkgnode.hpp \
	include/cpm/logging_level.hpp \
	include/cpm/file_parser_correctness.hpp \
	include/cpm/backtrace.hpp \
	include/cpm/file_parser.hpp \
	include/cpm/matchable_expression.hpp \
	include/cpm/importance.hpp \
	include/cpm/name.hpp \
	include/cpm/package.hpp \
	include/cpm/upstream_object.hpp \
	include/cpm/abstract_object.hpp \
	include/cpm/configuration.hpp \
	include/cpm/context.hpp \
	include/cpm/pass_log.hpp \
	include/cpm/version.hpp \
	include/cpm/abstract.hpp \
	include/cpm/repository.hpp \
	include/cpm/log.hpp \
	include/cpm/logging.hpp \
	include/cpm/errors.hpp \
	include/cpm/logging_levels.hpp \
	include/cpm/export.hpp \
	include/cpm/standardized.hpp \
	include/cpm/callback.hpp \
	include/cpm/matchable_expression_any.hpp

TESTS = $(dist_check_SCRIPTS)
dist_check_SCRIPTS = \
	tests/cpmconf_help.sh \
	tests/cfg_filetypes/test.sh
EXTRA_DIST += \
	tests/cfg_filetypes/portagerepo.cfg

shell_verbose = $(shell_verbose_@AM_V@)
shell_verbose_ = $(shell_verbose_@AM_DEFAULT_V@)
shell_verbose_0 = @echo "  SH      " $<;

doxygen_timestamp = doc/timestamp
doxygen_config = Doxyfile

doxygen_verbose = $(doxygen_verbose_@AM_V@)
doxygen_verbose_ = $(doxygen_verbose_@AM_DEFAULT_V@)
doxygen_verbose_0 = @echo "  DOXYGEN " $<;

docfiles = $(nodist_man3_MANS) $(nodist_html_DATA) $(nodist__html_search_DATA)

$(docfiles) : $(doxygen_timestamp)
$(doxygen_timestamp) : $(doxygen_config) $(built_libcpm_la_SOURCES)
	@-rm -rf doc
if ENABLE_DOC_ANY_AM
	$(doxygen_verbose)$(DOXYGEN) '$(doxygen_config)'
endif
	@echo timestamp > '$(doxygen_timestamp)'

.PHONY : doc.mk-update

doc.mk-update : $(top_srcdir)/scripts/diram.sh $(doxygen_timestamp)
	$(shell_verbose) $(SHELL) 'scripts/diram.sh' FOLDER doc/html AT doc/html IF ENABLE_DOC_HTML_AM AS DATA TO html ',' FOLDER doc/man/man3 AT doc/man/man3 IF ENABLE_DOC_MAN_AM AS MANS TO man3 INTO doc.mk

CLEANFILES = $(doxygen_timestamp)
include doc.mk
