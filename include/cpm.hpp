/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_HPP
#define CPM_HPP

/*!
 * @file
 * @brief Includes the most commonly used parts of the puplic API of libcpm.
 */

/*!
 * @brief Contains all libcpm related classes.
 */
namespace cpm {}

#include "cpm/abstract.hpp"
#include "cpm/abstract_object.hpp"
#include "cpm/backtrace.hpp"
#include "cpm/callback.hpp"
#include "cpm/configuration.hpp"
#include "cpm/context.hpp"
#include "cpm/errors.hpp"
#include "cpm/file_parser_correctness.hpp"
#include "cpm/file_parser.hpp"
#include "cpm/flags.hpp"
#include "cpm/importance.hpp"
#include "cpm/logging_level.hpp"
#include "cpm/logging_levels.hpp"
#include "cpm/logging_message.hpp"
#include "cpm/log.hpp"
#include "cpm/matchable_expression_any.hpp"
#include "cpm/matchable_expression.hpp"
#include "cpm/name.hpp"
#include "cpm/package.hpp"
#include "cpm/pass_log.hpp"
#include "cpm/pkgnode.hpp"
#include "cpm/repository.hpp"
#include "cpm/standardized.hpp"
#include "cpm/upstream_object.hpp"
#include "cpm/version.hpp"

#endif
