/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_VERSION_HPP
#define CPM_VERSION_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::version class.
 */

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT version;
}

#include "standardized.hpp"

namespace cpm {
	/*!
	 * @brief A version.
	 * 
	 * @sa cpm::standardized
	 */
	class version : public standardized {
		public:
			virtual version* clone() const = 0;
			
			/*!
			 * @brief A destructor doing nothing.
			 * 
			 * @sa cpm::standardized::~standardized()
			 */
			virtual ~version();
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * This function is for determining whether a package should be upgraded.
			 * 
			 * In compairson to determining the most recent version, it may be that an older version will compile to the same code and just has new settings and similar. In this case, this function should return false.
			 * 
			 * Returns false if the standards don't match.
			 * 
			 * @param o The version this version should be compaired to.
			 * @return false if this version should be upgraded to o or the other way round, else true.
			 * 
			 * @sa cpm::version::operator!=(const version& o) const
			 * @sa cpm::version::operator<(const version& o) const
			 * @sa cpm::version::operator>(const version& o) const
			 * @sa cpm::version::operator<=(const version& o) const
			 * @sa cpm::version::operator>=(const version& o) const
			 */
			virtual bool operator==(const version& o) const = 0;
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * This function is for determining whether a package should be upgraded.
			 * 
			 * In compairson to determining the most recent version, it may be that an older version will compile to the same code and just has new settings and similar. In this case, this function should return false.
			 * 
			 * Returns true if the standards don't match.
			 * 
			 * Internally, it is treated as !(*this == o).
			 * 
			 * @param o The version this version should be compaired to.
			 * @return true if this version should be upgraded to o or the other way round, else false.
			 * 
			 * @sa cpm::version::operator==(const version& o) const
			 * @sa cpm::version::operator<(const version& o) const
			 * @sa cpm::version::operator>(const version& o) const
			 * @sa cpm::version::operator<=(const version& o) const
			 * @sa cpm::version::operator>=(const version& o) const
			 */
			bool operator!=(const version& o) const;
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * This function is for determining the most recent version.
			 * 
			 * Returns false if the standards don't match.
			 * 
			 * @param o The version this version should be compaired to.
			 * @return true if this version is older than the other version.
			 * 
			 * @sa cpm::version::operator==(const version& o) const
			 * @sa cpm::version::operator!=(const version& o) const
			 * @sa cpm::version::operator>(const version& o) const
			 * @sa cpm::version::operator<=(const version& o) const
			 * @sa cpm::version::operator>=(const version& o) const
			 */
			virtual bool operator<(const version& o) const = 0;
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * Internally returns (*this < o) || (*this == o)
			 * 
			 * Returns false if the standards don't match.
			 * 
			 * @param o the version to compare this version to.
			 * @return false if this version should be upgraded to o, else true.
			 * 
			 * @sa cpm::version::operator==(const version& o) const
			 * @sa cpm::version::operator!=(const version& o) const
			 * @sa cpm::version::operator<(const version& o) const
			 * @sa cpm::version::operator>(const version& o) const
			 * @sa cpm::version::operator>=(const version& o) const
			 */
			bool operator<=(const version& o) const;
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * This function is for determining the most recent version.
			 * 
			 * Returns false if the standards don't match.
			 * 
			 * @param o The version this version should be compaired to.
			 * @return true if this version is newer than the other version.
			 * 
			 * @sa cpm::version::operator==(const version& o) const
			 * @sa cpm::version::operator!=(const version& o) const
			 * @sa cpm::version::operator<(const version& o) const
			 * @sa cpm::version::operator<=(const version& o) const
			 * @sa cpm::version::operator>=(const version& o) const
			 */
			virtual bool operator>(const version& o) const = 0;
			
			/*!
			 * @brief Compares two versions.
			 * 
			 * Internally returns (*this > o) || (*this == o)
			 * 
			 * Returns false if the standards don't match.
			 * 
			 * Although this method seems pointless to me, I'll keep it, as I don't know whether anyone will find a use case for it.
			 * 
			 * @param o the version to compare this version to.
			 * @return (*this > o) || (*this == o); as above.
			 * 
			 * @sa cpm::version::operator==(const version& o) const
			 * @sa cpm::version::operator!=(const version& o) const
			 * @sa cpm::version::operator<(const version& o) const
			 * @sa cpm::version::operator>(const version& o) const
			 * @sa cpm::version::operator<=(const version& o) const
			 */
			bool operator>=(const version& o) const;
	};
}

#endif
