/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_NAME_HPP
#define CPM_NAME_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::name class.
 */

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT name;
}

#include "standardized.hpp"

namespace cpm {
	/*!
	 * @brief A name of anything.
	 * 
	 * It is @ref standardized.
	 * 
	 * @sa cpm::standard
	 * @sa cpm::name_standard
	 * @sa cpm::standardized
	 */
	class name : public standardized {
		public:
			virtual name* clone() const = 0;
			
			/*!
			 * @brief A destructor doing nothing.
			 * 
			 * @sa cpm::standardized::~standardized()
			 */
			virtual ~name();
			
			/*!
			 * @brief Compares whether two names are the same.
			 * 
			 * @param o The name to compare to.
			 * 
			 * @return Whether both are the same.
			 */
			virtual bool operator==(const name& o) const = 0;
			
			/*!
			 * @brief Compares whether two names aren't the same.
			 * 
			 * @param o The name to compare to.
			 * 
			 * @return Whether both aren't the same.
			 */
			bool operator!=(const name& o) const;
			
			/*!
			 * @brief Converts a name to a c style string.
			 * 
			 * The result has to be free-ed.
			 * 
			 * @return A char-Array containing the name. Has to be free-ed.
			 * 
			 * @sa cpm::name::toCharArray() const
			 */
			operator char*() const;
			
			/*!
			 * @brief Converts a name to a c style string.
			 * 
			 * The result has to be free-ed.
			 * 
			 * @return A char-Array containing the name. Has to be free-ed.
			 * 
			 * @sa cpm::name::toString() const
			 * @sa cpm::name::operator char*() const
			 */
			virtual char* toCharArray() const = 0;
	};
}

#endif
