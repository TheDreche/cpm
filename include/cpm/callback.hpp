/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_CALLBACK_HPP
#define CPM_CALLBACK_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::callback class.
 */

#include <functional>

#include "export.hpp"

namespace cpm {
	template<class output, class... inputs>
	class LIBCPM_EXPORT callback;
}

namespace cpm {
	/*!
	 * @brief A callback function.
	 * 
	 * This is a class for a callback function. It should support all kinds of them.
	 * 
	 * @tparam output The return type.
	 * @tparam inputs The input parameters given.
	 */
	template<class output, class... inputs>
	class callback {
		private:
			/*!
			 * @brief Callback for std::function.
			 */
			static output func_stdfunction(void* stdfunction, inputs&... args) {
				return (*((const std::function<output(inputs...)>*) stdfunction)) (args...);
			}
		public:
			/*!
			 * @brief The function to be called.
			 * 
			 * @param context @ref cpm::callback::context
			 * 
			 * @sa cpm::callback::context
			 */
			void(*func)(void* context, inputs...);
			
			/*!
			 * @brief The context a function is called in.
			 * 
			 * You can set this to any pointer you like.
			 * It will be passed as the first parametre to @ref func.
			 */
			void* context;
			
			/*!
			 * @brief Call the callback function.
			 * 
			 * Actually, prepends @ref context to the list of parametres.
			 */
			void operator()(inputs&... params) const {
				func(context, params...);
			}
			
			/*!
			 * @brief Convenience function for std::function&.
			 * 
			 * This is a convenience function for setting to a
			 * std::function.
			 * 
			 * The reference given must not be deleted before this gets
			 * deleted!
			 * 
			 * @param f The std::function to set to; Mustn't be deleted.
			 * @return *this
			 */
			callback<output, inputs...>& operator=(const std::function<output(inputs...)>& f) {
				context = (void*) &f;
				func = func_stdfunction;
				return *this;
			}
	};
}

#endif
