/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_REPOSITORY_HPP
#define CPM_REPOSITORY_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::repository class.
 */

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT repository;
}

#include "context.hpp"
#include "pkgnode.hpp"

namespace cpm {
	/*!
	 * @brief Repository.
	 * 
	 * A repository is a collection of packages provided by a provider.
	 * 
	 * A provider is a grouop of people or just a single person owning
	 * a repository. Some providers own multiple repositories.
	 * 
	 * Sometimes it is also used for grouping packages. This is
	 * especially true for package managers not supporting categories
	 * or similar grouping mechanisms, like apt or rpm. Actually,
	 * there are less package managers supporting such a feature than
	 * not supporting such a feature, I think.
	 * 
	 * @sa cpm::pkgnode
	 */
	class repository : public pkgnode {
		protected:
			/*!
			 * @brief The context this is in.
			 */
			context* ctx;
		public:
			virtual repository* clone() const = 0;
			virtual ~repository();
			
			virtual const pkgnode* getParent() const;
	};
}

#endif
