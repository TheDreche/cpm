/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_BACKTRACE_HPP
#define CPM_BACKTRACE_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::backtrace class.
 */

#include <stddef.h>

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT backtrace;
}

namespace cpm {
	/*!
	 * @brief A backtrace.
	 */
	class backtrace {
		public:
			/*!
			 * @brief A backtrace iterator.
			 * 
			 * It can just iterate from deep to top calls.
			 */
			class LIBCPM_EXPORT const_iterator {
				private:
					/*!
					 * @brief Where the iterator is.
					 * 
					 * NULL if past the end.
					 */
					const backtrace* at = NULL;
					
				public:
					/*!
					 * @brief Create a past end iterator over a backtrace.
					 */
					const_iterator();
					
					/*!
					 * @brief Create an iterator iterating over a backtrace.
					 * 
					 * @param over The backtrace to iterate over.
					 */
					const_iterator(const backtrace& over);
					
					/*!
					 * @brief Go to the function that called the current position.
					 * 
					 * @return *this
					 */
					const_iterator& operator++();
					
					/*!
					 * @brief Go to the function that called the current position.
					 * 
					 * @return A copy of this before the change.
					 */
					const_iterator operator++(int);
					
					/*!
					 * @brief Get the @ref backtrace this is at.
					 */
					const backtrace& operator*();
					
					/*!
					 * @brief Access a member of the @ref backtrace this is at.
					 */
					const backtrace* operator->();
			};
			
			/*!
			 * @brief The file the function call is in.
			 */
			const char* file;
			
			/*!
			 * @brief The line in which something happened.
			 */
			int line;
			
			/*!
			 * @brief The function name this is in, if availible.
			 * 
			 * NULL if not availible.
			 */
			const char* function = NULL;
			
			/*!
			 * @brief Next level.
			 * 
			 * If the backtrace looks like this:
			 * 
			 * 	in line 123 in file.cpp: ...
			 * 	in line 455 in another.cpp: ...
			 * 
			 * and this is the first line, then
			 * this will be the second line.
			 * NULL if this is the last line.
			 */
			const backtrace* called_by = NULL;
			
			/*!
			 * @brief Iterate over this.
			 * 
			 * The specific calls will come first.
			 */
			const_iterator cbegin() const;
			
			/*!
			 * @brief Past the end iterator.
			 */
			const_iterator cend() const;
	};
}

#endif
