/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_STANDARDIZED_HPP
#define CPM_STANDARDIZED_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::standardized class.
 */

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT standardized;
}

#include "abstract_object.hpp"

namespace cpm {
	/*!
	 * @brief A standardized object.
	 * 
	 * A standardized object is any object conforming to a standard.
	 * A standard was a class before the introduction of this new
	 * concept.
	 * 
	 * A standard is identified by a pointer, meaning you can use
	 * any pointer you like. It doesn't even need to be valid! It
	 * is just for comparison. However, it is recommended having
	 * a valid pointer to an object controlled by you.
	 */
	class standardized : public abstract_object {
		public:
			virtual standardized* clone() const = 0;
			
			/*!
			 * @brief An unique identifier for a standard.
			 */
			virtual const void* stdIdentifier() const = 0;
	};
}

#endif
