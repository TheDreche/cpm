/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_FILE_PARSER_CORRECTNESS_HPP
#define CPM_FILE_PARSER_CORRECTNESS_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::file_parser_correctness class.
 */

#include <stdint.h>

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT file_parser_correctness;
}

#include "importance.hpp"

namespace cpm {
	/*!
	 * @brief Saves all information needed for determining the right file parser.
	 */
	class file_parser_correctness {
		public:
			/*!
			 * @brief Whether all errors in the file are recoverable.
			 */
			bool recoverable = true;
			
			/*!
			 * @brief The count of syntax errors in the file.
			 */
			uint8_t syntax_error_count = 0;
			
			/*!
			 * @brief The count of used variables.
			 * 
			 * More is better. Also temporary variables belong here.
			 */
			uint8_t used_variables = 0;
			
			/*!
			 * @brief How likely it is that this is the right parser.
			 * 
			 * If there are multiple parsers having the same count
			 * of syntax errors, this decides which to use.
			 */
			importance overwrite;
			
			/*!
			 * @brief Convenience function for increasing the syntax error count.
			 * 
			 * It also takes care of overflows.
			 * 
			 * If it returns true, it's hopeless and the function should stop trying
			 * to parse the file as there are enough syntax errors to trigger an
			 * overflow.
			 * 
			 * @return Whether stopping parsing the file is better.
			 */
			bool syntax_error();
			
			/*!
			 * @brief Increase used variables count.
			 * 
			 * Also takes care of overflows.
			 */
			void variable_use();
	};
}

#endif
