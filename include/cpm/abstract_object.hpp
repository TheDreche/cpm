/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_ABSTRACT_OBJECT_HPP
#define CPM_ABSTRACT_OBJECT_HPP

#include "export.hpp"

/*!
 * @file
 * @brief Defines the @ref cpm::abstract_object class.
 */

namespace cpm {
	class LIBCPM_EXPORT abstract_object;
}

namespace cpm {
	/*!
	 * @brief An abstract object.
	 * 
	 * Every abstract class should inherit from this because this has the functions
	 * you usually need.
	 */
	class abstract_object {
		public:
			/*!
			 * @brief Copy an object.
			 * 
			 * This is required because you can't otherwise copy an
			 * object from an unknown type.
			 */
			virtual abstract_object* clone() const = 0;
	};
}

#endif
