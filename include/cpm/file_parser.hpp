/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_FILE_PARSER_HPP
#define CPM_FILE_PARSER_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::file_parser class.
 */

#include <stdio.h>

#include <functional>

#include "export.hpp"

namespace cpm {
	template<class result>
	class LIBCPM_EXPORT file_parser;
}

#include "file_parser_correctness.hpp"
#include "pass_log.hpp"

namespace cpm {
	/*!
	 * @brief A file parser.
	 * 
	 * It should be able to parse a specific file type.
	 * 
	 * @tparam result The result type.
	 */
	template<class result>
	class file_parser {
		public:
			/*!
			 * @brief The function for parsing the file.
			 * 
			 * @param stream The file stream to read from. Caller is responsible for opening.
			 * @param r The result to write to.
			 * @param p The passed log.
			 * @return An @ref importance. The result of the parser returning the highest @ref importance will be used.
			 */
			std::function<file_parser_correctness(FILE* stream, result& r, pass_log&& p)> read_function;
			
			/*!
			 * @brief A constructor for setting everything.
			 * 
			 * Currently, just the function has to be set.
			 * 
			 * In the future there may come optional parameters.
			 * 
			 * @param parser The function parsing a file.
			 */
			file_parser(std::function<file_parser_correctness(FILE* stream, result& r, pass_log&& p)> parser) : read_function(parser) {}
			
			/*!
			 * @brief Parse a specific file type.
			 * 
			 * @copydoc read_function
			 */
			file_parser_correctness read(FILE* stream, result& r, pass_log&& p) {
				return read_function(stream, r, std::move(p));
			}
			
			/*!
			 * @brief Parse a specific file type.
			 * 
			 * @copydoc read_function
			 */
			file_parser_correctness operator()(FILE* stream, result& r, pass_log&& p) {
				return read_function(stream, r, std::move(p));
			}
	};
}

#endif
