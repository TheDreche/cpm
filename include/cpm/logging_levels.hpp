/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_LOGGING_LEVELS_HPP
#define CPM_LOGGING_LEVELS_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::logging_levels typedef.
 */

#include "export.hpp"

#include "flags.hpp"
#include "logging_level.hpp"

namespace cpm {
	/*!
	 * @brief A logging mask.
	 * 
	 * A logging mask specifies which logging_level should be saved.
	 * 
	 * @sa cpm::logging_level
	 */
	typedef flags<logging_level, (size_t)logging_level::MAX> logging_levels;
}

#endif
