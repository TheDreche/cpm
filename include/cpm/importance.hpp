/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_IMPORTANCE_HPP
#define CPM_IMPORTANCE_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::importance class.
 */

#include <vector>

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT importance;
}

namespace cpm {
	/*!
	 * @brief An importance useful for letting something being overwritten.
	 * 
	 * It is like a value, but there is always something in between.
	 * 
	 * I may write it in a form such as `upper lower upper upper lower` ...
	 * 
	 * If you want a thing that is more important than another, append upper.
	 * If you want a thing that is less important than another, append lower.
	 * 
	 * In a function which should return an importance, you can create one
	 * in several ways:
	 * 
	 * 	importance a = "upper lower";
	 * 	importance b;
	 * 	b.upper().lower().lower();
	 * 	importance c = "+--+"; // upper lower lower upper
	 * 
	 * Usually, if an importance starts with lower, it should be ignored.
	 */
	class importance {
		public:
			/*!
			 * @brief Creates an empty importance.
			 * 
			 * It is between `lower` and `upper`.
			 */
			importance();
			
			/*!
			 * @brief Converts suffixes to an importance.
			 * 
			 * Although no separator is required, any non-alphabetic
			 * character except `+`, `-`, `0` and `1` will work. Separators
			 * are skipped. They also can appear at the beginning.
			 * 
			 * `+`, `-`, `0` and `1` can replace `upper` and `lower`:
			 * - `+`: `upper`
			 * - `-`: `lower`
			 * - `1`: `upper`
			 * - `0`: `lower`
			 * 
			 * `upper`and `lower` are compared case-sensitive.
			 * 
			 * @param s The string to convert.
			 */
			importance(const char* s);
			
			/*!
			 * @brief Lowers the importance.
			 * 
			 * Appends `lower` to the value.
			 * 
			 * @return A reference to this.
			 */
			importance& lower();
			/*!
			 * @brief Makes the importance more important.
			 * 
			 * Appends `upper` to the value.
			 * 
			 * @return A reference to this.
			 */
			importance& upper();
			
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator<(const importance& o) const;
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator>(const importance& o) const;
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator==(const importance& o) const;
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator!=(const importance& o) const;
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator<=(const importance& o) const;
			/*!
			 * @brief Compares two importances.
			 * 
			 * Higher is more important.
			 */
			bool operator>=(const importance& o) const;
			
			/*!
			 * @brief Makes the importance more important.
			 * 
			 * Appends `upper` to the value.
			 * 
			 * @return A reference to this.
			 * 
			 * @sa cpm::importance::upper
			 */
			importance& operator++(int);
			/*!
			 * @brief Lowers the importance.
			 * 
			 * Appends `lower` to the value.
			 * 
			 * @return A reference to this.
			 * 
			 * @sa cpm::importance::lower
			 */
			importance& operator--(int);
			/*!
			 * @brief Makes the importance more important.
			 * 
			 * Appends `upper` to the value.
			 * 
			 * @return A reference to this.
			 * 
			 * @sa cpm::importance::upper
			 */
			importance& operator++();
			/*!
			 * @brief Lowers the importance.
			 * 
			 * Appends `lower` to the value.
			 * 
			 * @return A reference to this.
			 * 
			 * @sa cpm::importance::lower
			 */
			importance& operator--();
			
			/*!
			 * @brief The list containing the `upper`s and `lower`s.
			 * 
			 * true = upper
			 * false = lower
			 */
			std::vector<bool> suffixes;
	};
}

#endif
