/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_MATCHABLE_EXPRESSION_HPP
#define CPM_MATCHABLE_EXPRESSION_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::matchable_expression class.
 */

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT matchable_expression;
}

namespace cpm {
	/*!
	 * @brief An expression strings can match to.
	 * 
	 * There may be a class for matching regex.
	 * 
	 * All such classes should be derived from this one.
	 */
	class matchable_expression {
		public:
			/*!
			 * @brief Whether a string matches this.
			 * 
			 * @param to The null-terminated string to check whether it matches.
			 * @return Whether to matches this.
			 */
			virtual bool matches(const char* to) const = 0;
			
			/*!
			 * @brief A string showing the rules to match.
			 * 
			 * @return A string clarifying this. NULL on error.
			 */
			virtual char* repr() const = 0;
	};
}

#endif
