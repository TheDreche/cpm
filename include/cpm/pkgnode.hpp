/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_PKGNODE_HPP
#define CPM_PKGNODE_HPP

/*!
 * @file
 * @brief Defines the cpm::pkgnode class.
 */

#include <vector>

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT pkgnode;
}

#include "package.hpp"
#include "pass_log.hpp"
#include "upstream_object.hpp"

namespace cpm {
	/*!
	 * @brief A node in the package tree.
	 * 
	 * The packages are organized in a tree, like this:
	 * 
	 * 	           <root node>
	 * 	          /           \
	 * 	     repo1             repo2
	 * 	    /     \           /     \
	 * 	pkg1       pkg2   cat1       cat2
	 * 	                 /    \     /    \
	 * 	             pkg1    pkg2 pkg3    pkg4
	 * 
	 * - pkg = package
	 * - cat = category (not all standards support this)
	 * - repo = repository
	 * 
	 * In the example above, the repositories and categories are pkgnodes.
	 */
	class pkgnode : public upstream_object {
		public:
			virtual pkgnode* clone() const = 0;
			virtual ~pkgnode();
			
			/*!
			 * @brief Checks whether two pkgnodes are the same.
			 */
			virtual bool operator==(const pkgnode& other) const = 0;
			
			/*!
			 * @brief Checks whether two pkgnodes aren't the same.
			 */
			bool operator!=(const pkgnode& other) const;
			
			/*!
			 * @brief child package nodes (const version).
			 * 
			 * Children of this node in the tree described in @ref pkgnode.
			 * 
			 * @return A std::vector of all child nodes.
			 */
			virtual std::vector<const pkgnode*> getChildren(pass_log&& p) const = 0;
			
			/*!
			 * @brief child package nodes (non-const version).
			 * 
			 * Children of this node in the tree described in @ref pkgnode.
			 * 
			 * @return A std::vector of all child nodes.
			 */
			virtual std::vector<pkgnode*> getChildren(pass_log&& p) = 0;
			
			/*!
			 * @brief packages in this node (const version).
			 * 
			 * Just returns the packages contained directly in this @ref pkgnode, doesn't
			 * search child nodes for more packages.
			 * 
			 * @return A std::vector of the packages contained.
			 */
			virtual std::vector<const package*> getPackages(pass_log&& p) const = 0;
			
			/*!
			 * @brief packages in this node (non-const version).
			 * 
			 * Just returns the packages contained directly in this @ref pkgnode, doesn't
			 * search child nodes for more packages.
			 * 
			 * @return A std::vector of the packages contained.
			 */
			virtual std::vector<package*> getPackages(pass_log&& p) = 0;
			
			/*!
			 * @brief The parent.
			 * 
			 * Should return null if it would be the root node.
			 */
			virtual const pkgnode* getParent() const = 0;
	};
}

#endif
