/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_LOGGING_MESSAGE_HPP
#define CPM_LOGGING_MESSAGE_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::logging_message class.
 */

#include <time.h> // time_t

#include "export.hpp"

namespace cpm {
	/*!
	 * @brief A logging message.
	 * 
	 * It saves the actual message along with some metadata.
	 */
	class LIBCPM_EXPORT logging_message;
}

#include "backtrace.hpp"
#include "logging_level.hpp"

namespace cpm {
	/*!
	 * @brief A logging message.
	 */
	class logging_message {
		public:
			/*!
			 * @brief The message to be logged.
			 */
			const char* message;
			
			/*!
			 * @brief The logging_level of this message.
			 */
			logging_level level;
			
			/*!
			 * @brief The time this was logged.
			 */
			time_t time;
			
			/*!
			 * @brief The backtrace.
			 * 
			 * @sa cpm::backtrace
			 */
			const backtrace& bt;
	};
}

#endif
