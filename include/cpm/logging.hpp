/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_LOGGING_HPP
#define CPM_LOGGING_HPP

/*!
 * @file
 * @brief Implement the @ref cpm::log class.
 * 
 * Also define the macros LOG_INIT, LOG_MESSAGE and LOG_PASS.
 */

#include <stdarg.h>

#include <vector>
#include <utility>
#include <functional>

#include "backtrace.hpp"
#include "context.hpp"
#include "logging_level.hpp"
#include "pass_log.hpp"

namespace cpm {
	class LIBCPM_EXPORT log;
	
	/*!
	 * @brief A logging system.
	 * 
	 * It works by saving a list of the log messages until it gets the ability to log all of them.
	 * 
	 * It automatically extracts the logging function from the @ref cpm::context, if given.
	 */
	class log {
		private:
			/*!
			 * @brief The context doing the logging.
			 */
			const context& ctx;
			
			/*!
			 * @brief The function that called this function.
			 */
			const backtrace called_by;
			
			/*!
			 * @brief Whether this is the top of the libcpm call stack.
			 */
			bool is_top;
			
			/*!
			 * @brief The file this function is defined in.
			 */
			const char* file_name;
			
			/*!
			 * @brief The function name of this function.
			 * 
			 * NULL if not availible.
			 */
			const char* function_name = NULL;
		public:
			/*!
			 * @brief Creates a log for a top-level process.
			 * 
			 * NEVER CALL DIRECTLY! Call function-like macro LOG_INIT instead!
			 * 
			 * Every logging function should have one.
			 */
			log(const context&, const char* function_name, const char* file_name);
			
			/*!
			 * @brief Creates a log for a sub-process.
			 * 
			 * NEVER CALL DIRECTLY! Call function-like macro LOG_INIT instead!
			 * 
			 * Every logging function should have one.
			 */
			log(const pass_log&, const char* function_name, const char* file_name);
			
			/*!
			 * @brief Log a general message.
			 * 
			 * DON'T CALL DIRECTLY! Call function-like macro LOG_MESSAGE instead.
			 * 
			 * @param line The line in which this function is being called.
			 * @param level The logging level.
			 * @param message The format string for the message.
			 */
			void message(int line, logging_level level, const char* message, ...);
	};
	
/*!
 * @def LOG_INIT(passed_log)
 * @brief Initialize a log.
 * 
 * This macro automatically fills in the function name and the file name. The result has to be saved.
 * 
 * @param passed_log Either the passed_log&& passed to your function, or a context& for having an empty backtrace.
 * @return A initialized @ref cpm::log.
 */
#ifdef HAVE_MACRO___func__
#define LOG_INIT(passed_log) cpm::log(passed_log, __func__, __FILE__)
#else
#define LOG_INIT(passed_log) cpm::log(passed_log, NULL, __FILE__)
#endif

/*!
 * @brief Pass a @ref cpm::log to a sub-function.
 * 
 * Automatically inserts the line.
 * 
 * @param private_log The log of your function.
 * @return A @ref cpm::pass_log to pass to another function.
 */
#define LOG_PASS(private_log) cpm::pass_log(private_log, __LINE__)

/*!
 * @brief Log a message.
 * 
 * @param log The @ref cpm::log obtained by @ref LOG_INIT.
 * @param level The @ref cpm::logging_level without `cpm::logging_level::` prefix.
 * @param ... A format string and values to insert.
 */
#define LOG_MESSAGE(log, level, ...) log.message(__LINE__, cpm::logging_level::level, __VA_ARGS__)
}

#endif
