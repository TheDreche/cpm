/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_MATCHABLE_EXPRESSION_ANY_HPP
#define CPM_MATCHABLE_EXPRESSION_ANY_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::matchable_expression_any class.
 */

#include <vector>

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT matchable_expression_any;
}

#include "matchable_expression.hpp"

namespace cpm {
	/*!
	 * @brief A match logical operator.
	 * 
	 * This matches if at least one out of many @ref matchable_expression matches.
	 */
	class matchable_expression_any : public matchable_expression {
		public:
			/*!
			 * @brief The list of expressions.
			 * 
			 * If any of these matches a string, this matches it.
			 */
			std::vector<const matchable_expression*> expressions;
			
			/*!
			 * @brief Add a single @ref matchable_expression.
			 * 
			 * If this matches a new string, this will also match it.
			 * 
			 * @param expr The new expression.
			 * @return *this
			 */
			matchable_expression_any& add(const matchable_expression* expr);
			
			/*!
			 * @brief Adds a bunch of new expressions.
			 * 
			 * Each one will be added to the list.
			 * 
			 * @param expr The @ref matchable_expression_any whose list will be added.
			 * @return *this
			 */
			matchable_expression_any& add(const matchable_expression_any& expr);
			
			bool matches(const char* to) const;
			
			char* repr() const;
	};
}

#endif
