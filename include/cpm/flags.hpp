/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_FLAGS_HPP
#define CPM_FLAGS_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::flags class.
 */

#include <stddef.h>

#include <bitset>

#include "export.hpp"

namespace cpm {
	// A class which I thought was missing, so I made it myself
	template<class _T, size_t max>
	class LIBCPM_EXPORT flags;
}

namespace cpm {
	/*!
	 * @brief System for handling bitwise-or-able enumerations.
	 * 
	 * Note: The following is written as if it wouldn't be in
	 * the namespace cpm. If you want me to move this into a
	 * library for such a purpose (instead of a packaging
	 * library), feel free to report it (e.g. as issue).
	 * 
	 * The first template parametre should be the enum type.
	 * Make sure to give it an unsigned base type.
	 * The second should specify the highest value the enum has.
	 * It may be good to add a last value, like this:
	 * 
	 * 	enum myflags : unsigned char {
	 * 		VAL1,
	 * 		VAL2,
	 * 		LAST=VAL2
	 * 	}
	 * 	// Then you can use this:
	 * 	flags<myflags, myflags::LAST>
	 * 
	 * If you do this, you may prefer `enum class` over `enum`.
	 * 
	 * For generating a flagset out of your enum, do this:
	 * 
	 * 	flags<enumtype>() << enumtype::item
	 * 
	 * For adding a bit, use bitwise or, as usual. This is made
	 * like this to make the following possible (based on operator
	 * precederence):
	 * 
	 * 	flags<enumtype>() << enumtype::item1 | enumtype::item2
	 * 
	 * If you prefer to construct it out of a enumtype value, that
	 * is possible, too:
	 * 
	 * 	flags<enumtype>(enumtype::item)
	 * 
	 * Againg, based upon operator precederence, you can do this:
	 * 
	 * 	(flags<enumtype>) enumtype::item1 | enumtype::item2
	 * 
	 * Since it is based upon operator precederence, _don't do this_:
	 * 
	 * 	// WRONG!
	 * 	(flags<enumtype>) (enumtype::item1 | enumtype::item2)
	 * 
	 * Instead, use one of the above ways.
	 * 
	 * Since it is impossible to overload the bitwise-or operator of
	 * a enum, don't do this:
	 * 
	 * 	void myfunc(flags<myfunc_flags> f) {
	 * 		doSth(f);
	 * 	}
	 * 	int main() {
	 * 		// WRONG!
	 * 		myfunc(myfunc_flags::item1 | myfunc_flags::item2)
	 * 	}
	 * 
	 * It will throw an exception if it can't convert the given number
	 * because it is too high. However, in such a case, it doesn't
	 * need to be too high. Don't rely on it!
	 * 
	 * If you want to make this handy to use, it may be useful to
	 * define a typedef:
	 * 
	 * 	typedef flags<myfunc_flags> a_new_name
	 * 
	 * In case you need a naming scheme, here is one which you can use:
	 * The enum is always singular (without s, e.g. myfunc_flag),
	 * the typedef is always plural (with s, e.g. myfunc_flags).
	 * 
	 * @tparam _T The enum. Has to be convertable to size_t.
	 * @tparam max The maximum value of the enum _T.
	 */
	template<class _T, size_t max>
	class flags {
		private:
			/*!
			 * @brief Saves the actual data.
			 */
			std::bitset<max + 1> content;
		public:
			/*!
			 * @brief Initializes flags to none.
			 * 
			 * No flag will be set.
			 * 
			 * Required for Syntax:
			 * 
			 * 	flags<enumname, maxvalue>() << enumname::val1 | enumname::val2
			 * 	flags<enumname, maxvalue>() << enumname::val1 << enumname::val2
			 */
			flags() {
				content.reset();
			}
			
			/*!
			 * @brief Initializes flags to contain just one value.
			 * 
			 * Just the value given will be set.
			 * 
			 * @param o The value to be set.
			 */
			flags(_T o) {
				content.reset();
				content.set((size_t)o, true);
			}
			
			/*!
			 * @brief In-place add a flag.
			 * 
			 * @param o The flag to get set.
			 * @return *this
			 */
			flags<_T, max>& operator<<=(_T o) {
				content.set((size_t)o, true);
				return *this;
			}
			
			/*!
			 * @brief In-place add a flag.
			 * 
			 * @param o The flag to be set.
			 * @return *this
			 */
			flags<_T, max>& operator|=(_T o) {
				content.set((size_t)o, true);
				return *this;
			}
			
			/*!
			 * @brief In-place merge flags.
			 * 
			 * @param o The flags to be set.
			 * @return *this
			 */
			flags<_T, max>& operator|=(const flags<_T, max>& o) {
				content |= o.content;
				return *this;
			}
			
			/*!
			 * @brief Does the thing expected from the usual ways.
			 * 
			 * Resets all bits except the one given.
			 * 
			 * @param o The flag to check.
			 * @return *this
			 */
			flags<_T, max>& operator&=(_T o) {
				bool a = content.test((size_t)o);
				content.reset();
				if(a) {
					content.set((size_t)o, true);
				}
				return *this;
			}
			
			/*!
			 * @brief Does the thing expected from the usual ways.
			 * 
			 * Resets all bits except the ones in the given flags.
			 * 
			 * @param o The flags to check.
			 * @return *this
			 */
			flags<_T, max>& operator&=(const flags<_T, max>& o) {
				content &= o.content;
				return *this;
			}
			
			/*!
			 * @brief Adds a flag.
			 * 
			 * Required for Syntax:
			 * 
			 * 	flags<enumname, maxvalue>() << enumname::val1 | enumname::val2
			 * 	flags<enumname, maxvalue>() << enumname::val1 << enumname::val2
			 * 
			 * @param o The flag to set.
			 * @return A set of flags additionally containing the given flag.
			 */
			flags<_T, max> operator<<(_T o) const {
				flags<_T, max> r = *this;
				r <<= o;
				return r;
			}
			
			/*!
			 * @brief Adds a flag.
			 * 
			 * Required for Syntax:
			 * 
			 * 	flags<enumname, maxvalue>() << enumname::val1 | enumname::val2
			 * 	flags<enumname, maxvalue>() << enumname::val1 << enumname::val2
			 * 	(flags<enumname, maxvalue>) enumname::val1 | enumname::val2
			 * 
			 * @param o The flag to set.
			 * @return A set of flags additionally containing the given flag.
			 */
			flags<_T, max> operator|(_T o) const {
				flags<_T, max> r = *this;
				r |= o;
				return r;
			}
			
			/*!
			 * @brief Adds flags.
			 * 
			 * This is the expected behaviour if you know the usual way of doing this.
			 * 
			 * Required if you want to combine multiple collections of flags.
			 * 
			 * @param o Flags containing all additional flags.
			 * @return A set of flags containing the flags of this and the given set.
			 */
			flags<_T, max> operator|(const flags<_T, max>& o) const {
				flags<_T, max> r = *this;
				r |= o;
				return r;
			}
			
			/*!
			 * @brief Checks whether a flag is set.
			 * 
			 * Returns a set of flags containing just this flag if it is set, otherwise
			 * containing none.
			 * 
			 * This is the expected behaviour if you know the usual way of doing this.
			 * 
			 * @param o The flag to check.
			 * @return A set of flags just containing the given flag if set.
			 */
			flags<_T, max> operator&(_T o) const {
				flags<_T, max> r = *this;
				r &= o;
				return r;
			}
			
			/*!
			 * @brief Checks whether a flag is set.
			 * 
			 * Returns a set of flags containing just this flag if it is set, otherwise
			 * containing none.
			 * 
			 * This is the expected behaviour if you know the usual way of doing this.
			 * 
			 * @param o The flag to check.
			 * @return A set of flags just containing the given flag if set.
			 */
			flags<_T, max> operator[](_T o) const {
				flags<_T, max> r = *this;
				r &= o;
				return r;
			}
			
			/*!
			 * @brief Checks whether flags are set.
			 * 
			 * Returns a set of flags just containing the flags which are set in both.
			 * 
			 * This is the expected behaviour if you know the usual way of doing this.
			 * 
			 * @param o Flags to check.
			 * @return A set of flags containing the given flags which are set.
			 */
			flags<_T, max> operator&(const flags<_T, max>& o) const {
				flags<_T, max> r = *this;
				r &= o;
				return r;
			}
			
			/*!
			 * @brief True if containing any flag.
			 * 
			 * False otherwise.
			 */
			operator bool() const {
				return content.any();
			}
			
			/*!
			 * @brief Opposite of operator bool.
			 * 
			 * True if no flag is set, false otherwise.
			 */
			bool operator!() const {
				return content.none();
			}
			
			/*!
			 * @brief Checks whether a flag is set.
			 * 
			 * @param f The flag to check.
			 * @return Whether the given flag is set.
			 */
			bool isSet(_T f) const {
				return content[(size_t)f];
			}
			
			/*!
			 * @brief Remove a flag.
			 * 
			 * @param o The flag to remove.
			 * @return A reference to this.
			 */
			flags<_T, max> rm(_T o) {
				content.set((size_t) o, false);
				return *this;
			}
			
			/*!
			 * @brief Checks whether the same flags are set.
			 * 
			 * This is the expected way if you know the usual way of doing this.
			 * 
			 * @return Whether both flag sets contain the same flags.
			 */
			bool operator==(const flags<_T, max>& o) const {
				return content == o.content;
			}
			
			/*!
			 * @brief Checks whether NOT the same flags are set.
			 * 
			 * This is the expected way if you know the usual way.
			 * 
			 * @return Whether both flag sets DON'T contain the same flags.
			 */
			bool operator!=(const flags<_T, max>& o) const {
				return !*this == o;
			}
	};
}

#endif
