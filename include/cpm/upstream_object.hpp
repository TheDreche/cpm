/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_UPSTREAM_OBJECT_HPP
#define CPM_UPSTREAM_OBJECT_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::upstream_object class.
 */

#include <stddef.h>

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT upstream_object;
}

#include "matchable_expression_any.hpp"
#include "name.hpp"
#include "standardized.hpp"

namespace cpm {
	/*!
	 * @brief An upstream object.
	 * 
	 * This is any object availible upstream, meaning in this case in a
	 * repository.
	 * 
	 * It should usually have an upstream name; if it hasn't, noone can
	 * say this upstream object is meant because of missing an identifier.
	 * 
	 * Upstream objects can also have a name preferred by the user used
	 * for showing, for example, which package will be installed.
	 * 
	 * The user may specify a script returning a name suitable, which may
	 * be different every time. It may use which name the user gave to
	 * return the best suitable name. It has to be runnable even if the
	 * user hasn't mentioned this package ever.
	 */
	class upstream_object : public standardized {
		public:
			/*!
			 * @brief The name to print.
			 * 
			 * If the user should be informed about this object,
			 * such as a package being installed, this name should
			 * be shown to the user.
			 * 
			 * This function may return a different name on each
			 * call.
			 * 
			 * The parameter called is the name the user used for
			 * mentioning this object, such as if the user wants
			 * to install a package called `foo`, called should
			 * be `foo`. The return value should be printed.
			 * 
			 * @param called The name the user used for mentioning this. NULL if not mentioned.
			 * @return A name for showing to the user.
			 */
			virtual name* nameUserShow(name* called = NULL) const;
			
			/*!
			 * @brief An expression matching any name.
			 * 
			 * If it should be checked whether the user means this,
			 * the match function of this object should be called.
			 */
			matchable_expression_any nameUserWritten;
			
			/*!
			 * @brief The preferred way to remind the user to his configuration about @ref nameUserShow.
			 * 
			 * The result should be freed by the caller.
			 * 
			 * @return A string to show to remind the user to his configuration.
			 */
			virtual char* nameMatchUserShow() const;
			
			/*!
			 * @brief The name to print to other people.
			 * 
			 * A name identifying this package upstream uniquely.
			 * 
			 * This function may return a different name on each
			 * call.
			 * 
			 * The parameter called is the name upstream used for
			 * mentioning this object, such as if a package wants
			 * to install a package called `foo`, called should
			 * be `foo`. The return value should be printed.
			 * 
			 * @param called The name upstream used for mentioning this. NULL if not mentioned.
			 * @return A name for showing to other people.
			 */
			virtual name* nameUpstreamShow(name* called = NULL) const = 0;
			
			/*!
			 * @brief An expression matching any upstream name.
			 * 
			 * If it should be checked whether upstream means this,
			 * the match function of this object should be called.
			 */
			matchable_expression_any nameUpstreamWritten;
			
			/*!
			 * @brief The preferred way to remind upstream to the configuration about @ref nameUpstreamShow.
			 * 
			 * The result should be freed by the caller.
			 * 
			 * @return A string to show to remind the user to his configuration.
			 */
			virtual char* nameMatchUpstreamShow() const = 0;
	};
}

#endif
