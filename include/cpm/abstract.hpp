/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_ABSTRACT_HPP
#define CPM_ABSTRACT_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::abstract class.
 */

#include <stddef.h>
#include <stdlib.h>

#include "export.hpp"

namespace cpm {
	template<class cls>
	class LIBCPM_EXPORT abstract;
}

namespace cpm {
	/*!
	 * @brief A manager for an abstract class.
	 * 
	 * It automatically copies, allocates, deallocates,
	 * destructs, ... any object of a class inheriting
	 * from @ref cpm::abstract_object.
	 * 
	 * @tparam _cls The abstract class.
	 */
	template<class _cls>
	class abstract {
		public:
			/*!
			 * @brief The abstract class.
			 */
			typedef _cls type;
			
			/*!
			 * @brief The actual object.
			 */
			type* object;
			
			/*!
			 * @brief Destroys the object.
			 */
			void destroy() {
				if(object != NULL) {
					object->~type();
					free(object);
					object = NULL;
				}
			}
			
			/*!
			 * @brief Take control over an object.
			 * 
			 * @param o The object to take control over.
			 */
			abstract(type*&& o) : object(o) {}
			
			/*!
			 * @brief Copy an initialized object.
			 * 
			 * @param o The object to copy.
			 * 
			 * @sa cpm::abstract<_cls>::operator=(const type*)
			 */
			abstract(const type* o) : object(o->clone()) {}
			
			/*!
			 * @brief Copy an abstract object of the same type.
			 * 
			 * @param o The object to copy.
			 * 
			 * @sa cpm::abstract<_cls>::operator=(const cpm::abstract<type>&)
			 */
			abstract(const abstract<type>& o) : object(o.object->clone()) {}
			
			/*!
			 * @brief Move another abstract object into this one.
			 * 
			 * Access can then just be done by using this.
			 * 
			 * @param o The object to move.
			 * 
			 * @sa cpm::abstract<_cls>::operator=(cpm::abstract<type>&&)
			 */
			abstract(abstract<type>&& o) : object(o.object) {
				o.object = NULL;
			}
			
			~abstract() {
				destroy();
			}
			
			/*!
			 * @brief Copy an initialized object.
			 * 
			 * @param o The object to copy.
			 * @return A reference to this.
			 * @sa cpm::abstract<_cls>::abstract(const type*)
			 */
			abstract<type>& operator=(const type* o) {
				destroy();
				object = o->clone();
				return *this;
			}
			
			/*!
			 * @brief Take control over an initialized object.
			 * 
			 * @param o The object to take control over.
			 * @return A reference to this.
			 * @sa cpm::abstract<_cls>::abstract(cpm::abstract<type>&&)
			 */
			abstract<type>& operator=(type*&& o) {
				destroy();
				object = o;
				return *this;
			}
			
			/*!
			 * @brief Copy an abstract object.
			 * 
			 * This will be replaced.
			 * 
			 * @param o The abstract object to copy.
			 * @return A reference to this.
			 * @sa cpm::abstract<_cls>::abstract(const abstract<type>&)
			 */
			abstract<type>& operator=(const abstract<type>& o) {
				destroy();
				object = o.object->clone();
				return *this;
			}
			/*!
			 * @brief Move an abstract object.
			 * 
			 * This will be replaced. The object to move will not be present there afterwards.
			 * 
			 * @param o The object to copy.
			 * @return A reference to this.
			 * @sa cpm::abstract<_cls>::abstract(abstract<type>&&)
			 */
			abstract<type>& operator=(abstract<type>&& o) {
				destroy();
				object = o.object;
				o.object = NULL;
				return *this;
			}
			
			/*!
			 * @brief Convert this to a plain pointer.
			 * 
			 * It will still be taken care of.
			 * 
			 * @return A plain pointer to the stored object.
			 */
			operator type*() {
				return object;
			}
			
			/*!
			 * @brief Convert this to a plain const pointer.
			 * 
			 * It will still be taken care of.
			 * 
			 * @return A plain const pointer to the stored object.
			 */
			operator const type*() const {
				return object;
			}
			
			/*!
			 * @brief Access members.
			 */
			type* operator->() {
				return object;
			}
			
			/*!
			 * @brief Access const members.
			 */
			const type* operator->() const {
				return object;
			}
			
			/*!
			 * @brief Release control of the object.
			 * 
			 * @return A pointer having to be destructed and freed.
			 */
			type* release() {
				type* r = object;
				object = NULL;
				return r;
			}
	};
}

#endif
