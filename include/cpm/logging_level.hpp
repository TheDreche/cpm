/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_LOGGING_LEVEL_HPP
#define CPM_LOGGING_LEVEL_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::logging_level class.
 */

#include <stdint.h>

#include "export.hpp"

namespace cpm {
	/*!
	 * @brief A logging level.
	 * 
	 * A logging level is an importance or type of logging
	 * messages. Each member is a logging level, take them
	 * as an example.
	 * 
	 * The most commonly used logging levels are (in order
	 * of importance):
	 * 1. error: something making the requested action
	 *    impossible happened (for example, if you tried to
	 *    read a file and you can't open it)
	 * 2. warning: something bad happened, but the action
	 *    is still possible. However, this may lead to
	 *    future errors. For example, if you can't read a
	 *    configuration file, it is still possible to
	 *    install a package, except if the repository of
	 *    that package was defined inside it, which will
	 *    run to an error because of a missing package.
	 * 3. (not as common) notice: A unusual scenario,
	 *    but nothing to worry about. This could be, for a
	 *    not too good example, that the `$PATH` doesn't
	 *    contain `/usr/bin`.
	 * 4. info: Just information which may be useful for
	 *    the user that he maybe should read. This could
	 *    be, for example, the requested action.
	 * 5. debug: Debugging information for the developers
	 *    of a project. This could, for example, be a
	 *    note that a value was read out of a
	 *    configuration file.
	 */
	enum class LIBCPM_EXPORT logging_level : int8_t {
		DEBUG, ///< @brief Debugging information.
		INFO, ///< @brief Informations about the things the application tries to do.
		NOTICE, ///< @brief Unusual circumstances, but nothing to worry about.
		WARN, ///< @brief Some circumstances may leading to an error.
		ERROR, ////< @brief An unrecoverable error.
		UPSTREAM_WARN, ///< @brief Invalid file/directory/..., but ignorable
		CONF_COMPLICATED, ///< @brief A complicated configuration.
		CONF_RARE, ///< @brief Rare configuration file syntax.
		CONF_UNLIKELY, ///< @brief Something else would make more sense.
		CONF_WARN, ///< @brief Unreadable line of configuration file.
		CONF_SYNTAX_ERROR, ///< @brief Have to stop because of unrecoverable syntax error (or similar).
		MAX=CONF_SYNTAX_ERROR ///< @brief The maximum value this enum can reach.
	};
}

#endif
