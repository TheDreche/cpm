/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_CONTEXT_HPP
#define CPM_CONTEXT_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::context class.
 */

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT context;
}

#include "callback.hpp"
#include "logging_message.hpp"
#include "pass_log.hpp"

namespace cpm {
	/*!
	 * @brief A context for libcpm.
	 * 
	 * It saves a @ref configuration and some run-time data.
	 */
	class context {
		public:
			/*!
			 * @brief The logging function.
			 * 
			 * It should handle logging operations in libcpm.
			 * 
			 * It gets all the metadata needed as a @ref logging_message.
			 * It is actually a temporary value. For performance reasons
			 * it is allowed to be changed.
			 */
			callback<void, logging_message&> logging_function;
			
			/*!
			 * @brief Convert to a @ref pass_log.
			 * 
			 * It will be the empty backtrace.
			 */
			operator pass_log() const;
	};
}

#endif
