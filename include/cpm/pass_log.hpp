/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_PASS_LOG_HPP
#define CPM_PASS_LOG_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::pass_log class.
 */

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT pass_log;
}

#include "context.hpp"
#include "log.hpp"

namespace cpm {
	/*!
	 * @brief Saves all data for passing a log.
	 * 
	 * It is used to just need one parametre for creating a log.
	 */
	class pass_log {
		public:
			/*!
			 * @brief Save all data needed.
			 * 
			 * NEVER CALL DIRECTLY! Call function-like macro LOG_PASS instead!
			 */
			pass_log(const log&, int line);
			
			/*!
			 * @brief Save all data needed (empty backtrace).
			 * 
			 * NEVER CALL DIRECTLY! Call function-like macro LOG_PASS instead!
			 */
			pass_log(const context&, int line);
			
			/*!
			 * @brief The line the function is called.
			 */
			int line;
			
			/*!
			 * @brief Whether this is an empty backtrace.
			 */
			bool is_top;
			
			union {
				/*!
				 * @brief The function that called this one.
				 * 
				 * Only there if !is_top.
				 */
				const log* l;
				
				/*!
				 * @brief The context.
				 * 
				 * Only there if is_top.
				 */
				const context* ctx;
			};
	};
}

#endif
