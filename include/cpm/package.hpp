/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_PACKAGE_HPP
#define CPM_PACKAGE_HPP

#include "export.hpp"

/*!
 * @file
 * @brief Defines the @ref cpm::package class.
 */

namespace cpm {
	class LIBCPM_EXPORT package;
}

#include "upstream_object.hpp"

namespace cpm {
	/*!
	 * @brief A package.
	 * 
	 * A package usually is software. It could be a program or library,
	 * for example. However, it can be anything that can be installed,
	 * meaning it could also be a icon theme.
	 * 
	 * It is a very common term in package management.
	 */
	class package : public upstream_object {
		public:
			virtual package* clone() const = 0;
			virtual ~package();
	};
}

#endif
