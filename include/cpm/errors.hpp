/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_ERRORS_HPP
#define CPM_ERRORS_HPP

/*!
 * @file
 * @brief Defines the libcpm errors.
 * 
 * @sa cpm::error
 * @sa cpm::error_errno
 * @sa cpm::error_format
 * @sa cpm::error_nostdsupport
 * @sa cpm::error_file_open
 * @sa cpm::error_file_format
 */

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT error;
	class LIBCPM_EXPORT error_errno;
	class LIBCPM_EXPORT error_format;
	class LIBCPM_EXPORT error_nostdsupport;
	class LIBCPM_EXPORT error_file_open;
	class LIBCPM_EXPORT error_file_format;
}

namespace cpm {
	/*!
	 * @brief A general error in libcpm happened.
	 * 
	 * This should not be thrown directly; the right subclass should be used instead.
	 */
	class error {
		public:
			/*!
			 * @brief Human-readable description of the error.
			 */
			virtual const char* what() const noexcept;
	};
	
	/*!
	 * @brief An exception based on errno happened.
	 * 
	 * The reasons are many.
	 */
	class error_errno : public error {
		public:
			/*!
			 * @brief The value errno got.
			 */
			int error_number;
			
			/*!
			 * @brief Default constructor.
			 * 
			 * No error.
			 */
			error_errno();
			
			/*!
			 * @brief Construct out of an errno value.
			 */
			error_errno(int error_number);
			
			const char* what() const noexcept;
	};
	
	/*!
	 * @brief An error occurred at formatting.
	 * 
	 * Maybe the format string is not quite right.
	 */
	class error_format : public error {
		public:
			/*!
			 * @brief Human-readable description of the error.
			 */
			virtual const char* what() const noexcept;
	};
	
	/*!
	 * @brief A standard doesn't support this operation.
	 */
	class error_nostdsupport : public error {
		public:
			/*!
			 * @brief Human-readable description of the error.
			 */
			virtual const char* what() const noexcept;
	};
	
	/*!
	 * @brief Couldn't open a file.
	 */
	class error_file_open : public error {
		public:
			/*!
			 * @brief Human-readable description of the error.
			 */
			virtual const char* what() const noexcept;
	};
	
	/*!
	 * @brief Couldn't detect file format.
	 * 
	 * All parsers were tried and none succeeded.
	 */
	class error_file_format : public error {
		public:
			/*!
			 * @brief Human-readable description of the error.
			 */
			virtual const char* what() const noexcept;
	};
}

#endif
