/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_CONFIGURATION_HPP
#define CPM_CONFIGURATION_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::configuration class.
 */

#include <vector>

#include "export.hpp"

namespace cpm {
	class LIBCPM_EXPORT configuration;
}

#include "abstract.hpp"
#include "context.hpp"
#include "configuration.hpp"
#include "file_parser.hpp"
#include "repository.hpp"

namespace cpm {
	/*!
	 * @brief A configuration for libcpm.
	 * 
	 * It saves all configuration data.
	 */
	class configuration {
		public:
			configuration();
			
			/*!
			 * @brief A list of parsers for configuration files.
			 * 
			 * If a configuration file should be read, this
			 * is a list of functions trying to read that
			 * file.
			 * 
			 * The file is the first parameter, the second
			 * is the @ref configuration to update using
			 * the file.
			 * 
			 * @sa file_parser
			 */
			std::vector<file_parser<configuration>> configuration_file_parsers;
			
			/*!
			 * @brief All repositories.
			 */
			std::vector<abstract<repository>> repositories;
			
			/*!
			 * @brief Merges two configurations.
			 * 
			 * The other configuration has precedance.
			 * 
			 * @param c The libcpm context.
			 * @param o The other configuration.
			 */
			void update(context& c, configuration& o);
			
			/*!
			 * @brief Parse the given configuration file.
			 * 
			 * It searches through configuration_file_parsers, lets
			 * each one try to parse the file and keep just the best.
			 * 
			 * Then, this will be updated using that data.
			 * 
			 * @param ctx The cpm context.
			 * @param path The path of the file to parse
			 */
			void parse(context& ctx, const char* path);
	};
}

#endif
