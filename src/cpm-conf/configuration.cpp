/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "configuration.hpp"

#include "logging.hpp"
#include "util.hpp"

#include "cpm.hpp"

void configuration::update(const configuration &c) {
	prompt.update(c.prompt);
	prompt_question.update(c.prompt_question);
	cpm_context.update(c.cpm_context);
	cpm_configuration.update(c.cpm_configuration);
	self.update(c.self);
	act.update(c.act);
	files += c.files;
	exit.update(c.exit);
	mask.update(c.mask);
	readline_name.update(c.readline_name);
	tree_indent.update(c.tree_indent);
	tree_indent_post_last.update(c.tree_indent_post_last);
	tree_node.update(c.tree_node);
	tree_node_last.update(c.tree_node_last);
}

void configuration::setLog(const char *changes) {
	cpm::logging_levels tmpmask = mask;
	bool add = true;
	bool conf = false;
	bool upstream = false;
	message_str(*this, cpm::logging_level::DEBUG, "Interpreting logging level mask change %s\n", changes);
	for(const char* at = changes; *at != '\0'; ++at) {
		message_str(*this, cpm::logging_level::DEBUG, "conf = %s\n", conf? "true" : "false");
		switch(std::tolower(*at)) {
			case 'c':
				conf = true;
				continue;
			case '+':
				add = true;
				break;
			case '-':
				add = false;
				break;
			case 'd':
				if(conf) {
					message_str(*this, cpm::logging_level::WARN, "Invalid logging level: configuration debug");
				} else if(upstream) {
					message_str(*this, cpm::logging_level::WARN, "Invalid logging level: upstream debug");
				} else {
					if(add) {
						tmpmask |= cpm::logging_level::DEBUG;
					} else {
						tmpmask.rm(cpm::logging_level::DEBUG);
					}
				}
				break;
			case 'i':
				if(conf) {
					message_str(*this, cpm::logging_level::WARN, "Invalid logging level: configuration info");
				} else if(upstream) {
					message_str(*this, cpm::logging_level::WARN, "Invalid logging level: upstream info");
				} else {
					if(add) {
						tmpmask |= cpm::logging_level::INFO;
					} else {
						tmpmask.rm(cpm::logging_level::INFO);
					}
				}
				break;
			case 'n':
				if(conf) {
					message_str(*this, cpm::logging_level::WARN, "Invalid logging level: configuration notice");
				} else if(upstream) {
					message_str(*this, cpm::logging_level::WARN, "Invalid logging level: upstream notice");
				} else {
					if(add) {
						tmpmask |= cpm::logging_level::NOTICE;
					} else {
						tmpmask.rm(cpm::logging_level::NOTICE);
					}
				}
				break;
			case 'w':
				if(conf) {
					if(add) {
						tmpmask |= cpm::logging_level::CONF_WARN;
					} else {
						tmpmask.rm(cpm::logging_level::CONF_WARN);
					}
				} else if(upstream) {
					if(add) {
						tmpmask |= cpm::logging_level::UPSTREAM_WARN;
					} else {
						tmpmask.rm(cpm::logging_level::UPSTREAM_WARN);
					}
				} else {
					if(add) {
						tmpmask |= cpm::logging_level::WARN;
					} else {
						tmpmask.rm(cpm::logging_level::WARN);
					}
				}
				break;
			case 'e':
				if(conf) {
					if(add) {
						tmpmask |= cpm::logging_level::CONF_SYNTAX_ERROR;
					} else {
						tmpmask.rm(cpm::logging_level::CONF_SYNTAX_ERROR);
					}
				} else if(upstream) {
					message_str(*this, cpm::logging_level::WARN, "Invalid logging level: upstream error");
				} else {
					if(add) {
						tmpmask |= cpm::logging_level::ERROR;
					} else {
						tmpmask.rm(cpm::logging_level::ERROR);
					}
				}
				break;
			case 'x':
				if(conf) {
					if(add) {
						tmpmask |= cpm::logging_level::CONF_COMPLICATED;
					} else {
						tmpmask.rm(cpm::logging_level::CONF_COMPLICATED);
					}
				} else if(upstream) {
					message_str(*this, cpm::logging_level::WARN, "Invalid logging level: upstream complicated");
				} else {
					message_str(*this, cpm::logging_level::WARN, "Invalid logging level: complicated");
				}
				break;
			case 'r':
				if(conf) {
					if(add) {
						tmpmask |= cpm::logging_level::CONF_RARE;
					} else {
						tmpmask.rm(cpm::logging_level::CONF_RARE);
					}
				} else if(upstream) {
					message_str(*this, cpm::logging_level::WARN, "Invalid logging level: upstream rare");
				} else {
					message_str(*this, cpm::logging_level::WARN, "Invalid logging level: rare, did you mean notice (n)?");
				}
				break;
			case 'u':
				if(conf) {
					if(add) {
						tmpmask |= cpm::logging_level::CONF_UNLIKELY;
					} else {
						tmpmask.rm(cpm::logging_level::CONF_UNLIKELY);
					}
				} else {
					upstream = true;
					continue;
				}
				break;
			case ' ':
				message_str(*this, cpm::logging_level::WARN, format("Skipping %c\n", *at));
				continue;
		}
		conf = false;
		upstream = false;
	}

	message_str(*this, cpm::logging_level::DEBUG, "Applying new mask\n");
	mask = tmpmask;
}
