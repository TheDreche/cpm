/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits.h>

#include <curses.h>
#include <term.h>

#include <string>

#include "addcursesstr.hpp"

static std::string* to = NULL;

static int putcurschr(int c) {
	if(c < 0 || c > UCHAR_MAX) {
		return EOF;
	}
	to->push_back((unsigned char) c);
	return (unsigned char) c;
}

void addcursesstr(std::string& str, const char* cursesstr) {
	to = &str;
	tputs(cursesstr, 1, &putcurschr);
	to = NULL;
}
