/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string>
#include <unordered_map>
#include <vector>

#include "config.hpp"
#ifdef WITH_READLINE
# include <readline/readline.h>
#endif

#include "command_enum.hpp"
#include "command_util.hpp"
#include "util.hpp"
#include "userline.hpp"

const command* command_enum::getNext(const char* value) const {
	auto pos = values.find(value);
	if(pos == values.end()) {
		return NULL;
	} else {
		return (*pos).second;
	}
}

char* command_enum::ask() const {
	/* |Select repository:
	 * |
	 * |  1. repo1
	 * |  2. repo2
	 * |
	 * |> 1
	 */
	size_t size_size;
	char* formatstring = NULL;
	size_t i;
	std::vector<const std::string*> v;
	bool _ignore;
	char* input_free = NULL;
	char* input;
	size_t input_max_i;
	char* at;
	unsigned long long inNum;
	char* r = NULL;
	v.reserve(values.size());
	
#ifdef WITH_READLINE
	ask_completor = this;
	rl_attempted_completion_function = &ask_completion_function;
	rl_completer_word_break_characters = strdup("");
	if(rl_completer_word_break_characters == NULL) {
		goto error;
	}
	rl_completer_quote_characters = "";
#endif
	
	printf("\n");
	if(ask_question != NULL) {
		printf("%s\n\n", ask_question);
	}
	size_size = 0;
	for(size_t len = values.size(); len > 0; ++size_size) {
		len /= 10;
	}
	formatstring = format("  %%%zuzu. %%s\n", size_size);
	if(formatstring == NULL) {
		goto error;
	}
	i = 0;
	for(const std::pair<const std::string, const command * const>& value : values) {
		v.push_back(&value.first);
		printf(formatstring, i + 1, value.first.c_str());
	}
	free(formatstring);
	formatstring = NULL;
	printf("\n");
	
	input_free = userline("Selection #", _ignore);
	if(input_free == NULL) {
		r = NULL;
		goto error;
	}
	input = input_free;
	input_max_i = strlen(input) - 1;
	while(*input != '\0' && isspace(input[input_max_i])) {
		input[input_max_i--] = '\0';
	}
	while (*input != '\0' && isspace(*input)) {
		++input;
	}
	inNum = strtoull(input, &at, 10);
	if(*at == '\0' && inNum > 0 && inNum <= values.size()) {
		r = strdup(v[inNum - 1]->c_str());
		if(r != NULL) {
			goto error;
		}
	} else {
		if(values.count(input) > 0) {
			size_t input_len = strlen(input);
			memmove(input_free, input, input_len * sizeof(char));
			input_free[input_len] = '\0';
			r = (char*) realloc(input_free, (input_len + 1) * sizeof(char));
			if(r == NULL) {
				goto error;
			}
			input_free = NULL;
		} else {
			r = NULL;
		}
	}
error:
#ifdef WITH_READLINE
	if(rl_completer_word_break_characters != NULL) {
		free(rl_completer_word_break_characters);
		rl_completer_word_break_characters = NULL;
	}
#endif
	if(formatstring != NULL) {
		free(formatstring);
		formatstring = NULL;
	}
	if(rl_completer_word_break_characters != NULL) {
		free(rl_completer_word_break_characters);
		rl_completer_word_break_characters = NULL;
	}
	if(input_free != NULL) {
		free(input_free);
		input_free = NULL;
	}
	if(r != NULL) {
		if(*r == '\0') {
			free(r);
			r = NULL;
		}
	}
	return r;
}

#ifdef WITH_READLINE
char** command_enum::complete(const char* text, size_t cursor_position) const {
	char** possibilities = NULL;
	size_t possibility_count = 0;
	for(const std::pair<const std::string, const command * const>& item : values) {
		if(strncmp(item.first.c_str(), text, cursor_position) == 0) {
			char* possibility = strdup(item.first.c_str());
			if(possibility == NULL) {
				goto error;
			}
			if(add_possibility(possibilities, possibility_count, possibility)) {
				goto error;
			}
		}
	}
	return possibilities;
error:
	if(possibilities != NULL) {
		for(char** p = possibilities; *p != NULL; ++p) {
			free(p);
		}
	}
	return NULL;
}

char** command_enum::ask_complete(const char* text, size_t cursor_position) const {
	rl_completion_suppress_append = 1;
	return complete(text, cursor_position);
}
#endif
