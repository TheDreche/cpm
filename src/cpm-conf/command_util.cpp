/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#ifdef WITH_READLINE
# ifdef HAVE_STDIO_H
#  include <stdio.h>
# endif
# include <readline/readline.h>
#endif

#include "command_util.hpp"

#ifdef WITH_READLINE
const command* ask_completor = NULL;

static char** completions;

static char* fake_generator(const char* text, int state) {
	static size_t pos = 0;
	if(state == 0) {
		pos = 0;
	}
	if(completions == NULL) {
		return NULL;
	} else {
		return completions[pos++];
	}
}

char** ask_completion_function(const char*, int, int) {
	char* line = strdup(rl_line_buffer);
	char** r = NULL;
	if(line == NULL) {
		goto error;
	}
	completions = ask_completor->ask_complete(line, rl_point);
	if(completions == NULL) {
		goto error;
	}
	r = rl_completion_matches(line, &fake_generator);
	free(completions);
	completions = NULL;
error:
	if(line != NULL) {
		free(line);
	}
	if(completions != NULL) {
		for(size_t i = 0; completions[i] != NULL; ++i) {
			free(completions[i]);
		}
		free(r);
	}
	return r;
}

bool add_possibility(char**& to, size_t& count, char* possibility) {
	++count;
	size_t newsize = (count + 1) * sizeof(char*);
	char** tmp = (char**) realloc(to, newsize);
	if(tmp == NULL) {
		return true;
	}
	to = tmp;
	to[count - 1] = possibility;
	to[count] = NULL;
	return false;
}
#endif

char* token_return_skip_whitespace(char* str) {
	if(*str == '\0') {
		return NULL;
	}
	*str++ = '\0';
	while(*str == ' ') {
		++str;
	}
	if(*str == '\0') {
		return NULL;
	} else {
		return str;
	}
}

