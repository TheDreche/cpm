/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#include <stdio.h>
#include <locale.h>

#include <string>
#include <iostream>

#include "gettext.h"
#define _(msgid) gettext(msgid)
#define _n(msgid_s, msgid_p, c) ngettext(msgid_s, msgid_p, c)

#include "configuration.hpp"
#include "logging.hpp"
#include "getconf.hpp"
#include "interactive.hpp"
#include "cmdlinetexts.hpp"

#ifdef WITH_READLINE
# include <readline/readline.h>

void initialize_readline(const configuration& c) {
	rl_readline_name = c.readline_name->c_str();
}
#endif

#ifdef WITH_NCURSES
# include <curses.h>
# include <term.h>
# ifdef HAVE_UNISTD_H
#  include <unistd.h>
# endif

void initialize_ncurses() {
	auto ret = setupterm(NULL, STDOUT_FILENO, NULL);
}
#endif

void localize() {
	setlocale(LC_ALL, "");
	bindtextdomain(PACKAGE, LOCALEDIR);
	textdomain(PACKAGE);
}

int main(int argc, char** argv) {
	try {
		localize();
#ifdef WITH_NCURSES
		initialize_ncurses();
#endif
		configuration c = get_default_conf();
		c.self = argv[0];
		std::function<void(cpm::logging_message&)> cpm_logging_function = [&c](cpm::logging_message& msg){message(c.mask, std::move(msg));};
		c.cpm_context->logging_function = cpm_logging_function;
		message_str(c, cpm::logging_level::DEBUG, "Starting cpm-conf ...\n");
		
		c.update(get_env_conf(c));
		c.update(parse_options(argc, argv));
#ifdef WITH_READLINE
		initialize_readline(c);
#endif
		switch(c.act) {
			case configuration::PRINT_HELP:
				help(c);
				break;
			case configuration::PRINT_VERSION:
				version(c);
				break;
			case configuration::INTERACTIVE:
			case configuration::PARSE:
				if(c.files.size() > 0) {
					for(const std::string& s : c.files) {
						printf(_("Parsing %s\n"), s.c_str());
						c.cpm_configuration->parse(c.cpm_context, s.c_str());
					}
				} else if(c.act == configuration::PARSE) {
					std::cerr << _("No configuration files to parse!") << std::endl;
					return EXIT_FAILURE;
				}
				if(c.act == configuration::PARSE) {
					break;
				}
				interactive(c);
				break;
		}
		
		return EXIT_SUCCESS;
	} catch(const cpm::error& e) {
		std::cerr << e.what() << std::endl;
	} catch(const std::exception& e) {
		std::cerr << e.what() << std::endl;
	}
	return EXIT_FAILURE;
}
