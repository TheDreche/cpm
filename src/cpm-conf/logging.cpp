/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdarg.h>
#include <string.h>

#include "cpm.hpp"

#include "logging.hpp"
#include "util.hpp"

void message(cpm::logging_levels mask, cpm::logging_message&& msg) {
	char suffix[2] = "\0";
	if(msg.message[strlen(msg.message) - 1] != '\n' && mask & cpm::logging_level::WARN) {
		printf("[WARN]   Following not ending in newline:\n");
		suffix[0] = '\n';
	}
	if(!mask.isSet(msg.level)) {
		return;
	}
	switch (msg.level) {
		case cpm::logging_level::DEBUG:
			printf("[DEBUG]  %s%s", msg.message, suffix);
			break;
		case cpm::logging_level::INFO:
			printf("[INFO]   %s%s", msg.message, suffix);
			break;
		case cpm::logging_level::NOTICE:
			printf("[NOTICE] %s%s", msg.message, suffix);
			break;
		case cpm::logging_level::WARN:
			printf("[WARN]   %s%s", msg.message, suffix);
			break;
		case cpm::logging_level::ERROR:
			printf("[ERROR]  %s%s", msg.message, suffix);
			break;
		case cpm::logging_level::UPSTREAM_WARN:
			printf("[UPSTREAM: WARN] %s%s", msg.message, suffix);
			break;
		case cpm::logging_level::CONF_WARN:
			printf("[CONF: WARN]     %s%s", msg.message, suffix);
			break;
		case cpm::logging_level::CONF_SYNTAX_ERROR:
			printf("[CONF: SYNTAX]   %s%s", msg.message, suffix);
			break;
		case cpm::logging_level::CONF_RARE:
			printf("[CONF: RARE]     %s%s", msg.message, suffix);
			break;
		case cpm::logging_level::CONF_UNLIKELY:
			printf("[CONF: UNLIKELY] %s%s", msg.message, suffix);
			break;
		case cpm::logging_level::CONF_COMPLICATED:
			printf("[CONF: COMPLEX]  %s%s", msg.message, suffix);
			break;
		default:
			printf("[Invalid] %s%s", msg.message, suffix);
	}
}

void message_str(configuration& c, cpm::logging_level lvl, const char* format, ...) {
	const static cpm::backtrace backtrace_cpm_conf {
		.file = "some file",
		.line = 0,
		.function = NULL,
		.called_by = NULL,
	};
	
	va_list va_all;
	va_start(va_all, format);
	
	char* msg = vformat(format, va_all);
	va_end(va_all);
	cpm::logging_message m = {
		.message = msg,
		.level = lvl,
		.time = time(NULL),
		.bt = backtrace_cpm_conf
	};
	message(c.mask, std::move(m));
	free(msg);
}
