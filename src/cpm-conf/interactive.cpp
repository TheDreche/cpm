/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#include <stddef.h>
#include <stdio.h>
#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#include <algorithm>
#include <vector>
#include <string>

#include "gettext.h"
#define _(string) gettext(string)
#define _n(string1, string2, n) ngettext(string1, string2, n)
#ifdef WITH_READLINE
# include "readline/readline.h"
#endif

#include "interactive.hpp"
#include "command.hpp"
#include "command_enum.hpp"
#include "command_file_existing.hpp"
#include "command_listable.hpp"
#include "command_log_mask_changes.hpp"
#ifdef WITH_READLINE
# include "completion.hpp"
#endif
#include "configuration.hpp"
#include "listables.hpp"
#include "logging.hpp"
#include "tree.hpp"
#include "userline.hpp"
#include "util.hpp"

static listable getListablesListable(configuration& c) {
	listable r = {
		.liststr = _("List of things to list:\n"),
		.selectstr = _("Select something to list:\n"),
		.items = {
			"repositories"
		},
	};
	return r;
}

static listable getRepositoryListable(configuration& c) {
	listable r = {
		.liststr = _("List of repositories:\n"),
		.selectstr = _("Select a repository:\n")
	};
	for(const cpm::repository* repo : c.cpm_configuration->repositories) {
		cpm::name* repo_name = repo->nameUserShow();
		char* repo_name_str = repo_name->toCharArray();
		r.items.emplace_back(repo_name_str);
		free(repo_name_str);
		delete repo_name;
	}
	return r;
}

void interactive_display_list(configuration& c, const listable& l, bool printListStr = true) {
	printf("\n");
	if(printListStr) {
		printf("%s", l.liststr);
	}
	for(const std::string& i : l.items) {
		printf("  - %s\n", i.c_str());
	}
	printf("\n");
}

namespace interactive_functions {
	void help(configuration& c, char** args) {
		printf(_(
			"List of commands:\n"
			"x exit            Quit %s\n"
			"p parse <file>    Read a file for file independent checks.\n"
			"r read  <file>    Read a file as configuration file.\n"
			"l list  <thing>   List something.\n"
			"t tree            Show the tree of repositories and packages\n"
			"  log   <changes> Change which messages will be displayed.\n"
			"h help            Print this help message.\n"
		), c.self->c_str());
	}
	
	void log(configuration& c, char** args) {
		c.setLog(args[0]);
	}
	
	void list(configuration& c, char** args) {
		enum listable : unsigned char {
			REPOSITORIES
		};
		std::vector<listable> spool;
		const command* current = getCommands(c).getNext("list");
		for(char** read = args; *read != NULL; ++read) {
			if(strcmp(*read, "repositories") == 0) {
				message_str(c, cpm::logging_level::DEBUG, "Adding repositories to things to list ...\n");
				spool.push_back(REPOSITORIES);
			} else {
				printf(_("Unknown thing to list: %s\n"), *read);
			}
		}
		for(listable i : spool) {
			if(i == REPOSITORIES) {
				interactive_display_list(c, getRepositoryListable(c), "repositories");
			}
		}
	}
	
	class pkgtree_node {
		public:
			char* name = NULL;
			cpm::pass_log& p;
			std::vector<pkgtree_node> child_pkgnodes;
			std::vector<pkgtree_node> child_packages;
		private:
			static bool pkgnode_lt(const pkgtree_node& a, const pkgtree_node& b) {
				return strcoll(a.name, b.name) < 0;
			}
		public:
			pkgtree_node(cpm::pass_log& plog, const cpm::package& p) : p(plog) {
				cpm::name* _name = p.nameUserShow();
				name = _name->toCharArray();
				_name->~name();
				free(_name);
				if(name == NULL) {
					throw std::bad_alloc();
				}
			}
			
			pkgtree_node(cpm::pass_log& p, const cpm::pkgnode& n) : p(p) {
				cpm::name* _name = n.nameUserShow();
				name = _name->toCharArray();
				_name->~name();
				free(_name);
				if(name == NULL) {
					throw std::bad_alloc();
				}
				for(const cpm::pkgnode* i : n.getChildren(std::move(p))) {
					child_pkgnodes.emplace_back(p, *i);
				}
				std::sort(child_pkgnodes.begin(), child_pkgnodes.end(), &pkgnode_lt);
				for(const cpm::package* i : n.getPackages(std::move(p))) {
					child_packages.emplace_back(p, *i);
				}
				std::sort(child_packages.begin(), child_packages.end(), &pkgnode_lt);
			}
			pkgtree_node(cpm::pass_log&& p, const cpm::configuration& c) : p(p) {
				name = strdup("<repositories>");
				if(name == NULL) {
					throw std::bad_alloc();
				}
				child_pkgnodes.reserve(c.repositories.size());
				for(const cpm::pkgnode* i : c.repositories) {
					child_pkgnodes.emplace_back(p, *i);
				}
				std::sort(child_pkgnodes.begin(), child_pkgnodes.end(), &pkgnode_lt);
			}
			pkgtree_node(const pkgtree_node& o) :
				p(o.p),
				child_pkgnodes(o.child_pkgnodes),
				child_packages(o.child_packages)
			{
				name = strdup(o.name);
				if(name == NULL) {
					throw std::bad_alloc();
				}
			}
			pkgtree_node(pkgtree_node&& o) :
				p(o.p),
				child_pkgnodes(o.child_pkgnodes),
				child_packages(o.child_packages),
				name(o.name)
			{
				o.name = NULL;
			}
			~pkgtree_node() {
				if(name != NULL) {
					free(name);
					name = NULL;
				}
			}
			pkgtree_node& operator=(const pkgtree_node& o) {
				child_pkgnodes = o.child_pkgnodes;
				child_packages = o.child_packages;
				if(name != NULL) {
					free(name);
				}
				name = NULL;
				name = strdup(o.name);
				if(name == NULL) {
					throw std::bad_alloc();
				}
				return *this;
			}
			pkgtree_node& operator=(pkgtree_node&& o) {
				child_pkgnodes = o.child_pkgnodes;
				child_packages = o.child_packages;
				free(name);
				name = o.name;
				o.name = NULL;
				return *this;
			}
			class const_iterator {
				public:
					const pkgtree_node* in;
					size_t pkg_at;
					size_t node_at;
				private:
					bool node_first() const {
						// If one is at the end, the other is first.
						if(in->child_pkgnodes.size() == node_at) {
							return false;
						} else if(in->child_packages.size() == pkg_at) {
							return true;
						}
						// Compare names
						auto cmp = strcoll(in->child_pkgnodes[node_at].name, in->child_packages[pkg_at].name);
						if(cmp != 0) {
							return cmp < 0;
						}
						
						// Nodes come after packages with the same name
						return false;
					}
				public:
					const_iterator operator++(int) {
						const_iterator r = *this;
						if(node_first()) {
							++node_at;
						} else {
							++pkg_at;
						}
						return r;
					}
					const_iterator& operator++() {
						if(node_first()) {
							++node_at;
						} else {
							++pkg_at;
						}
						return *this;
					}
					const pkgtree_node operator*() {
						if(node_first()) {
							return in->child_pkgnodes[node_at];
						} else {
							return in->child_packages[pkg_at];
						}
					}
					bool operator==(const const_iterator& o) const {
						return in == o.in && pkg_at == o.pkg_at && node_at == o.node_at;
					}
					bool operator!=(const const_iterator& o) const {
						return !(*this == o);
					}
			};
			const_iterator cbegin() const {
				return {
					.in = this,
					.pkg_at = 0,
					.node_at = 0
				};
			}
			const_iterator cend() const {
				return {
					.in = this,
					.pkg_at = child_packages.size(),
					.node_at = child_pkgnodes.size()
				};
			}
			operator const char*() const {
				return name;
			}
	};
	
	void tree(configuration& c, char** args) {
		printTree(c, pkgtree_node((cpm::context) c.cpm_context, c.cpm_configuration), "", stdout);
	}
	
	void read(configuration& c, char** args) {
		for(char** read = args; *read != NULL; ++read) {
			errno = 0;
			try {
				c.cpm_configuration->parse(c.cpm_context, *read);
			} catch(const cpm::error& e) {
				printf("Skipping %s: %s\n", *read, e.what());
				errno = 0;
			} catch(const std::exception& e) {
				printf("Skipping %s: %s\n", *read, e.what());
				errno = 0;
			}
			if(errno != 0) {
				printf("Skipping %s: %s\n", *read, strerror(errno));
			}
		}
	}
	
	void parse(configuration& c, char** args) {
		for(char** read = args; *read != NULL; ++read) {
			errno = 0;
			try {
				cpm::configuration cpm;
				cpm.parse(c.cpm_context, *read);
			} catch(const cpm::error& e) {
				printf("Skipping %s: %s\n", *read, e.what());
				errno = 0;
			} catch(const std::exception& e) {
				printf("Skipping %s: %s\n", *read, e.what());
				errno = 0;
			}
			if(errno != 0) {
				printf("Skipping %s: %s\n", *read, strerror(errno));
			}
		}
	}
	
	void exit(configuration& c, char** args) {
		c.exit = true;
	}
}

const command_enum& getCommands(configuration& c) {
	static command_enum commands;
	static command_enum cmd_help;
	static command_listable cmd_list_additional(getListablesListable, c, &cmd_list_additional);
	static command_listable cmd_list(&getListablesListable, c, &cmd_list_additional);
	static command_log_mask_changes cmd_log;
	static command_file_existing cmd_parse; // Also used for read
	static command_file_existing cmd_parse_additionalfiles;
	if(commands.values.size() == 0) {
		commands.optional = false;
		cmd_help.optional = true;
		cmd_list.optional = false;
		cmd_list_additional.optional = true;
		cmd_log.optional = false;
		cmd_parse.optional = false;
		cmd_parse_additionalfiles.optional = true;
		
		cmd_parse.next = &cmd_parse_additionalfiles; // For parsing multiple files at once
		
		commands.values["x"] = NULL; // No arguments
		commands.values["exit"] = NULL;
		commands.values["t"] = NULL;
		commands.values["tree"] = NULL;
		commands.values["h"] = &cmd_help;
		commands.values["help"] = &cmd_help;
		commands.values["l"] = &cmd_list;
		commands.values["list"] = &cmd_list;
		commands.values["p"] = &cmd_parse;
		commands.values["parse"] = &cmd_parse;
		commands.values["r"] = &cmd_parse;
		commands.values["read"] = &cmd_parse;
		commands.values["log"] = &cmd_log;
	}
	return commands;
}

void interactive(configuration& c) {
	const static command_enum& commands = getCommands(c);
	bool isEOF = false;
	errno = 0;
	while(!c.exit) {
		char* in = NULL;
		char** args = NULL;
		size_t argc = 0;
		size_t cmdargs = 0;
		char* at;
		const command* current;
#ifdef WITH_READLINE
		cpmconf_rl_completion_configuration = &c;
		rl_attempted_completion_function = &complete_full_command;
		rl_char_is_quoted_p = &full_command_is_quoted;
		rl_completer_word_break_characters = strdup(" \n\t");
		rl_completer_quote_characters = "";
#endif
		in = userline(c.prompt->c_str(), isEOF);
#ifdef WITH_READLINE
		free(rl_completer_word_break_characters);
		rl_completer_word_break_characters = NULL;
#endif
		if(isEOF) {
			c.exit = true;
			printf("exit\n");
			break;
		}
		if(in == NULL) {
			goto cmderror;
		}
		message_str(c, cpm::logging_level::DEBUG, "Running command %s\n", in);
		args = (char**) calloc(1, sizeof(char*));
		if(args == NULL) {
			goto cmderror;
		}
		args[0] = NULL;
		at = in;
		
		current = &getCommands(c);
		while(current != NULL && at != NULL) {
			char* read = at;
			at = current->token(read);
			
			char** tmp = (char**) realloc(args, (++argc + 1) * sizeof(char*));
			if(tmp == NULL) {
				goto cmderror;
			}
			args = tmp;
			args[argc - 1] = read;
			args[argc] = NULL;
			++cmdargs;
			
			current = current->getNext(read);
		}
		if(current == NULL && at != NULL) {
			printf("%s", _("Too many arguments for function!\n"));
			goto cmderror;
		}
		if(current != NULL && !current->optional) {
			while(current != NULL && !current->optional) {
				char* add = current->ask();
				if(add == NULL) {
					printf("%s", "Operation cancelled.\n");
					goto cmderror;
				}
				
				char** tmp = (char**) realloc(args, (++argc + 1) * sizeof(char*));
				if(tmp == NULL) {
					free(add);
					goto cmderror;
				}
				args = tmp;
				args[argc - 1] = add;
				args[argc] = NULL;
				
				current = current->getNext(add);
			}
		}
		
#define cmd(a, b) strcmp(args[0], a) == 0 || strcmp(args[0], b) == 0
#define cmd_s(a) strcmp(args[0], a) == 0
		if(cmd("x", "exit")) {
			interactive_functions::exit(c, args + 1);
		} else if(cmd("t", "tree")) {
			interactive_functions::tree(c, args + 1);
		} else if(cmd("h", "help")) {
			interactive_functions::help(c, args + 1);
		} else if(cmd("l", "list")) {
			interactive_functions::list(c, args + 1);
		} else if(cmd("p", "parse")) {
			interactive_functions::parse(c, args + 1);
		} else if(cmd("r", "read")) {
			interactive_functions::read(c, args + 1);
		} else if(cmd_s("log")) {
			interactive_functions::log(c, args + 1);
		} else {
			printf(_("Unknown command %s\nType help for help\n"), args[0]);
			goto cmderror;
		}
#undef cmd
#undef cmd_s
cmderror:
		if(in != NULL) {
			free(in);
			in = NULL;
		}
		if(rl_completer_word_break_characters != NULL) {
			free(rl_completer_word_break_characters);
			rl_completer_word_break_characters = NULL;
		}
		if(args != NULL) {
			for(char** i = args + cmdargs; *i != NULL; ++i) {
				free(*i);
			}
			free(args);
		}
		if(errno != 0) {
			printf("Error: %s\n", strerror(errno));
			errno = 0;
		}
	}
}
