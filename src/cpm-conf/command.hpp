/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BIN_CPM_CONF_INTERACTIVE_COMMAND_HPP
#define BIN_CPM_CONF_INTERACTIVE_COMMAND_HPP

#include <stddef.h>

#include "config.hpp"

class command {
	public:
		bool optional = false;
		virtual const command* getNext(const char* value) const = 0;
		virtual char* token(char* str) const;
		virtual char* ask() const = 0;
#ifdef WITH_READLINE
		virtual char** ask_complete(const char* text, size_t cursor_position) const = 0;
		virtual char** complete(const char* text, size_t cursor_position) const = 0;
#endif
};

#endif
