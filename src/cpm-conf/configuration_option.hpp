/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BIN_CPM_CONF_CONFIGURATION_OPTION_HPP
#define BIN_CPM_CONF_CONFIGURATION_OPTION_HPP

#include <utility>
#include <algorithm>

#include "cpm.hpp"

template<class _T>
class configuration_option {
	public:
		bool has = false;
		_T val;
		configuration_option() : has(false) {}
		configuration_option(const configuration_option<_T>& o) : has(o.has), val(o.val) {}
		configuration_option(configuration_option<_T>&& o) : has(std::move(o.has)), val(std::move(o.val)) {}
		template<class... Args>
		configuration_option(Args... args) : has(true), val(std::forward<Args>(args)...) {}
		
		configuration_option<_T>& update(const configuration_option<_T>& o) {
			if(o.has) {
				has = true;
				val = o.val;
			}
			return *this;
		}
		configuration_option<_T>& unset() {
			has = false;
			return *this;
		}
		template<class Arg>
		configuration_option<_T>& operator=(Arg&& v) {
			has = true;
			val = std::move(v);
			return *this;
		}
		template<class Arg>
		configuration_option<_T>& operator=(Arg& v) {
			has = true;
			val = v;
			return *this;
		}
		operator _T&() {
			return val;
		}
		operator const _T&() const {
			return val;
		}
		_T* operator->() {
			return &val;
		}
		const _T* operator->() const {
			return &val;
		}
};

template<>
class configuration_option<cpm::logging_levels>;

template<>
class configuration_option<cpm::logging_levels> {
	public:
		typedef cpm::logging_levels _T;
		_T has;
		_T val;
		configuration_option();
		configuration_option(const _T& o);
		configuration_option(_T&& o);
		
		configuration_option<_T>& update(const configuration_option<_T>& o);
		configuration_option<_T>& unset();
		configuration_option<_T>& operator=(_T&& v);
		configuration_option<_T>& operator=(_T& v);
		operator _T&();
		operator const _T&() const;
		_T* operator->();
		const _T* operator->() const;
};

template<class _T>
std::ostream& operator<<(std::ostream& s, const configuration_option<_T> o) {
	if(o.has) {
		s << o.val;
	}
	return s;
}

#endif
