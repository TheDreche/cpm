/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#include <errno.h>
#include <stdio.h>

#ifdef WITH_READLINE
# include <readline/readline.h>
# include <readline/history.h>
# ifdef HAVE_STDLIB_H
#  include <stdlib.h>
# endif
#endif

#include "cpm.hpp"

#include "userline.hpp"

char* userline(const char* prompt, bool& isEOF) {
#ifdef WITH_READLINE
	char* input = readline(prompt);
	if(input == NULL) {
		isEOF = true;
		return NULL;
	} else {
		// Check whether it contains any non-whitespace character
		char* at_ws = input;
		while(*at_ws != '\0') {
			if(!isspace(*at_ws)) {
				break;
			}
			++at_ws;
		}
		if(*at_ws == '\0') {
			// Just whitespace
			free(input);
			input = NULL;
			return NULL;
		}
		// Save in the history
		char* at = input;
		while(*at != '\0') {
			if(!isspace(*at++)) {
				add_history(input);
			}
		}
		// Return
		return input;
	}
#else
	if(feof(stdin)) {
		isEOF = true;
		return NULL;
	}
	printf("%s", prompt);
	size_t len = 0;
	char* input = NULL;
	if(getline(&input, &len, stdin) == -1) {
		switch (errno) {
			case ENOMEM:
				throw std::bad_alloc();
			default:
				throw cpm::error();
		}
	}
	// Check whether it contains any non-whitespace character
	char* at_ws = input;
	while(*at_ws != '\0') {
		if(!isspace(*at_ws)) {
			break;
		}
		++at_ws;
	}
	if(*at_ws == '\0') {
		// Just whitespace
		free(input);
		input = NULL;
		return NULL;
	}
	return input;
#endif
}
