/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BIN_CPM_CONF_TREE_HPP
#define BIN_CPM_CONF_TREE_HPP

#include "config.hpp"

#include <stdio.h>
#ifdef HAVE_STRING_H
# include <string.h>
#endif

#include "configuration.hpp"

template<class _T>
void printTree(configuration& c, const _T root, const char* indent, FILE* stream) {
	char* childIndent = NULL;
	auto iter = root.cbegin();
	auto iter_end = root.cend();
	
	size_t indent_len = strlen(indent);
	childIndent = (char*) calloc(indent_len + c.tree_indent->size() + 1, sizeof(char));
	if(childIndent == NULL) {
		goto free;
	}
	memcpy(childIndent, indent, indent_len);
	memcpy(childIndent + indent_len, c.tree_indent->c_str(), c.tree_indent->size());
	childIndent[indent_len + c.tree_indent->size()] = '\0';
	
	printf("%s\n", (const char*) root);
	
	while(iter != iter_end) {
		auto next = iter;
		++next;
		if(next == iter_end) {
			char* tmp = (char*) realloc(childIndent, indent_len + c.tree_indent_post_last->size() + 1);
			if(tmp == NULL) {
				goto free;
			}
			childIndent = tmp;
			tmp = NULL;
			memcpy(childIndent + indent_len, c.tree_indent_post_last->c_str(), c.tree_indent_post_last->size());
			childIndent[indent_len + c.tree_indent_post_last->size()] = '\0';
			printf("%s%s", indent, c.tree_node_last->c_str());
		} else {
			printf("%s%s", indent, c.tree_node->c_str());
		}
		printTree(c, *iter, childIndent, stream);
		iter = next;
	}
free:
	if(childIndent != NULL) {
		free(childIndent);
	}
}

#endif
