/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#include <stdio.h> // Required for readline
#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif
#ifdef HAVE_STRING_H
# include <string.h>
#endif

#ifdef WITH_READLINE
# include <readline/readline.h>
#endif

#include "completion.hpp"
#include "interactive.hpp"
#include "command.hpp"
#include "configuration.hpp"

configuration* cpmconf_rl_completion_configuration = NULL;
static char** completions = NULL;

static char* generator(const char* text, int state) {
	static size_t i;
	if(state == 0) {
		i = 0;
	}
	if(completions == NULL) {
		return NULL;
	}
	return completions[i++];
}

char** complete_full_command(const char* word, int start, int end) {
	const static command_enum& commands = getCommands(*cpmconf_rl_completion_configuration);
	
	char* line = strdup(rl_line_buffer);
	char* current = line;
	char** possibilities = NULL;
	
	char* cursor = &line[rl_point];
	bool shouldCompleteThis = false;
	
	const command* current_arg = &commands;
	while(true) {
		char* next = current_arg->token(current);
		char* end = current + strlen(current);
		if(cursor <= end) {
			// This should be completed
			// current > cursor if the cursor is at a space between separated values
			possibilities = current_arg->complete(current, current > cursor? 0 : cursor - current);
			break;
		} else {
			if(next == NULL) {
				// This can happen when the cursor is at the end of the line
				current_arg = current_arg->getNext(current);
				if(current_arg == NULL) {
					break;
				}
				possibilities = current_arg->complete("", 0);
				break;
			} else {
				current_arg = current_arg->getNext(current);
				if(current_arg == NULL) {
					// This can happen when the user tries to give more arguments than needed.
					break;
				}
				current = next;
			}
		}
	}
	free(line);
	
	completions = possibilities;
	char** ret = rl_completion_matches(word, &generator);
	free(completions);
	completions = NULL;
	
	return ret;
}

int full_command_is_quoted(char* text, int position) {
	const static command_enum& commands = getCommands(*cpmconf_rl_completion_configuration);
	
	char* line = NULL;
	char* cursor;
	char* current;
	const command* current_arg = &commands;
	
	size_t text_len = strlen(text);
	
	if(position < 0 || position >= text_len) {
		errno = EINVAL;
		goto error;
	}
	
	line = strdup(text);
	if(line == NULL) {
		goto error;
	}
	cursor = &line[position];
	
	current = line;
	while(true) {
		char* next = current_arg->token(current);
		char* end = current + strlen(current);
		if(cursor < end) {
			free(line);
			return cursor < current;
		} else {
			if(next == NULL) {
				goto error;
			} else {
				current_arg = current_arg->getNext(current);
				if(current_arg == NULL) {
					goto error;
				}
				current = next;
			}
		}
	}
error:
	if(line != NULL) {
		free(line);
	}
	return true;
}
