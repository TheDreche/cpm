/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#include "gettext.h"
#define _(s) gettext(s)
#define _n(s) ngettext(s)
#ifdef WITH_READLINE
# ifdef HAVE_STDIO_H
#  include <stdio.h> // required for readline
# endif
# include <readline/readline.h>
#endif

#include "command_log_mask_changes.hpp"
#include "command_util.hpp"
#include "userline.hpp"

char* command_log_mask_changes::token(char* str) const {
	while(*str != '\0' && *str != ' ') {
		++str;
	}
	return token_return_skip_whitespace(str);
}

char* command_log_mask_changes::ask() const {
	bool isEOF;
#ifdef WITH_READLINE
	ask_completor = this;
	rl_attempted_completion_function = &ask_completion_function;
	rl_completer_word_break_characters = strdup("");
	rl_completer_quote_characters = "";
#endif
	char* r = userline(_("Changes? "), isEOF);
#ifdef WITH_READLINE
	if(rl_completer_word_break_characters != NULL) {
		free(rl_completer_word_break_characters);
		rl_completer_word_break_characters = NULL;
	}
#endif
	if(r != NULL) {
		if(*r == '\0') {
			free(r);
			r = NULL;
		}
	}
	return r;
}

#ifdef WITH_READLINE
char** command_log_mask_changes::complete(const char* text, size_t cursor_position) const {
	return NULL;
}

char** command_log_mask_changes::ask_complete(const char* text, size_t cursor_position) const {
	return NULL;
}
#endif
