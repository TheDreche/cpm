/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_STRING_H
# include <string.h>
#endif
#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include <vector>
#include <string>
#include <sstream>

#include "cpm.hpp"

#ifdef WITH_NCURSES
# include <ncurses.h>
# include <term.h>
# include "addcursesstr.hpp"
#endif

#include "getconf.hpp"
#include "errors.hpp"
#include "logging.hpp"
#include "util.hpp"

configuration parse_options(int argc, char** argv) {
	configuration a;
	int opt_;
	while((opt_ = getopt(argc, argv, "-hil:f:dVr:")) != -1) {
		char opt = opt_;
		switch(opt) {
			case 'h':
				a.act = configuration::PRINT_HELP;
				break;
			case 'V':
				a.act = configuration::PRINT_VERSION;
				break;
			case 'f':
				a.files.emplace_back(optarg);
				break;
			case 'i':
				a.act = configuration::INTERACTIVE;
				break;
			case 'l':
				a.setLog(optarg);
				break;
			case 'r':
				a.readline_name.val = optarg;
				break;
			case 'd':
				a.mask.val |= cpm::logging_level::DEBUG;
				a.mask.has |= cpm::logging_level::DEBUG;
				break;
			case '?':
				throw error_invalid_option();
		}
	}
	return a;
}

configuration get_env_conf(configuration& c) {
	configuration r = configuration();
	const char* env;
	if((env = getenv("CPMCONF_ACTION")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Setting action to %s\n", env));
		if(strcmp(env, "PARSE") == 0 || strcmp(env, "parse") == 0) {
			r.act = configuration::action::PARSE;
		} else if(strcmp(env, "INTERACTIVE") == 0 || strcmp(env, "interactive") == 0) {
			r.act = configuration::action::INTERACTIVE;
		} else if(strcmp(env, "HELP") == 0 || strcmp(env, "help") == 0) {
			r.act = configuration::action::PRINT_HELP;
		} else if(strcmp(env, "VERSION") == 0 || strcmp(env, "version") == 0) {
			r.act = configuration::action::PRINT_VERSION;
		} else {
			message_str(c, cpm::logging_level::WARN, format("Unknown action \"%s\" for $CPMCONF_ACTION\n", env));
		}
	}
	if((env = getenv("CPMCONF_SELF")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("I am %s\n", env));
		r.self = env;
	}
	if((env = getenv("CPMCONF_FILES")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Note for parsing: %s\n", env));
		std::stringstream in(env);
		std::string filepath;
		while(in.good()) {
			in >> filepath;
			r.files.push_back(filepath);
		}
	}
	if((env = getenv("CPMCONF_LOG_MASK")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Changing logging mask: %s\n", env));
		r.setLog(env);
	}
	if((env = getenv("CPMCONF_PROMPT")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Setting prompt to %s\n", env));
		r.prompt = env;
	}
	if((env = getenv("CPMCONF_READLINE_NAME")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Setting readline_name to %s\n", env));
		r.readline_name = env;
	}
	if((env = getenv("CPMCONF_PROMPT_QUESTION")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Setting question prompt to %s\n", env));
		r.prompt_question = env;
	}
	if((env = getenv("CPMCONF_TREE_INDENT")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Setting tree indent string to %s\n", env));
		r.tree_indent = env;
	}
	if((env = getenv("CPMCONF_TREE_INDENT_POST_LAST")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Setting tree indent post last string to %s\n", env));
		r.tree_indent_post_last = env;
	}
	if((env = getenv("CPMCONF_TREE_NODE")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Setting tree node string to %s\n", env));
		r.tree_node = env;
	}
	if((env = getenv("CPMCONF_TREE_NODE_LAST")) != NULL) {
		message_str(c, cpm::logging_level::DEBUG, format("Setting tree node last string to %s", env));
		r.tree_node_last = env;
	}
	return r;
}

configuration get_default_conf() {
	static const char* self = CPMCONF_DEFAULT_SELF;
	configuration r = {
		.prompt = CPMCONF_DEFAULT_PROMPT,
		.prompt_question = CPMCONF_DEFAULT_PROMPT_QUESTION,
		.cpm_context = cpm::context(),
		.self = self,
		.readline_name = CPMCONF_DEFAULT_READLINE_NAME,
		.act = configuration::action::CPMCONF_DEFAULT_ACTION,
		.tree_indent = "| ",
		.tree_indent_post_last = "  ",
		.tree_node = "+- ",
		.tree_node_last = "+- ",
		.files = CPMCONF_DEFAULT_FILES,
		.exit = false,
	};
	r.setLog(CPMCONF_DEFAULT_LOG_MASK);
#ifdef WITH_NCURSES
	// Set the tree strings
	// The fallback:
	// 
	// top
	// +- child
	// | +- subchild1
	// | +- subchild2
	// +- child2
	//   +- subchild3
	//   +- subchild4
	// 
	// The usual one looks much better.
	if(isatty(STDOUT_FILENO)) {
		r.tree_indent->clear();
		r.tree_indent_post_last->clear();
		r.tree_node->clear();
		r.tree_node_last->clear();
		
		r.tree_indent->reserve(5);
		addcursesstr(r.tree_indent.val, tparm(enter_alt_charset_mode));
		r.tree_indent->push_back('\x78');
		addcursesstr(r.tree_indent.val, tparm(exit_alt_charset_mode));
		r.tree_indent->push_back(' ');
		r.tree_indent->push_back(' ');
		
		r.tree_indent_post_last.val = "   ";
		
		r.tree_node->reserve(5);
		addcursesstr(r.tree_node.val, tparm(enter_alt_charset_mode));
		r.tree_node->push_back('\x74');
		r.tree_node->push_back('\x71');
		addcursesstr(r.tree_node.val, tparm(exit_alt_charset_mode));
		r.tree_node->push_back(' ');
		
		r.tree_node_last->reserve(5);
		addcursesstr(r.tree_node_last.val, tparm(enter_alt_charset_mode));
		r.tree_node_last->push_back('\x6D');
		r.tree_node_last->push_back('\x71');
		addcursesstr(r.tree_node_last.val, tparm(exit_alt_charset_mode));
		r.tree_node_last->push_back(' ');
		
		r.tree_indent->shrink_to_fit();
		r.tree_indent_post_last->shrink_to_fit();
		r.tree_node->shrink_to_fit();
		r.tree_node_last->shrink_to_fit();
	}
#endif
	
	return r;
}
