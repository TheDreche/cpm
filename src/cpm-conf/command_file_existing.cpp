/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

#include <locale>

#include "cpm.hpp"

#include "config.hpp"
#ifdef WITH_READLINE
# include <readline/readline.h>
# ifdef HAVE_SYS_STAT_H
#  include <sys/stat.h>
#  include <dirent.h>
# endif
#endif

#include "command_file_existing.hpp"
#include "command_util.hpp"
#include "passwdentry.hpp"
#include "userline.hpp"
#include "util.hpp"

enum class quoting : unsigned char {
	SINGLE = 0b001,
	DOUBLE = 0b010,
	BACKSLASH = 0b100,
	NONE = 0b000,
	MAX = 0b111
};

char* command_file_existing::token(char* str) const {
	cpm::flags<quoting, (size_t) quoting::MAX> quoted;
	while(*str != '\0') {
		if(quoted & quoting::BACKSLASH) {
			quoted.rm(quoting::BACKSLASH);
		} else {
			if(quoted.isSet(quoting::SINGLE)) {
				if(*str == '\'') {
					quoted.rm(quoting::SINGLE);
				}
			} else {
				if(quoted.isSet(quoting::DOUBLE)) {
					switch(*str) {
						case '"':
							quoted.rm(quoting::DOUBLE);
							break;
						case '\\':
							quoted |= quoting::BACKSLASH;
							break;
					}
				} else {
					if(*str == ' ') {
						break;
					}
					switch (*str) {
						case '\'':
							quoted |= quoting::SINGLE;
							break;
						case '"':
							quoted |= quoting::DOUBLE;
							break;
					}
				}
			}
		}
		++str;
	}
	return token_return_skip_whitespace(str);
}

static char _first_unquoted_char(const char* at, const char* to) {
	cpm::flags<quoting, (size_t) quoting::MAX> q;
	while(at < to) {
		if(q.isSet(quoting::BACKSLASH)) {
			return *at;
		} else if(q.isSet(quoting::SINGLE)) {
			if(*at == '\'') {
				q.rm(quoting::SINGLE);
			} else {
				return *at;
			}
		} else if(q.isSet(quoting::DOUBLE)) {
			if(*at == '"') {
				q.rm(quoting::DOUBLE);
			} else if(*at == '\\') {
				q |= quoting::BACKSLASH;
			} else {
				return *at;
			}
		} else {
			if(*at == '\'') {
				q |= quoting::SINGLE;
			} else if(*at == '"') {
				q |= quoting::DOUBLE;
			} else if(*at == '\\') {
				q |= quoting::BACKSLASH;
			} else {
				return *at;
			}
		}
		++at;
	}
	return '\0';
}

static char* _strrchrend_firstismatch(char* begin, char* from, char search) {
	if(from != begin) {
		do {
			--from;
		} while(*from != search);
	}
	return from;
}

static char* _path_shorten(char* abspath) {
	char* at = abspath + 1;
	char* prev = at;
	char* prev_prev = at;
	unsigned char specialfolder = 1; // 1 = could be; 0 = can't be; 2 = ./; 3 = ../
	cpm::flags<quoting, (size_t) quoting::MAX> q;
	char current;
	while((current = *at) != '\0') {
		if(q.isSet(quoting::BACKSLASH)) {
			q.rm(quoting::BACKSLASH);
		} else if(q.isSet(quoting::DOUBLE)) {
			if(current == '"') {
				q.rm(quoting::DOUBLE);
				memmove(at, at + 1, (strlen(at + 1) + 1) * sizeof(char));
				continue;
			} else if(current == '\\') {
				q |= quoting::BACKSLASH;
				memmove(at, at + 1, (strlen(at + 1) + 1) * sizeof(char));
				continue;
			}
		} else if(q.isSet(quoting::SINGLE)) {
			if(current == '\'') {
				q.rm(quoting::SINGLE);
				memmove(at, at + 1, (strlen(at + 1) + 1) * sizeof(char));
				continue;
			}
		} else {
			if(current == '"') {
				q |= quoting::DOUBLE;
				memmove(at, at + 1, (strlen(at + 1) + 1) * sizeof(char));
				continue;
			} else if(current == '\'') {
				q |= quoting::SINGLE;
				memmove(at, at + 1, (strlen(at + 1) + 1) * sizeof(char));
				continue;
			} else if(current == '\\') {
				q |= quoting::BACKSLASH;
				memmove(at, at + 1, (strlen(at + 1) + 1) * sizeof(char));
				continue;
			}
		}
		if(current == '/') {
			if(specialfolder == 1) {
				// Found //, remove one slash
				--at;
				memmove(at, at + 1, (strlen(at + 1) + 1) * sizeof(char));
			} else if(specialfolder == 2) {
				// Found ./, remove it
				at -= 2;
				memmove(at, at + 2, (strlen(at + 2) + 1) * sizeof(char));
			} else if(specialfolder == 3) {
				// Found ../, remove it along the previous directory part
				memmove(prev_prev, at + 1, (strlen(at + 1) + 1) * sizeof(char));
				at = prev_prev - 1;
				
				--prev_prev;
				prev_prev = _strrchrend_firstismatch(abspath, prev_prev, '/');
				++prev_prev;
			} else {
				prev_prev = prev;
			}
			prev = at + 1;
			specialfolder = 1;
		} else if(specialfolder > 0) {
			if(specialfolder == 3) {
				specialfolder = 0;
			} else if(current == '.') {
				++specialfolder;
			} else {
				specialfolder = 0;
			}
		}
		++at;
	}
	return (char*) realloc(abspath, (strlen(abspath) + 1) * sizeof(char));
}

char* command_file_existing::interpret(const char* from, const char* to) {
	char* tmp;
	if(to == NULL) {
		to = from + strlen(from);
	}
	if(from == to) {
		return get_current_working_directory();
	}
	char* abspath;
	char firstchar = _first_unquoted_char(from, to);
	
	char* user = NULL;
	char* user_home = NULL;
	if(firstchar == '/') {
		tmp = (char*) calloc((to - from) + 1, sizeof(char));
		if(tmp == NULL) {
			goto error;
		}
		abspath = tmp;
		memcpy(abspath, from, to - from);
		abspath[to - from] = '\0';
	} else if(from[0] == '~') {
		const char* user_end = from + 1;
		while(std::isalnum(*user_end, std::locale::classic()) && user_end < to) {
			++user_end;
		}
		if(*user_end == '/' || user_end == to) {
			// The path starts with ~user/, replace it by the absolute path
			if(from + 1 == user_end) {
				// Just ~/, set user to current user.
				
			} else {
				// ~ gets replaced by \0, so no +1 in size needed
				size_t user_len = user_end - (from + 1);
				user = (char*) calloc(user_len + 1, sizeof(char));
				if(user == NULL) {
					goto error;
				}
				memcpy(user, from + 1, user_len * sizeof(char));
				user[user_len] = '\0';
				user_home = get_entry_of_user(5, user);
				free(user);
			}
			if(user_home == NULL) {
				goto error;
			}
			size_t user_home_len = strlen(user_home);
			if(user_home[user_home_len - 1] == '/') {
				user_home[--user_home_len] = '\0';
			}
			if(user_end < to) {
				tmp = (char*) std::realloc(user_home, (user_home_len + 1 + (to - (user_end + 1)) + 1) * sizeof(char));
				if(tmp == NULL) {
					goto error;
				}
				user_home = tmp;
			} else {
				tmp = (char*) std::realloc(user_home, (user_home_len + 1) * sizeof(char));
				if(tmp == NULL) {
					goto error;
				}
				user_home = tmp;
			}
			abspath = user_home;
			abspath[user_home_len] = '/';
			if(user_end < to) {
				memcpy(abspath + user_home_len + 1, user_end + 1, (to - (user_end + 1)) * sizeof(char));
				abspath[user_home_len + 1 + (to - (user_end + 1))] = '\0';
			} else {
				abspath[user_home_len] = '\0';
			}
		}
	} else {
		abspath = get_current_working_directory();
		size_t abspath_len = strlen(abspath);
		tmp = (char*) realloc(abspath, (abspath_len + 1 + (to - from) + 1) * sizeof(char));
		if(tmp == NULL) {
			goto error;
		}
		abspath = tmp;
		abspath[abspath_len++] = '/';
		memcpy(abspath + abspath_len, from, (to - from));
		abspath_len += to - from;
		abspath[abspath_len] = '\0';
	}
	if(abspath == NULL) {
		goto error;
	}
	tmp = _path_shorten(abspath);
	if(tmp == NULL) {
		goto error;
	}
	return tmp;
	
error:
	if(abspath != NULL) {
		free(abspath);
	}
	if(user != NULL) {
		free(user);
	}
	if(user_home != NULL) {
		free(user_home);
	}
	return NULL;
}

char* command_file_existing::ask() const {
	bool isEOF;
#ifdef WITH_READLINE
	ask_completor = this;
	rl_attempted_completion_function = &ask_completion_function;
	rl_completer_word_break_characters = strdup("");
	rl_completer_quote_characters = "";
#endif
	char* r = userline("File? ", isEOF);
#ifdef WITH_READLINE
	if(rl_completer_word_break_characters != NULL) {
		free(rl_completer_word_break_characters);
		rl_completer_word_break_characters = NULL;
	}
#endif
	if(r != NULL) {
		if(*r == '\0') {
			free(r);
			r = NULL;
		}
	}
	return r;
}

#ifdef HAVE_SYS_STAT_H
static cpm::flags<quoting, (size_t) quoting::MAX> _get_quoting_at_end(const char* from, const char* to = NULL) {
	if(to == NULL) {
		to = from + strlen(from);
	}
	cpm::flags<quoting, (size_t) quoting::MAX> q;
	
	char current;
	for(const char* at = from; at < to; ++at) {
		current = *at;
		if(q.isSet(quoting::BACKSLASH)) {
			q.rm(quoting::BACKSLASH);
		} else if(q.isSet(quoting::SINGLE)) {
			if(current == '\'') {
				q.rm(quoting::SINGLE);
			}
		} else if(q.isSet(quoting::DOUBLE)) {
			if(current == '"') {
				q.rm(quoting::DOUBLE);
			} else if(current == '\\') {
				q |= quoting::BACKSLASH;
			}
		} else {
			if(current == '\'') {
				q |= quoting::SINGLE;
			} else if(current == '"') {
				q |= quoting::DOUBLE;
			} else if(current == '\\') {
				q |= quoting::BACKSLASH;
			}
		}
	}
	
	return q;
}

static char* _command_file_existing_quote(const char* text, quoting q, bool add_end_quote = false) {
	size_t len = 0;
	for(const char* at = text; *at != '\0'; ++at) {
		++len;
		switch(*at) {
			case ' ':
				if(!(q == quoting::DOUBLE || q == quoting::SINGLE)) {
					++len;
				}
				break;
			case '\\':
			case '"':
				if(q != quoting::SINGLE) {
					++len;
				}
				break;
			case '\'':
				if(q != quoting::DOUBLE) {
					if(q == quoting::SINGLE) {
						len += 4; // '\''
						goto next_c;
					} else {
						++len;
					}
				}
				break;
		}
next_c:
		continue;
	}
	if(add_end_quote) {
		if(q == quoting::DOUBLE || q == quoting::SINGLE) {
			++len;
		}
	}
	
	char* r = (char*) calloc(len + 1, sizeof(char));
	if(r == NULL) {
		return NULL;
	}
	r[len] = '\0';
	const char* text_at = text;
	char* r_at = r;
	while(*text_at != '\0') {
		switch(*text_at) {
			case ' ':
				if(!(q == quoting::DOUBLE || q == quoting::SINGLE)) {
					*r_at++ = '\\';
				}
				break;
			case '\\':
			case '"':
				if(q != quoting::SINGLE) {
					*r_at++ = '\\';
				}
				break;
			case '\'':
				if(q != quoting::DOUBLE) {
					if(q == quoting::SINGLE) {
						len += 4; // '\''
						*r_at++ = '\'';
						*r_at++ = '\\';
						*r_at++ = '\'';
						*r_at++ = '\'';
						goto next_w;
					} else {
						*r_at++ = '\\';
					}
				}
				break;
		}
		*r_at++ = *text_at++;
next_w:
		continue;
	}
	if(add_end_quote) {
		if(q == quoting::DOUBLE) {
			*r_at++ = '"';
		} else if(q == quoting::SINGLE) {
			*r_at++ = '\'';
		}
	}
	
	return r;
}

char** command_file_existing::complete(const char* text, size_t cursor_position) const {
	bool had_error = false;
	struct stat s;
	char* p;
	char** r = NULL;
	size_t c = 0;
	char* possibility = NULL;
	DIR* d = NULL;
	size_t p_len = 0;
	cpm::flags<quoting, (size_t) quoting::MAX> quoting_end = _get_quoting_at_end(text, text + cursor_position);
	quoting q = quoting::NONE;
	char* means = command_file_existing::interpret(text, text + cursor_position);
	p = (char*) calloc(cursor_position + 1, sizeof(char));
	memcpy(p, text, cursor_position * sizeof(char));
	p[cursor_position] = '\0';
	if(p == NULL || means == NULL) {
		had_error = true;
		goto error;
	}
	p_len = strlen(p);
	
	// Determine which quoting to use
	if(quoting_end.isSet(quoting::DOUBLE)) {
		q = quoting::DOUBLE;
	} else if(quoting_end.isSet(quoting::SINGLE)) {
		q = quoting::SINGLE;
	} else {
		q = quoting::BACKSLASH;
	}
	
	if(stat(means, &s) == 0) {
		if(S_ISDIR(s.st_mode)) {
			if(p[p_len - 1] == '/') {
				d = opendir(means);
				if(d == NULL) {
					had_error = true;
					goto error;
				}
				errno = 0;
				dirent* e;
				while((e = readdir(d)) != NULL) {
					if(e->d_name[0] == '.') {
						continue;
					}
					bool add_end_quote = true;
					bool add_slash = false;
					if(e->d_type == DT_DIR) {
						add_end_quote = false;
						add_slash = true;
						rl_completion_suppress_append = 1;
					}
					char* name = _command_file_existing_quote(e->d_name, q, add_end_quote);
					if(name == NULL) {
						had_error = true;
						goto error;
					}
					size_t name_len = strlen(name);
					if(add_slash) {
						char* tmp = (char*) realloc(name, (++name_len + 1) * sizeof(char));
						if(tmp == NULL) {
							free(name);
							had_error = true;
							goto error;
						}
						name = tmp;
						name[name_len - 1] = '/';
						name[name_len] = '\0';
					}
					size_t len = p_len + name_len;
					possibility = (char*) calloc(len + 1, sizeof(char));
					if(possibility == NULL) {
						free(name);
						had_error = true;
						goto error;
					}
					memcpy(possibility, p, p_len * sizeof(char));
					memcpy(possibility + p_len, name, (name_len + 1) * sizeof(char));
					free(name);
					if(add_possibility(r, c, possibility)) {
						had_error = true;
						goto error;
					}
					possibility = NULL;
				}
				if(errno != 0) {
					had_error = true;
					goto error;
				}
			} else {
				char* tmp = (char*) realloc(p, p_len + 2);
				if(tmp == NULL) {
					had_error = true;
					goto error;
				}
				p = tmp;
				p[p_len] = '/';
				++p_len;
				p[p_len] = '\0';
				if(add_possibility(r, c, p)) {
					had_error = true;
					goto error;
				}
				rl_completion_suppress_append = 1;
			}
		} else {
			if(q == quoting::SINGLE || q == quoting::DOUBLE) {
				char* tmp = (char*) realloc(p, ++p_len + 1);
				if(tmp == NULL) {
					had_error = true;
					goto error;
				}
				p = tmp;
				p[p_len] = '\0';
				if(q == quoting::SINGLE) {
					p[p_len - 1] = '\'';
				} else if(q == quoting::DOUBLE) {
					p[p_len - 1] = '"';
				}
			}
			if(add_possibility(r, c, p)) {
				had_error = true;
				goto error;
			}
		}
	} else {
		// File doesn't exist, so in the way of typing a file name
		char* name_start = strrchr(p, '/');
		size_t name_start_len;
		if(name_start == NULL) {
			name_start = p;
			char* cwd = get_current_working_directory();
			if(cwd == NULL) {
				had_error = true;
				goto error;
			}
			d = opendir(cwd);
			free(cwd);
			p_len = 0;
			name_start_len = strlen(name_start);
		} else {
			char* tmp = (char*) realloc(p, p_len + 2);
			if(tmp == NULL) {
				had_error = true;
				goto error;
			}
			p = tmp;
			++name_start;
			name_start_len = strlen(name_start);
			memmove(name_start + 1, name_start, (name_start_len + 1) * sizeof(char));
			*(name_start++) = '\0';
			char* dir = command_file_existing::interpret(p);
			if(dir == NULL) {
				had_error = true;
				goto error;
			}
			d = opendir(dir);
			free(dir);
			p_len = strlen(p);
		}
		if(d == NULL) {
			had_error = true;
			goto error;
		}
		errno = 0;
		dirent* e;
		while((e = readdir(d)) != NULL) {
			if(name_start[0] != '.' && e->d_name[0] == '.') {
				continue;
			}
			if(strncmp(e->d_name, name_start, name_start_len) == 0) {
				bool add_end_quote = true;
				bool add_slash = false;
				if(e->d_type == DT_DIR) {
					add_end_quote = false;
					add_slash = true;
					rl_completion_suppress_append = 1;
				}
				char* name = _command_file_existing_quote(e->d_name, q, add_end_quote);
				if(name == NULL) {
					had_error = true;
					goto error;
				}
				size_t name_len = strlen(name);
				if(add_slash) {
					char* tmp = (char*) realloc(name, (++name_len + 1) * sizeof(char));
					if(tmp == NULL) {
						free(name);
						had_error = true;
						goto error;
					}
					name = tmp;
					name[name_len - 1] = '/';
					name[name_len] = '\0';
				}
				size_t len = p_len + name_len;
				possibility = (char*) calloc(len + 1, sizeof(char));
				if(possibility == NULL) {
					free(name);
					had_error = true;
					goto error;
				}
				memcpy(possibility, p, p_len * sizeof(char));
				memcpy(possibility + p_len, name, (name_len + 1) * sizeof(char));
				free(name);
				if(add_possibility(r, c, possibility)) {
					had_error = true;
					goto error;
				}
				possibility = NULL;
			}
		}
		if(errno != 0) {
			had_error = true;
			goto error;
		}
	}
error:
	if(p != NULL) {
		free(p);
	}
	if(means != NULL) {
		free(means);
	}
	if(had_error && r != NULL) {
		for(size_t i = 0; r[i] != NULL; ++i) {
			free(r[i]);
		}
		free(r);
		r = NULL;
	}
	if(d != NULL) {
		closedir(d);
	}
	return r;
}

char** command_file_existing::ask_complete(const char* text, size_t cursor_position) const {
	bool had_error = false;
	struct stat s;
	char* p;
	char** r = NULL;
	size_t c = 0;
	char* possibility = NULL;
	DIR* d = NULL;
	size_t p_len = 0;
	rl_completion_suppress_append = 1;
	p = (char*) calloc(cursor_position + 1, sizeof(char));
	if(p == NULL) {
		had_error = true;
		goto error;
	}
	memcpy(p, text, cursor_position * sizeof(char));
	p[cursor_position] = '\0';
	p_len = cursor_position;
	
	if(stat(p, &s) == 0) {
		if(S_ISDIR(s.st_mode)) {
			if(p[p_len - 1] == '/') {
				d = opendir(p);
				if(d == NULL) {
					had_error = true;
					goto error;
				}
				errno = 0;
				dirent* e;
				while((e = readdir(d)) != NULL) {
					if(e->d_name[0] == '.') {
						continue;
					}
					size_t name_len = strlen(e->d_name);
					if(e->d_type == DT_DIR) {
						possibility = (char*) calloc(p_len + ++name_len + 1, sizeof(char));
						if(possibility == NULL) {
							had_error = true;
							goto error;
						}
						memcpy(possibility, p, p_len * sizeof(char));
						memcpy(possibility + p_len, e->d_name, name_len * sizeof(char));
						possibility[p_len + name_len - 1] = '/';
						possibility[p_len + name_len] = '\0';
					} else {
						possibility = (char*) calloc(p_len + name_len + 1, sizeof(char));
						if(possibility == NULL) {
							had_error = true;
							goto error;
						}
						memcpy(possibility, p, p_len * sizeof(char));
						memcpy(possibility + p_len, e->d_name, name_len * sizeof(char));
						possibility[p_len + name_len] = '\0';
					}
					if(add_possibility(r, c, possibility)) {
						had_error = true;
						goto error;
					}
					possibility = NULL;
				}
				if(errno != 0) {
					had_error = true;
					goto error;
				}
			} else {
				char* tmp = (char*) realloc(p, p_len + 2);
				if(tmp == NULL) {
					had_error = true;
					goto error;
				}
				p = tmp;
				p[p_len] = '/';
				++p_len;
				p[p_len] = '\0';
				if(add_possibility(r, c, p)) {
					had_error = true;
					goto error;
				}
			}
		} else {
			if(add_possibility(r, c, p)) {
				had_error = true;
				goto error;
			}
		}
	} else {
		// File doesn't exist, so in the way of typing a file name
		char* name_start = strrchr(p, '/');
		size_t name_start_len;
		if(name_start == NULL) {
			name_start = p;
			char* cwd = get_current_working_directory();
			if(cwd == NULL) {
				had_error = true;
				goto error;
			}
			d = opendir(cwd);
			free(cwd);
			p_len = 0;
			name_start_len = strlen(name_start);
		} else {
			char* tmp = (char*) realloc(p, p_len + 2);
			if(tmp == NULL) {
				had_error = true;
				goto error;
			}
			p = tmp;
			++name_start;
			name_start_len = strlen(name_start);
			memmove(name_start + 1, name_start, (name_start_len + 1) * sizeof(char));
			*name_start++ = '\0';
			d = opendir(p);
			p_len -= name_start_len;
		}
		if(d == NULL) {
			had_error = true;
			goto error;
		}
		errno = 0;
		dirent* e;
		while((e = readdir(d)) != NULL) {
			if(name_start[0] != '.' && e->d_name[0] == '.') {
				continue;
			}
			if(strncmp(e->d_name, name_start, name_start_len) == 0) {
				size_t name_len = strlen(e->d_name);
				if(e->d_type == DT_DIR) {
					possibility = (char*) calloc(p_len + ++name_len + 1, sizeof(char));
					if(possibility == NULL) {
						had_error = true;
						goto error;
					}
					memcpy(possibility, p, p_len * sizeof(char));
					memcpy(possibility + p_len, e->d_name, name_len * sizeof(char));
					possibility[p_len + name_len - 1] = '/';
					possibility[p_len + name_len] = '\0';
				} else {
					possibility = (char*) calloc(p_len + name_len + 1, sizeof(char));
					if(possibility == NULL) {
						had_error = true;
						goto error;
					}
					memcpy(possibility, p, p_len * sizeof(char));
					memcpy(possibility + p_len, e->d_name, name_len * sizeof(char));
					possibility[p_len + name_len] = '\0';
				}
				if(add_possibility(r, c, possibility)) {
					had_error = true;
					goto error;
				}
				possibility = NULL;
			}
		}
		if(errno != 0) {
			had_error = true;
			goto error;
		}
	}
error:
	if(p != NULL) {
		free(p);
	}
	if(had_error && r != NULL) {
		for(size_t i = 0; r[i] != NULL; ++i) {
			free(r[i]);
		}
		free(r);
		r = NULL;
	}
	if(d != NULL) {
		closedir(d);
	}
	return r;
}
#else
char** command_file_existing::complete(const char* text, size_t cursor_position) const {
	return NULL;
}

char** command_file_existing::ask_complete(const char* text, size_t cursor_position) const {
	return NULL;
}
#endif
