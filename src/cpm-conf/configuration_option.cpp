/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "configuration_option.hpp"

#include <algorithm>

configuration_option<cpm::logging_levels>::configuration_option() : has() {}
configuration_option<cpm::logging_levels>::configuration_option(const _T& o) : has(), val(o) {
	for(size_t i = 0; i <= (size_t) cpm::logging_level::MAX; ++i) {
		has |= (cpm::logging_level) i;
	}
}
configuration_option<cpm::logging_levels>::configuration_option(_T&& o) : has(), val(std::move(o)) {
	for(size_t i = 0; i <= (size_t) cpm::logging_level::MAX; ++i) {
		has |= (cpm::logging_level) i;
	}
}
configuration_option<cpm::logging_levels>& configuration_option<cpm::logging_levels>::update(const configuration_option<_T> &o) {
	for(size_t i = 0; i <= (size_t) cpm::logging_level::MAX; ++i) {
		if(o.has.isSet((cpm::logging_level) i)) {
			this->has |= (cpm::logging_level) i;
			if(o->isSet((cpm::logging_level) i)){
				this->val |= (cpm::logging_level) i;
			} else {
				this->val.rm((cpm::logging_level) i);
			}
		}
	}
	return *this;
}
configuration_option<cpm::logging_levels>& configuration_option<cpm::logging_levels>::unset() {
	for(size_t i = 0; i <= (size_t) cpm::logging_level::MAX; ++i) {
		has.rm((cpm::logging_level) i);
	}
	return *this;
}
configuration_option<cpm::logging_levels>& configuration_option<cpm::logging_levels>::operator=(_T&& v) {
	val = std::move(v);
	for(size_t i = 0; i <= (size_t) cpm::logging_level::MAX; ++i) {
		has |= (cpm::logging_level) i;
	}
	return *this;
}
configuration_option<cpm::logging_levels>& configuration_option<cpm::logging_levels>::operator=(_T& v) {
	val = v;
	for(size_t i = 0; i <= (size_t) cpm::logging_level::MAX; ++i) {
		has |= (cpm::logging_level) i;
	}
	return *this;
}
configuration_option<cpm::logging_levels>::operator cpm::logging_levels&() {
	return val;
}
configuration_option<cpm::logging_levels>::operator const cpm::logging_levels&() const {
	return val;
}
cpm::logging_levels* configuration_option<cpm::logging_levels>::operator->() {
	return &val;
}
const cpm::logging_levels* configuration_option<cpm::logging_levels>::operator->() const {
	return &val;
}
