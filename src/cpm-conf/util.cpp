/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>

#include "util.hpp"

char* format(const char* format, ...) {
	va_list va_len;
	va_list va_read;
	va_start(va_len, format);
	va_copy(va_read, va_len);
	
	int len = vsnprintf(NULL, 0, format, va_len);
	va_end(va_len);
	if(len < 0) {
		va_end(va_read);
		throw cpm::error_format();
	}
	
	char* r = (char*) calloc(len + 1, sizeof(char));
	len = vsnprintf(r, len + 1, format, va_read);
	va_end(va_read);
	if(len < 0) {
		throw cpm::error_format();
	}
	return r;
}

char* vformat(const char* format, va_list& va_read) {
	va_list va_len;
	va_copy(va_len, va_read);
	
	int len = vsnprintf(NULL, 0, format, va_len);
	va_end(va_len);
	if(len < 0) {
		va_end(va_read);
		throw cpm::error_format();
	}
	
	char* r = (char*) calloc(len + 1, sizeof(char));
	len = vsnprintf(r, len + 1, format, va_read);
	va_end(va_read);
	if(len < 0) {
		throw cpm::error_format();
	}
	return r;
}

bool strprefix(const char* first, const char* second) {
	return strncmp(first, second, strlen(second)) == 0;
}

char* get_current_working_directory() {
	size_t s = 128 * sizeof(char);
	char* buffer = (char*) calloc(s, sizeof(char));
	while(getcwd(buffer, s) == NULL) {
		s *= 2;
		buffer = (char*) realloc(buffer, s);
	}
	size_t len = strlen(buffer);
	if(buffer[len - 1] == '/') {
		buffer[--len] = '\0';
	}
	s = (len + 1) * sizeof(char);
	buffer = (char*) realloc(buffer, s);
	return buffer;
}

char* get_absolute_path(const char* path) {
	char* full = NULL;
	char* cwd = get_current_working_directory();
	size_t cwdlen = strlen(cwd);
	size_t pathlen = strlen(path);
	size_t fulllen = 0;
	if(path[0] == '/') {
		return strdup(path);
	} else {
		fulllen = cwdlen + 1 + pathlen;
		full = (char*) malloc(fulllen + 1);
		memcpy(full, cwd, cwdlen);
		full[cwdlen] = '/';
		memcpy(full + cwdlen + 1, path, pathlen + 1);
	}
	
	char* at = full;
	char* previous = at + 1;
	while(at != NULL) {
		++at;
		if(*at == '\0') {
			break;
		}
		if(strprefix(at, "./")) {
			memmove(at, at + 2, (strlen(at + 2) + 1) * sizeof(char));
			--at;
		} else if(strprefix(at, "../")) {
			memmove(previous, at + 3, (strlen(at + 3) + 1) * sizeof(char));
			at = previous - 1;
			--previous;
			if(previous != full) {
				do {
					--previous;
				} while(*previous != '/');
			}
			++previous;
		} else {
			previous = at;
			at = strchr(at, '/');
		}
	}
	
	free(cwd);
	full = (char*) realloc(full, strlen(full) * sizeof(char));
	return full;
}
