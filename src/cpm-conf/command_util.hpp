/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BIN_CPM_CONF_COMMAND_UTIL_HPP
#define BIN_CPM_CONF_COMMAND_UTIL_HPP

#include "config.hpp"

#include "command.hpp"

char* token_return_skip_whitespace(char* str);

#ifdef WITH_READLINE
extern const command* ask_completor;
char** ask_completion_function(const char*, int, int);
bool add_possibility(char**& to, size_t& count, char* possibility);
#endif

#endif
