/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_UTIL_HPP
#define CPM_UTIL_HPP

#include <stdarg.h>

#include "cpm.hpp"

/*!
 * @brief A function for formatting c style strings.
 * 
 * Formats strings like printf.
 * 
 * @param format The format string.
 * 
 * @return The formatted c style string.
 */
char* format(const char* format, ...);

/*!
 * @brief A function for formatting c style strings.
 * 
 * Formats strings like vprintf.
 * 
 * @param format The format string.
 * @param l The va_list.
 * 
 * @return The formatted c style string.
 */
char* vformat(const char* format, va_list& l);

/*!
 * @brief A function for comparing the next characters.
 * 
 * Currently not in the public API.
 * 
 * This returns true if the first c style string given starts
 * with the second.
 * 
 * @param first The string which should start with the second.
 * @param second The prefix the first string should start with.
 * 
 * @return True if first starts with second, false otherwise.
 */
bool strprefix(const char *first, const char *second);

char* get_current_working_directory();

char* get_absolute_path(const char* path);

#endif
