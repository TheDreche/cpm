/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BIN_CPM_CONF_PASSWDENTRY_HPP
#define BIN_CPM_CONF_PASSWDENTRY_HPP

/*!
 * @file
 * @brief This file is for reading /etc/passwd.
 */

#include <stddef.h>

/*!
 * @brief Returns an entry of a user in /etc/passwd.
 * 
 * Returns the specified entry.
 * 
 * Entrys of a line are separated by :. Each line starts with the
 * entry for the user name.
 * 
 * If @ref entry is `(size_t) -1`, return the whole line. Otherwise, 
 * return the correct entry.
 * 
 * On error, returns NULL.
 * 
 * @param entry The number of the entry.
 * @return The entry, the whole line if entry is `(size_t) -1` and NULL on error.
 */
char* get_entry_of_user(size_t entry, const char* user);

#endif
