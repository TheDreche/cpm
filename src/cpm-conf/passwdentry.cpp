/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#include <stdio.h>
#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif
#ifdef HAVE_STRING_H
# include <string.h>
#endif

#include "passwdentry.hpp"

char* get_entry_of_user(size_t entry, const char* user) {
	size_t user_len = strlen(user);
	
	FILE* fd = fopen(ETCPASSWD, "r");
	if(fd == NULL) {
		return NULL;
	}
	char* line = NULL;
	size_t line_size = 0;
	while(!feof(fd)) {
		getline(&line, &line_size, fd);
		if(line_size >= user_len && strncmp(line, user, user_len) == 0 && line[user_len] == ':') {
			// We have found the correct line.
			if(entry == (size_t) -1) {
				return line;
			} else {
				char* original_line = line;
				for(size_t i = 0; i < entry; ++i) {
					line = strchr(line, ':');
					if(line == NULL) {
						free(line);
						return NULL;
					}
					++line;
				}
				char* entry_end = strchr(line, ':');
				if(entry_end == NULL) {
					entry_end = line + strlen(line) - 1 /* \n */;
				}
				size_t entry_size = entry_end - line;
				memmove(original_line, line, entry_size);
				original_line[entry_size] = '\0';
				line = (char*) realloc(original_line, (entry_size + 1) * sizeof(char));
				return line;
			}
		}
	}
	free(line);
	return NULL;
}
