/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BIN_CPM_CONF_CONFIGURATION_HPP
#define BIN_CPM_CONF_CONFIGURATION_HPP

#include <vector>
#include <string>

#include "cpm.hpp"

#include "common.hpp"
#include "configuration_option.hpp"

class configuration {
	public:
		enum action {
			PARSE,
			INTERACTIVE,
			PRINT_HELP,
			PRINT_VERSION
		};
		configuration_option<std::string> prompt;
		configuration_option<std::string> prompt_question;
		configuration_option<cpm::context> cpm_context;
		configuration_option<cpm::configuration> cpm_configuration;
		configuration_option<std::string> self;
		configuration_option<std::string> readline_name;
		configuration_option<action> act;
		configuration_option<std::string> tree_indent;
		configuration_option<std::string> tree_indent_post_last;
		configuration_option<std::string> tree_node;
		configuration_option<std::string> tree_node_last;
		std::vector<std::string> files;
		configuration_option<bool> exit = false;
		configuration_option<cpm::logging_levels> mask;
		void update(const configuration& c);
		void setLog(const char* changes);
};

#endif
