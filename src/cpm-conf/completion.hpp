/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BIN_CPM_CONF_COMPLETION_HPP
#define BIN_CPM_CONF_COMPLETION_HPP

#include "configuration.hpp"

extern configuration* cpmconf_rl_completion_configuration;
char** complete_full_command(const char* word, int start, int end);
int full_command_is_quoted(char* text, int position);

#endif
