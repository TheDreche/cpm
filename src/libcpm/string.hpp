/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_STRING_HPP
#define CPM_STRING_HPP

/*!
 * @file
 * @brief Defines the @ref cpm::string class.
 */

#include <stddef.h>

namespace cpm {
	class string;
}

namespace cpm {
	/*!
	 * @brief A C compatible string.
	 */
	class string {
		public:
			/*!
			 * @brief The character type used.
			 * 
			 * Will always stay char.
			 */
			typedef char char_t;
			
			/*!
			 * @brief The non-const iterator type used.
			 * 
			 * Will always stay @ref char_t*.
			 * 
			 * @sa char_t
			 * @sa const_iterator
			 */
			typedef char_t* iterator;
			
			/*!
			 * @brief The const iterator type used.
			 * 
			 * Will always stay const @ref char_t*.
			 * 
			 * @sa char_t
			 * @sa iterator
			 */
			typedef const char_t* const_iterator;
			
			/*!
			 * @brief The size type used.
			 * 
			 * Will always stay size_t.
			 */
			typedef size_t size_type;
		private:
			/*!
			 * @brief The C style string.
			 * 
			 * NULL means empty string.
			 */
			char_t* c_str = NULL;
			
			/*!
			 * @brief The allocated space.
			 * 
			 * Counted without null termination character.
			 */
			size_type reserved = 0;
		public:
			/*!
			 * @brief An empty string.
			 */
			string();
			
			/*!
			 * @brief Create a substring.
			 * 
			 * Will copy the (sub)string before using it.
			 * 
			 * @param cstr The C string to copy the beginning of.
			 * @param len The length of the substring.
			 */
			string(const char_t* cstr, size_type len);
			
			/*!
			 * @brief A c string wrapper.
			 * 
			 * Will copy the string before using it.
			 * 
			 * @param cstr The C string to copy.
			 */
			string(const char_t* cstr);
			
			/*!
			 * @brief A c string wrapper.
			 * 
			 * Takes the reference, so you mustn't free it.
			 * 
			 * @param cstr The C string to take reference over.
			 */
			string(char_t* cstr);
			
			/*!
			 * @brief Copy constructor.
			 * 
			 * Copies another @ref string.
			 * 
			 * @param o The @ref string to copy.
			 * 
			 * @throw error_errno When strdup fails.
			 * @sa cpm::string::operator=(const cpm::string&)
			 */
			string(const string& o);
			
			/*!
			 * @brief Move constructor.
			 * 
			 * Moves another @ref string into this one, replacing this.
			 * 
			 * It is faster than copying because an allocation can be saved.
			 * Also, it may save some exceptions.
			 * 
			 * The moved string is empty afterwards.
			 * 
			 * @param o The string to move.
			 * @sa cpm::string::operator=(cpm::string&&)
			 */
			string(string&& o);
			
			~string();
			
			
			/*!
			 * @brief Assigns this to something else.
			 * 
			 * Copies another string into this string.
			 * 
			 * @param o The @ref string to copy.
			 * @return A reference to this.
			 * @throw error_errno When strdup fails.
			 * @sa cpm::string::string(const cpm::string&)
			 */
			string& operator=(const string& o);
			
			/*!
			 * @brief Assigns this to something else by moving.
			 * 
			 * Moves another string into this string.
			 * 
			 * @param o The string to be moved.
			 * @return A reference to this.
			 * @sa cpm::string::string(cpm::string&&)
			 */
			string& operator=(string&& o);
			
			/*!
			 * @brief Converts this to a C string.
			 * 
			 * Can be used for passing to C string functions.
			 * 
			 * @sa cpm::string::operator const cpm::string::char_t*()const
			 */
			operator char_t*();
			
			/*!
			 * @brief Converts this to a constant C string.
			 * 
			 * Can be used for passing to C string functions.
			 * 
			 * @sa cpm::string::operator cpm::string::char_t*()
			 */
			operator const char_t*() const;
			
			
			/*!
			 * @brief Appends another string to this one.
			 * 
			 * @param o The string to be appended.
			 * @return A reference to this.
			 * @throw error_errno When reallocation fails.
			 * @sa cpm::string::operator+(const cpm::string&)const
			 * @sa cpm::string::operator*=(cpm::string::size_type)
			 */
			string& operator+=(const string& o);
			
			/*!
			 * @brief Repeats this string count times.
			 * 
			 * @param count The number of times to repeat.
			 * @return A reference to this.
			 * @throw error_errno When reallocation fails.
			 * @sa cpm::string::operator*(cpm::string::size_type)
			 * @sa cpm::string::operator+=(const cpm::string&)
			 */
			string& operator*=(size_type count);
			
			/*!
			 * @brief Joins two strings.
			 * 
			 * @param o The string to join with this one.
			 * @return The joined @ref cpm::string.
			 * @throw error_errno When allocation fails.
			 * @sa cpm::string::operator+=(const cpm::string&)
			 * @sa cpm::string::operator*(cpm::string::size_type)
			 */
			string  operator+ (const string& o) const;
			
			/*!
			 * @brief Repeats a string.
			 * 
			 * @param count How many times to repeat this string.
			 * @return A @ref cpm::string containing this string count times.
			 * @throw error_errno When allocation fails.
			 * @sa cpm::string::operator*=(cpm::string::size_type)
			 * @sa cpm::string::operator+(const cpm::string&)
			 */
			string  operator* (size_type count) const;
			
			
			/*!
			 * @brief The character at a position.
			 * 
			 * @param pos The position of the character to return.
			 * @return The character at pos.
			 * @sa cpm::string::operator[](cpm::string::size_type pos)const
			 */
			char_t& operator[](size_type pos);
			
			/*!
			 * @brief The (const) character at position.
			 * 
			 * @param pos The position of the character to return.
			 * @return The character at pos.
			 * @sa cpm::string::operator[](cpm::string::size_type)
			 */
			const char_t& operator[](size_type pos) const;
			
			
			/*!
			 * @brief An @ref iterator at the beginning.
			 * 
			 * @return An @ref iterator at the beginning.
			 * @sa cpm::string::begin()
			 * @sa cpm::string::cbegin()const
			 * @sa cpm::string::end()
			 * @sa cpm::string::cend()const
			 */
			iterator begin();
			
			/*!
			 * @brief A @ref const_iterator at the beginning.
			 * 
			 * @return A @ref const_iterator at the beginning.
			 * @sa cpm::string::begin()
			 * @sa cpm::string::cbegin()const
			 * @sa cpm::string::end()
			 * @sa cpm::string::cend()const
			 */
			const_iterator cbegin() const;
			
			/*!
			 * @brief An @ref iterator past the end.
			 * 
			 * @return An @ref iterator past the end.
			 * @sa cpm::string::begin()
			 * @sa cpm::string::cbegin()const
			 * @sa cpm::string::end()
			 * @sa cpm::string::cend()const
			 */
			iterator end();
			
			/*!
			 * @brief A @ref const_iterator past the end.
			 * 
			 * @return A @ref const_iterator past the end.
			 * @sa cpm::string::begin()
			 * @sa cpm::string::cbegin()const
			 * @sa cpm::string::end()
			 * @sa cpm::string::cend()const
			 */
			const_iterator cend() const;
			
			
			/*!
			 * @brief The length of this string.
			 * 
			 * The null termination character is excluded.
			 */
			size_type size() const;
			
			/*!
			 * @brief How many characters can be saved without reallocation.
			 * 
			 * @sa cpm::string::reserve(cpm::string::size_type)
			 * @sa cpm::string::shrink_to_fit()
			 */
			size_type capacity() const;
			
			/*!
			 * @brief Reserve space for letters characters.
			 * 
			 * The null termination character is excluded in count.
			 * 
			 * @param letters How many characters should be savable.
			 * @throw error_errno When reallocation fails.
			 * @sa cpm::string::capacity()const
			 */
			void reserve(size_type letters);
			
			
			/*!
			 * @brief Reserve just the space needed.
			 * 
			 * @throw error_errno When reallocation fails.
			 * @sa cpm::string::capacity()const
			 */
			void shrink_to_fit();
			
			/*!
			 * @brief Pass to C code.
			 * 
			 * Returns a C string.
			 * 
			 * This is useful when C code calls C++ code and
			 * expects a modifyable stringto be returned.
			 * 
			 * Gives reference to the pointer away. The caller
			 * has to make sure the space gets freed.
			 */
			char_t* toApi();
			
			/*!
			 * @brief The last character.
			 * 
			 * Undefined if string is empty.
			 * 
			 * @return A reference to the last character in the string.
			 */
			char_t& back();
			
			/*!
			 * @brief The last const character.
			 * 
			 * Undefined if string is empty.
			 * 
			 * @return A const reference to the last character in the string.
			 */
			const char_t& back() const;
			
			/*!
			 * @brief Delete the last character.
			 * 
			 * Undefined if string is empty.
			 * 
			 * @return The previous last character.
			 */
			char_t pop_back();
	};
}

#endif
