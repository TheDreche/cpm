/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_NEW_HPP
#define CPM_NEW_HPP

#include <stddef.h>

namespace cpm {
	/*!
	 * @brief Tag for using the cpm-specific allocator.
	 */
	class tag_t {};
	
	/*!
	 * @brief The tag constant.
	 * 
	 * Use this when using the tag @ref cpm::tag_t.
	 */
	const tag_t tag;
}

/*!
 * @brief cpm-specific allocator.
 * 
 * It is based on the C library implementation.
 */
void* operator new(size_t s, cpm::tag_t);

/*!
 * @brief cpm-specific array allocator.
 * 
 * Calls @ref operator new(size_t, cpm::tag_t)
 */
void* operator new[](size_t s, cpm::tag_t);

#endif
