/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cpm/backtrace.hpp"

namespace cpm {
	// backtrace::const_iterator
	backtrace::const_iterator::const_iterator() : at(NULL) {}
	backtrace::const_iterator::const_iterator(const backtrace& over) : at(&over) {}
	backtrace::const_iterator& backtrace::const_iterator::operator++() {
		if(at != NULL) {
			at = at->called_by;
		}
		return *this;
	}
	backtrace::const_iterator backtrace::const_iterator::operator++(int) {
		backtrace::const_iterator r = *this;
		++*this;
		return r;
	}
	const backtrace& backtrace::const_iterator::operator*() {
		return *at;
	}
	const backtrace* backtrace::const_iterator::operator->() {
		return at;
	}
	
	// backtrace
	backtrace::const_iterator backtrace::cbegin() const {
		return const_iterator(*this);
	}
	backtrace::const_iterator backtrace::cend() const {
		return const_iterator();
	}
}
