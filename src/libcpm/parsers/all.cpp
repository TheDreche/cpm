/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#include <vector>

#ifdef ENABLE_STD_EAPI
# include "portage/repos.hpp"
#endif

#include "all.hpp"
#include "configuration.hpp"
#include "file_parser.hpp"

namespace cpm {
	namespace parsers {
		namespace all {
			std::vector<cpm::file_parser<cpm::configuration>> configuration_file_parsers() {
				std::vector<cpm::file_parser<cpm::configuration>> r;
#ifdef ENABLE_STD_EAPI
				r.emplace_back(&cpm::parsers::portage::repo);
#endif
				return r;
			}
		}
	}
}
