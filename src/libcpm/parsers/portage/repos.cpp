/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif

#include "repos.hpp"
#include "new.hpp"
#include "parsers/ini.hpp"
#include "standards/eapi8/names.hpp"
#include "standards/eapi8/pkgnodes.hpp"
#include "util.hpp"

namespace cpm {
	namespace parsers {
		namespace portage {
			file_parser_correctness repo(FILE* stream, configuration& c, pass_log&& p) {
				log log = LOG_INIT(p);
				string msg;
				file_parser_correctness r;
				LOG_MESSAGE(log, DEBUG, "Parsing as portage repository\n");
				cpm::parsers::ini::ini parsed_ini = cpm::parsers::ini::parse(stream, r, LOG_PASS(log));
				for(const std::pair<std::string, cpm::parsers::ini::section>& s : parsed_ini) {
					LOG_MESSAGE(log, DEBUG, "Parsing repository %s\n", s.first.c_str());
					r.variable_use();
					auto pos = s.second.find("location");
					if(pos != s.second.end()) {
						r.variable_use();
						cpm::standards::eapi8::repo_name name = s.first.c_str();
						cpm::standards::eapi8::repository_form form = {
							.location = pos->second.c_str(),
							.nameUpstream = name
						};
						cpm::standards::eapi8::repository* repo = new(cpm::tag) cpm::standards::eapi8::repository(form);
						c.repositories.push_back(repo);
						LOG_MESSAGE(log, DEBUG, "Repository %s has location %s\n", s.first.c_str(), pos->second.c_str());
					}
				}
				LOG_MESSAGE(log, DEBUG, "Now there are %zu repositories\n", c.repositories.size());
				return r;
			}
		}
	}
}
