/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#include <stddef.h>
#ifdef HAVE_STRING_H
# include <string.h>
#endif

#include <algorithm>
#include <fstream>

#include "ini.hpp"
#include "charset.hpp"
#include "logging.hpp"
#include "util.hpp"

namespace cpm {
	namespace parsers {
		namespace ini {
			ini parse(FILE* stream, file_parser_correctness& c, pass_log&& p) {
				log log = LOG_INIT(p);
				ini r;
				std::string section_name;
				section sect;
				char* line = NULL;
				size_t line_size = 0;
				const static charset variable_chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+_.-@0123456789";
				const static charset whitespace = " \f\n\r\t\v"; // Copied from isspace(3)
				
				while(getline(&line, &line_size, stream) >= 0) {
					// Find first non-whitespace character
					char* first = line;
					while(whitespace.contains(*first) && *first != '\0') {
						++first;
					}
					
					if(*first == '\0') {
						// empty line
						continue;
					}
					
					size_t maxindex = strlen(first);
					while(maxindex > 0 && whitespace.contains(first[--maxindex])) {}
					if(first[0] == '[' && first[maxindex] == ']') {
						// [section]
						// For now, sections can contain any name
						r[std::move(section_name)] = std::move(sect);
						first[maxindex] = '\0';
						section_name = first + 1;
						continue;
					}
					
					if(
						*first == '#' ||
						*first == ';' ||
						(first[0] == '/' && first[1] == '/')
					) {
						// comment
						continue;
					}
					
					if(strchr(first, '=') != NULL) {
						// var = value
						
						// Variable name begin
						char* var_beg = first;
						
						// Variable name end
						char* var_end = var_beg;
						do {
							++var_end;
						} while(variable_chars.contains(*var_end) && *var_end != '\0');
						
						// Value begin
						char* val_beg = var_end;
						while(*val_beg != '=' && *val_beg != '\0' && whitespace.contains(*val_beg)) {
							++val_beg;
						}
						if(*val_beg == '\0') {
							// String terminated too early
							c.syntax_error();
							LOG_MESSAGE(log, CONF_WARN, "Syntax error: %s\nMissing =\n", line);
							// Skip line
							continue;
						} else if(*val_beg != '=') {
							// Invalid syntax like
							// varname sth = value
							// varname:= value
							c.syntax_error();
							LOG_MESSAGE(log, CONF_WARN, "Syntax error: %s\nInvalid symbols before =\n", line);
							continue;
						}
						// Just had =, skip whitespace
						do {
							++val_beg;
						} while(whitespace.contains(*val_beg) && *val_beg != '\0');
						
						// Value end
						char* val_end = val_beg + strlen(val_beg);
						do {
							--val_end;
						} while(whitespace.contains(*val_end));
						++val_end;
						
						// Mark ends
						*var_end = '\0';
						*val_end = '\0';
						
						// Saving
						sect[var_beg] = val_beg;
					} else {
						// Invalid syntax such as
						// abc def
						c.syntax_error();
						LOG_MESSAGE(log, CONF_WARN, "Syntax error: %s\nInvalid line\n", line);
						// Skip line
						continue;
					}
				}
				r[std::move(section_name)] = std::move(sect);
				return r;
			}
		}
	}
}
