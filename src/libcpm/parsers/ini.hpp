/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_PARSERS_RAW_INI_HPP
#define CPM_PARSERS_RAW_INI_HPP

#include <stdio.h>

#include <string>
#include <unordered_map>

#include "file_parser_correctness.hpp"
#include "logging.hpp"
#include "pass_log.hpp"

namespace cpm {
	namespace parsers {
		namespace ini {
			typedef std::unordered_map<std::string, std::string> section;
			typedef std::unordered_map<std::string, section> ini;
			
			/*!
			 * @brief Parse an ini file.
			 * 
			 * It is the callers responsibility to open the file.
			 * 
			 * @param stream The stream to read the file from.
			 * @return The parsed ini structure.
			 */
			ini parse(FILE* f, file_parser_correctness& c, pass_log&& p);
		}
	}
}

#endif
