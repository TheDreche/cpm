/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#include <errno.h>
#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif
#ifdef HAVE_STRING_H
# include <string.h>
#endif

#include "string.hpp"
#include "errors.hpp"

namespace {
	cpm::string::size_type string_reserve(cpm::string::char_t*& prev, cpm::string::size_type chars) {
		int errno_save = errno;
		errno = 0;
		
		cpm::string::char_t* tmp = (cpm::string::char_t*) realloc(prev, chars + 1);
		if(tmp == NULL) {
			int throw_errno = errno;
			errno = errno_save;
			throw cpm::error_errno(errno);
		}
		prev = tmp;
		
		errno = errno_save;
		return chars;
	}
}

namespace cpm {
	string::string() {}
	string::string(const char_t* cstr, size_type len) {
		reserve(len);
		strncpy(c_str, cstr, len);
		c_str[len] = '\0';
	}
	string::string(const char_t* cstr) {
		auto errno_save = errno;
		errno = 0;
		c_str = strdup(cstr);
		if(c_str == NULL) {
			auto errno_throw = errno;
			errno = errno_save;
			throw error_errno(errno_throw);
		}
		reserved = strlen(c_str);
		errno = errno_save;
	}
	string::string(char_t* cstr) : c_str(cstr), reserved(strlen(cstr)) {}
	string::string(const string& o) {
		int errno_save = errno;
		errno = 0;
		c_str = strdup(o.c_str);
		if(c_str == NULL) {
			int throw_errno = errno;
			errno = errno_save;
			throw error_errno(throw_errno);
		}
		reserved = strlen(c_str);
		errno = errno_save;
	}
	string::string(string&& o) {
		c_str = o.c_str;
		reserved = o.reserved;
		
		o.c_str = NULL;
		o.reserved = 0;
	}
	string::~string() {
		if(c_str != NULL) {
			free(c_str);
		}
	}
	string& string::operator=(const string& o) {
		int errno_save = errno;
		errno = 0;
		if(c_str != NULL) {
			free(c_str);
		}
		c_str = strdup(o.c_str);
		if(c_str == NULL) {
			int throw_errno = errno;
			errno = errno_save;
			throw error_errno(throw_errno);
		}
		reserved = strlen(c_str);
		errno = errno_save;
		return *this;
	}
	string& string::operator=(string&& o) {
		if(c_str != NULL) {
			free(c_str);
		}
		c_str = o.c_str;
		reserved = o.reserved;
		
		o.c_str = NULL;
		o.reserved = 0;
		
		return *this;
	}
	string::operator char_t*() {
		return c_str;
	}
	string::operator const char_t*() const {
		return c_str;
	}
	string& string::operator+=(const string& o) {
		size_type o_len = o.size();
		size_type len = size();
		reserve(len + o_len);
		memcpy(&c_str[len], o.c_str, o_len * sizeof(char));
		len += o_len;
		c_str[len] = '\0';
		return *this;
	}
	string& string::operator*=(size_type count) {
		size_type len = size();
		reserve(count * len);
		for(size_type i = 1; i < count; ++i) {
			memcpy(&c_str[i * len], c_str, len * sizeof(char));
		}
		len *= count;
		c_str[len] = '\0';
		return *this;
	}
	string string::operator+(const string& o) const {
		string r;
		size_type len = size();
		size_type o_len = o.size();
		r.reserve(len + o.size());
		memcpy(r.c_str, c_str, len * sizeof(char));
		memcpy(&r.c_str[len], o.c_str, o.size() * sizeof(char));
		r.c_str[len + o_len] = '\0';
		return r;
	}
	string string::operator*(size_type count) const {
		string r;
		size_type len = size();
		r.reserve(count * len);
		for(size_type i = 0; i < count; ++i) {
			memcpy(&r.c_str[i * len], c_str, len * sizeof(char));
		}
		r.c_str[count * len] = '\0';
		return r;
	}
	string::char_t& string::operator[](size_type pos) {
		return c_str[pos];
	}
	const string::char_t& string::operator[](size_type pos) const {
		return c_str[pos];
	}
	string::iterator string::begin() {
		return c_str;
	}
	string::const_iterator string::cbegin() const {
		return c_str;
	}
	string::iterator string::end() {
		return begin() + size();
	}
	string::const_iterator string::cend() const {
		return cbegin() + size();
	}
	string::size_type string::size() const {
		return strlen(c_str);
	}
	string::size_type string::capacity() const {
		return reserved;
	}
	void string::reserve(size_type letters) {
		if(reserved < letters) {
			reserved = string_reserve(c_str, letters);
		}
	}
	void string::shrink_to_fit() {
		if(size() != capacity()) {
			reserved = string_reserve(c_str, size());
		}
	}
	string::char_t* string::toApi() {
		shrink_to_fit();
		char_t* r = c_str;
		c_str = NULL;
		return r;
	}
	const string::char_t& string::back() const {
		return c_str[size() - 1];
	}
	string::char_t& string::back() {
		return c_str[size() - 1];
	}
	string::char_t string::pop_back() {
		size_type len = size();
		--len;
		char_t r = c_str[len];
		c_str[len] = '\0';
		return r;
	}
}
