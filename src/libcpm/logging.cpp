/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdarg.h>

#include <vector>
#include <algorithm>

#include "logging.hpp"
#include "string.hpp"
#include "util.hpp"

#define NULLNULL(ptr, value) ((ptr) == NULL? NULL : ptr->value)

namespace cpm {
	pass_log::pass_log(const log& l, int line) : l(&l), line(line), is_top(false) {}
	pass_log::pass_log(const context& ctx, int line) : ctx(&ctx), line(line), is_top(true) {}
	
	log::log(const context& ctx, const char* function_name, const char* file_name)
		: ctx(ctx)
		, function_name(function_name)
		, file_name(file_name)
		, called_by()
		, is_top(true) {}
	log::log(const pass_log& p, const char* function_name, const char* file_name)
		: ctx(p.is_top? *p.ctx : p.l->ctx)
		, function_name(function_name)
		, file_name(file_name)
		, called_by(p.is_top? backtrace() : backtrace {
			.file = p.l->file_name,
			.line = p.line,
			.function = p.l->function_name,
			.called_by = &p.l->called_by
		})
		, is_top(p.is_top) {}
	void log::message(int line, logging_level level, const char* message, ...) {
		va_list params;
		va_start(params, message);
		string msg = vformat(message, params);
		va_end(params);
		backtrace bt = {
			.file = file_name,
			.line = line,
			.function = function_name,
			.called_by = is_top? NULL : &called_by,
		};
		logging_message l = {
			.message = msg,
			.level = level,
			.time = time(NULL),
			.bt = bt
		};
		ctx.logging_function(l);
	}
}
