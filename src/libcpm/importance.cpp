/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cpm/importance.hpp"
#include "util.hpp"

namespace cpm {
	importance::importance() {}
	importance::importance(const char* s) {
		char l;
		while((l = *s) != '\0') {
			if(strprefix_mv(s, "upper")) {
				upper();
				continue;
			} else if(strprefix_mv(s, "lower")) {
				lower();
				continue;
			} else if(l == '+') {
				upper();
			} else if(l == '-') {
				lower();
			}
			++s;
		}
	}
	importance& importance::lower() {
		suffixes.push_back(false);
		return *this;
	}
	importance& importance::upper() {
		suffixes.push_back(true);
		return *this;
	}
	bool importance::operator<(const importance &o) const {
		auto iter1 = this->suffixes.begin();
		auto iter1_end = this->suffixes.end();
		auto iter2 = o.suffixes.begin();
		auto iter2_end = o.suffixes.end();
		bool suff1;
		bool suff2;
		bool end1 = false;
		bool end2 = false;
		while(true) {
			if(iter1 == iter1_end) {
				end1 = true;
			}
			if(iter2 == iter2_end) {
				end2 = true;
			}
			if(end1 || end2) {
				break;
			}
			if((suff1 = *iter1) != (suff2 = *iter2)) {
				return suff2;
			}
			++iter1;
			++iter2;
		}
		if(end1 && end2) {
			return false;
		} else {
			if(!end1) {
				++iter1;
				return !*iter1;
			} else { // !end2
				++iter2;
				return *iter2;
			}
		}
	}
	bool importance::operator>(const importance &o) const {
		auto iter1 = this->suffixes.begin();
		auto iter1_end = this->suffixes.end();
		auto iter2 = o.suffixes.begin();
		auto iter2_end = o.suffixes.end();
		bool suff1;
		bool suff2;
		bool end1 = false;
		bool end2 = false;
		while(true) {
			if(iter1 == iter1_end) {
				end1 = true;
			}
			if(iter2 == iter2_end) {
				end2 = true;
			}
			if(end1 || end2) {
				break;
			}
			if((suff1 = *iter1) != (suff2 = *iter2)) {
				return suff1;
			}
			++iter1;
			++iter2;
		}
		if(end1 && end2) {
			return false;
		} else {
			if(!end1) {
				++iter1;
				return *iter1;
			} else { // !end2
				++iter2;
				return !*iter2;
			}
		}
	}
	bool importance::operator==(const importance &o) const {
		return suffixes == o.suffixes;
	}
	bool importance::operator!=(const importance &o) const {
		return suffixes != o.suffixes;
	}
	bool importance::operator<=(const importance &o) const {
		return *this == o || *this < o;
	}
	bool importance::operator>=(const importance &o) const {
		return *this == o || *this > o;
	}
	importance& importance::operator++() {
		return upper();
	}
	importance& importance::operator--() {
		return lower();
	}
	importance& importance::operator++(int) {
		return upper();
	}
	importance& importance::operator--(int) {
		return lower();
	}
}
