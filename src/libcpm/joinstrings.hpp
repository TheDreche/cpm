/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_JOINSTRINGS_HPP
#define CPM_JOINSTRINGS_HPP

#include <stdarg.h>
#include <wchar.h>

namespace cpm {
	/*!
	 * @brief Join strings.
	 * 
	 * Joins many strings easily.
	 * 
	 * The list of arguments has to be terminated with NULL.
	 * 
	 * @param a The first string to join.
	 * @return The joined string.
	 * @throw error_errno See @ref error_errno.
	 * @sa joinstrings(wchar_t* a, ...)
	 */
	char* joinstrings(const char* a, ...);
	
	/*!
	 * @brief Join wide strings.
	 * 
	 * Joins many wide strings easily.
	 * 
	 * The list of arguments has to be terminated with NULL.
	 * 
	 * @param a The first wide string to join.
	 * @return The joined string.
	 * @throw error_errno See @ref error_errno.
	 * @sa joinstrings(char* a, ...)
	 */
	wchar_t* joinstrings(const wchar_t* a, ...);
	
	/*!
	 * @brief Join strings in place.
	 * 
	 * Joins many strings easily.
	 * 
	 * The list of arguments has to be terminated with NULL.
	 * 
	 * @param a The first string to join.
	 * @return The joined string.
	 * @throw error_errno See @ref error_errno.
	 * @sa joinstrings(wchar_t* a, ...)
	 */
	char* ijoinstrings(char* a, ...);
	
	/*!
	 * @brief Join wide strings in place.
	 * 
	 * Joins many wide strings easily.
	 * 
	 * The list of arguments has to be terminated with NULL.
	 * 
	 * @param a The first wide string to join.
	 * @return The joined string.
	 * @throw error_errno See @ref error_errno.
	 * @sa joinstrings(char* a, ...)
	 */
	wchar_t* ijoinstrings(wchar_t* a, ...);
}

#endif
