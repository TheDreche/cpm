/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "errors.hpp"

namespace cpm {
	const char* error::what() const noexcept {
		return "An exception happened in libcpm.";
	}
	const char* error_errno::what() const noexcept {
		return strerror(error_number);
	}
	const char* error_format::what() const noexcept {
		return "An error occurred at formatting a format string.";
	}
	const char* error_nostdsupport::what() const noexcept {
		return "The standard doesn't support this operation.";
	}
	const char* error_file_open::what() const noexcept {
		return "Could not open file.";
	}
	const char* error_file_format::what() const noexcept {
		return "No file parser has succeeded.";
	}
	
	error_errno::error_errno() : error_number(0) {}
	error_errno::error_errno(int error_number) : error_number(error_number) {}
}

