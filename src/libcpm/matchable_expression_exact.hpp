/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBCPM_MATCHABLE_EXPRESSION_EXACT_HPP
#define LIBCPM_MATCHABLE_EXPRESSION_EXACT_HPP

#include "matchable_expression.hpp"

namespace cpm {
	class matchable_expression_exact;
	
	/*!
	 * @brief An expression matching a single string.
	 */
	class matchable_expression_exact : public matchable_expression {
		public:
			/*!
			 * @brief The single matching string.
			 */
			const char* match;
			
			bool matches(const char* to) const;
			
			char* repr() const;
	};
}

#endif
