/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "matchable_expression_exact.hpp"
#include "string.hpp"

namespace cpm {
	bool matchable_expression_exact::matches(const char* to) const {
		return strcmp(match, to) == 0;
	}
	
	char* matchable_expression_exact::repr() const {
		return string(match).toApi();
	}
}
