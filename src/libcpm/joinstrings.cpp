/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#include <errno.h>
#include <stdarg.h>
#include <stddef.h>
#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif
#ifdef HAVE_STRING_H
# include <string.h>
#endif
#include <wchar.h>

#include <algorithm>

#include "joinstrings.hpp"
#include "errors.hpp"

namespace cpm {
	char* joinstrings(const char* a, ...) {
		if(a == NULL) {
			char* r = strdup("");
			if(r == NULL) {
				throw error_errno(errno);
			}
			return r;
		}
		
		char* result = NULL;
		size_t len = strlen(a);
		va_list list_read;
		va_list list_append;
		const char* current;
		char* at;
		size_t current_len;
		
		va_start(list_read, a);
		va_copy(list_append, list_read);
		
		while((current = va_arg(list_read, const char*)) != NULL) {
			len += strlen(current);
		}
		va_end(list_read);
		result = (char*) calloc(len + 1 /* \0 */, sizeof(char));
		if(result == NULL) {
			throw error_errno(errno);
		}
		at = result;
		
		current = a;
		do {
			current_len = strlen(current);
			memcpy(at, current, current_len * sizeof(char));
			at += current_len;
		} while((current = va_arg(list_append, const char*)) != NULL);
		*at = '\0';
		va_end(list_append);
		return result;
	}
	
	wchar_t* joinstrings(const wchar_t* a, ...) {
		if(a == NULL) {
			wchar_t* r = wcsdup(L"");
			if(r == NULL) {
				throw error_errno(errno);
			}
			return r;
		}
		
		wchar_t* result = NULL;
		size_t len = wcslen(a);
		va_list list_read;
		va_list list_append;
		const wchar_t* current;
		wchar_t* at;
		
		va_start(list_read, a);
		va_copy(list_append, list_read);
		
		while((current = va_arg(list_read, const wchar_t*)) != NULL) {
			len += wcslen(current);
		}
		va_end(list_read);
		result = (wchar_t*) calloc(len, sizeof(wchar_t));
		if(result == NULL) {
			throw error_errno(errno);
		}
		at = result;
		
		current = a;
		do {
			size_t current_len = wcslen(current);
			memcpy(at, current, current_len * sizeof(wchar_t));
			at += current_len;
		} while((current = va_arg(list_append, const wchar_t*)) != NULL);
		*at = L'\0';
		va_end(list_append);
		return result;
	}
	
	char* ijoinstrings(char* a, ...) {
		char* result = NULL;
		size_t len = strlen(a);
		va_list list_read;
		va_list list_append;
		const char* current;
		char* at;
		
		va_start(list_read, a);
		va_copy(list_append, list_read);
		
		while((current = va_arg(list_read, const char*)) != NULL) {
			len += strlen(current);
		}
		va_end(list_read);
		result = (char*) realloc(a, len * sizeof(char));
		if(result == NULL) {
			throw error_errno(errno);
		}
		at = result;
		do {
			size_t current_len = strlen(current);
			memcpy(at, current, current_len * sizeof(char));
			at += current_len;
		} while((current = va_arg(list_append, const char*)) != NULL);
		*at = L'\0';
		va_end(list_append);
		return result;
	}
	
	wchar_t* ijoinstrings(wchar_t* a, ...) {
		wchar_t* result = NULL;
		size_t len = wcslen(a);
		va_list list_read;
		va_list list_append;
		const wchar_t* current;
		wchar_t* at;
		
		va_start(list_read, a);
		va_copy(list_append, list_read);
		
		while((current = va_arg(list_read, const wchar_t*)) != NULL) {
			len += wcslen(current);
		}
		va_end(list_read);
		result = (wchar_t*) realloc(a, len * sizeof(wchar_t));
		if(result == NULL) {
			throw error_errno(errno);
		}
		at = result;
		do {
			size_t current_len = wcslen(current);
			memcpy(at, current, current_len * sizeof(wchar_t));
			at += current_len;
		} while((current = va_arg(list_append, const wchar_t*)) != NULL);
		*at = L'\0';
		va_end(list_append);
		return result;
	}
}

