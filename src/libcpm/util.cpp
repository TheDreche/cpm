/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <string.h>
#include <limits.h>

#include <algorithm>
#include <string>
#include <gmpxx.h>

#include "util.hpp"
#include "errors.hpp"
#include "joinstrings.hpp"
#include "new.hpp"
#include "string.hpp"

namespace cpm {
	char* format(const char* format, ...) {
		va_list va_len;
		va_list va_read;
		va_start(va_len, format);
		va_copy(va_read, va_len);
		
		int len = vsnprintf(NULL, 0, format, va_len);
		va_end(va_len);
		if(len < 0) {
			va_end(va_read);
			throw cpm::error_format();
		}
		
		char* r = (char*) calloc(len + 1, sizeof(char));
		len = vsnprintf(r, len + 1, format, va_read);
		va_end(va_read);
		if(len < 0) {
			throw cpm::error_format();
		}
		return r;
	}
	
	char* vformat(const char* format, va_list& va_read) {
		va_list va_len;
		va_copy(va_len, va_read);
		
		int len = vsnprintf(NULL, 0, format, va_len);
		va_end(va_len);
		if(len < 0) {
			va_end(va_read);
			throw cpm::error_format();
		}
		
		char* r = (char*) calloc(len + 1, sizeof(char));
		len = vsnprintf(r, len + 1, format, va_read);
		va_end(va_read);
		if(len < 0) {
			throw cpm::error_format();
		}
		return r;
	}
	
	bool strprefix(const char* first, const char* second) {
		return strncmp(first, second, strlen(second)) == 0;
	}
	
	bool strprefix_mv(const char*& first, const char *second) {
		size_t len = strlen(second);
		if(memcmp(first, second, len) == 0) {
			first += len;
			return true;
		} else {
			return false;
		}
	}
	
	int_fast8_t strasciicmp(const char *first, const char *second) {
		while(*first != '\0' && *second != '\0') {
			if(*first != *second) {
				if(*first < *second) {
					return -1;
				} else {
					return 1;
				}
			}
			first++;
			second++;
		}
		// At least one of *first and *second is '\0'
		if(*first != *second) {
			if(*first == '\0') {
				return -1;
			} else {
				return 1;
			}
		}
		return 0;
	}
	
	mpz_class strtompz(const char*& str) {
		size_t len = 0;
		const char* at = str;
		while (*at >= '0' && *at <= '9') {
			++at;
			++len;
		}
		if(len == 0) {
			return 0;
		}
		char* s = NULL;
		s = new(cpm::tag) char[len + 1 /* \0 */];
		s[len] = '\0';
		memcpy(s, str, len);
		mpz_class r(s);
		str = at;
		return r;
	}
	
	nullprefixeduint::nullprefixeduint() : zeros(1), value(0) {}
	
	nullprefixeduint::nullprefixeduint(int n) : zeros(n == 0? 1 : 0), value(n) {}
	
	nullprefixeduint::nullprefixeduint(const char*& o) {
		const char* at = o;
		while(*at == '0') {
			++zeros;
			++at;
		}
		size_t len = 0;
		const char* newat = at;
		while(*newat >= '0' && *newat <= '9') {
			++len;
			++newat;
		}
		if(len == 0) {
			value = 0;
		} else {
			char* num = NULL;
			num = new(cpm::tag) char[len + 1 /* \0 */];
			num[len] = '\0';
			memcpy(num, at, len);
			value = num;
			free(num);
		}
		o = newat;
	}
	
	nullprefixeduint& nullprefixeduint::operator=(const char*& o) {
		zeros = 0;
		const char* at = o;
		while(*at == '0') {
			++zeros;
			++at;
		}
		size_t len = 0;
		const char* newat = at;
		while(*newat >= '0' && *newat <= '9') {
			++len;
			++newat;
		}
		if(len == 0) {
			value = 0;
		} else {
			char* num = NULL;
			num = new(cpm::tag) char[len + 1 /* \0 */];
			num[len] = '\0';
			memcpy(num, at, len);
			value = num;
			free(num);
		}
		o = newat;
		return *this;
	}
	
	nullprefixeduint& nullprefixeduint::operator=(int n) {
		if(n == 0) {
			zeros = 1;
			value = 0;
		} else {
			zeros = 0;
			value = n;
		}
		return *this;
	}
	
	nullprefixeduint::operator char*() const {
		int errno_save = errno;
		errno = 0;
		std::string value_str = value.get_str();
		if(zeros > UINT_MAX) {
			errno = errno_save;
			throw error_errno(ERANGE);
		}
		size_t len = zeros.get_ui() + value_str.size();
		char* r = (char*) calloc(len, sizeof(char));
		if(r == NULL) {
			int errno_throw = errno;
			errno = errno_save;
			throw error_errno(errno_throw);
		}
		memset(r, '0', zeros.get_ui());
		r[zeros.get_ui()] = '\0';
		r = ijoinstrings(r, value_str.c_str(), NULL);
		errno = errno_save;
		return r;
	}
	nullprefixeduint::operator string() const {
		return string((char*&&) *this);
	}
	
	bool nullprefixeduint::operator>(const nullprefixeduint& o) const {
		return value > o.value;
	}
	bool nullprefixeduint::operator<(const nullprefixeduint& o) const {
		return value < o.value;
	}
	bool nullprefixeduint::operator==(const nullprefixeduint& o) const {
		return value == o.value;
	}
	bool nullprefixeduint::operator!=(const nullprefixeduint& o) const {
		return value != o.value;
	}
	bool nullprefixeduint::operator>=(const nullprefixeduint& o) const {
		return value >= o.value;
	}
	bool nullprefixeduint::operator<=(const nullprefixeduint& o) const {
		return value <= o.value;
	}
}
