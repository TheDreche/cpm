/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#include <errno.h>
#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif
#include <string.h>

#include <vector>

#include "matchable_expression_any.hpp"
#include "errors.hpp"
#include "string.hpp"

namespace cpm {
	matchable_expression_any& matchable_expression_any::add(const matchable_expression* expr) {
		expressions.push_back(expr);
		return *this;
	}
	matchable_expression_any& matchable_expression_any::add(const matchable_expression_any& expr) {
		expressions.reserve(expressions.size() + expr.expressions.size());
		for(const matchable_expression*const& e : expr.expressions) {
			expressions.push_back(e);
		}
		return *this;
	}
	
	bool matchable_expression_any::matches(const char* to) const {
		for(const matchable_expression* expr : expressions) {
			if(expr->matches(to)) {
				return true;
			}
		}
		return false;
	}
	
	char* matchable_expression_any::repr() const {
		static const char * const prepend = "One of: ";
		static const size_t prepend_len = strlen(prepend);
		size_t len = 0;
		size_t at;
		char* result = NULL;
		std::vector<string> descriptions;
		descriptions.reserve(expressions.size());
		
		for(size_t i = 0; i < expressions.size(); ++i) {
			descriptions.push_back(expressions[i]->repr());
			len += descriptions[i].size();
		}
		// Prepend + space for each expression + len + \0
		result = (char*) calloc(prepend_len + len + expressions.size() + 1, sizeof(char));
		if(result == NULL) {
			throw error_errno(errno);
		}
		memcpy(result, prepend, prepend_len * sizeof(char));
		at = prepend_len;
		for(size_t i = 0; i < expressions.size(); ++i) {
			result[at++] = ' ';
			size_t current_len = strlen(descriptions[i]);
			memcpy(result + at, descriptions[i], current_len * sizeof(char));
			at += current_len;
		}
		result[at] = '\0';
		return result;
	}
}
