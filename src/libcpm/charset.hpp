/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_CHARSET_HPP
#define CPM_CHARSET_HPP

#include <bitset>

namespace cpm {
	class charset;
}

namespace cpm {
	/*!
	 * @brief A unordered collection of characters.
	 * 
	 * Currently not in the public API.
	 */
	class charset {
		private:
			/*!
			 * @brief Saves which characters are inside.
			 * 
			 * For checking whether a unsigned char c is inside, check the c-th
			 * element of this.
			 */
			std::bitset<256> content;
		public:
			/*!
			 * @brief Makes all characters in o part of the set.
			 * 
			 * @param o A c style string containing all needed characters.
			 */
			charset(const char* o);
			
			/*!
			 * @brief Checks whether all characters in a c style string are in this set.
			 * 
			 * @param s The c style string to check.
			 * 
			 * @return A bool whether all characters of s are contained in this set.
			 */
			bool contains(const char* s) const;
			
			/*!
			 * @brief Checks whether a single character is in the set.
			 * 
			 * @param c The character to check.
			 * 
			 * @return A bool whether c is contained in this set.
			 */
			bool contains(char c) const;
	};
}

#endif
