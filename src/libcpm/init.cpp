/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <errno.h>
#include <string.h>

#include <algorithm>
#include <fstream>
#include <functional>
#include <utility>

#include "errors.hpp"
#include "logging.hpp"
#include "parsers/all.hpp"
#include "string.hpp"

namespace {
	class file_stream {
		public:
			FILE* file;
			file_stream(const char* path, const char* mode) {
				file = fopen(path, mode);
				if(file == NULL) {
					throw cpm::error_errno(errno);
				}
			}
			~file_stream() {
				fclose(file);
			}
			operator FILE*() {
				return file;
			}
	};
}

namespace cpm {
	context::operator pass_log() const {
		return LOG_PASS(*this);
	}
	
	configuration::configuration() : configuration_file_parsers(cpm::parsers::all::configuration_file_parsers()) {}
	void configuration::update(context& ctx, configuration &o) {
		log log = LOG_INIT(ctx);
		for(repository* i : o.repositories) {
			for(repository* c : this->repositories) {
				if(*i == *c) {
					abstract<name> redefined_name = i->nameUpstreamShow();
					string redefined_name_str = redefined_name->toCharArray();
					LOG_MESSAGE(log, CONF_WARN, "Redefinition of repository %s\n", (const char*) redefined_name_str);
					goto loopend;
				}
			}
			// Not in there
			repositories.push_back(i->clone());
		loopend:
			continue;
		}
	}
	void configuration::parse(context& ctx, const char* path) {
		int errno_save = errno;
		errno = 0;
		
		log log = LOG_INIT(ctx);
		LOG_MESSAGE(log, DEBUG, "Trying to parse configuration file %s\n", path);
		
		file_stream stream(path, "r");
		bool found = false;
		fpos_t begin;
		configuration best_conf;
		file_parser_correctness best_c;
		
		LOG_MESSAGE(log, DEBUG, "Successfully opened configuration file\n");
		
		if(fgetpos(stream, &begin) == -1) {
			goto error_errno;
		}
		for(file_parser<configuration>& parser : configuration_file_parsers) {
			if(fsetpos(stream, &begin) == -1) {
				goto error_errno;
			}
			configuration current;
			file_parser_correctness corr = parser(stream, current, LOG_PASS(log));
			if(corr.recoverable) {
				// Found something usable
				if(!found) {
					found = true;
				} else {
#define chkshoulduse(a) if(corr.a < best_c.a) {continue;}
					chkshoulduse(syntax_error_count);
					chkshoulduse(overwrite);
					chkshoulduse(used_variables);
#undef chkshoulduse
				}
				best_conf = current;
				best_c = corr;
			}
		}
		if(found) {
			LOG_MESSAGE(log, DEBUG, "Found correct file parser\n");
			this->update(ctx, best_conf);
		} else {
			LOG_MESSAGE(log, WARN, "Could not detect file format\n");
			throw error_file_format();
		}
		errno = errno_save;
		return;
	error_errno:
		int errno_throw = errno;
		errno = errno_save;
		throw error_errno(errno_throw);
	}
}
