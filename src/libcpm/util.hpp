/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_UTIL_HPP
#define CPM_UTIL_HPP

#include <stddef.h>
#include <string.h>

#include <gmpxx.h>

#include "new.hpp"
#include "string.hpp"

namespace cpm {
	/*!
	 * @brief A function for formatting c style strings.
	 * 
	 * Currently not in public API.
	 * 
	 * Formats strings like printf.
	 * 
	 * @param format The format string.
	 * 
	 * @return The formatted c style string.
	 * 
	 * @sa vformat(const char* format, va_list& l)
	 */
	char* format(const char* format, ...);
	
	/*!
	 * @brief A function for formatting c style strings.
	 * 
	 * Currently not in public API.
	 * 
	 * Formats strings like vprintf.
	 * 
	 * @param format The format string.
	 * @param l The va_list.
	 * 
	 * @return The formatted c style string.
	 * 
	 * @sa vformat(const char* format, ...)
	 */
	char* vformat(const char* format, va_list& l);
	
	/*!
	 * @brief A function for comparing the next characters.
	 * 
	 * Currently not in the public API.
	 * 
	 * This returns true if the first c style string given starts
	 * with the second.
	 * 
	 * @param first The string which should start with the second.
	 * @param second The prefix the first string should start with.
	 * 
	 * @return True if first starts with second, false otherwise.
	 */
	bool strprefix(const char *first, const char *second);
	
	/*!
	 * @brief Compare strings and go to end.
	 * 
	 * Similar to @ref strprefix(const char*, const char*)
	 * but moves the first char\* to the end of the prefix if it starts
	 * with it.
	 * 
	 * @param first The string to compare (and move).
	 * @param second The prefix.
	 */
	bool strprefix_mv(const char*& first, const char *second);
	
	/*!
	 * @brief Compares C strings by ASCII.
	 * 
	 * Currently not in the public API.
	 * 
	 * Goes through the whole string and at the first character which
	 * is different from the other string, returns values similar to
	 * c++20 <=>.
	 * 
	 * @param first The first string to compare.
	 * @param second The second string to compare.
	 * @return A value similar to c++20 <=>, determining whichs ASCII
	 *         representation is higher.
	 */
	int_fast8_t strasciicmp(const char* first, const char* second);
	
	/*!
	 * @brief strtoull for mpz_class.
	 * 
	 * Currently not in the public API.
	 * 
	 * Does override the given pointer instead of doing it like strtoull.
	 * 
	 * @param str The string to convert (gets moved).
	 * @return The converted value, or 0 if no valid character was there.
	 */
	mpz_class strtompz(const char*& str);
	
	/*!
	 * @brief An unlimited unsigned 0-prefixed integer.
	 * 
	 * Currently not in the public API.
	 * 
	 * This means a full number which is positive and can be prefixed
	 * with zeros, meaning that `001` is different than `1`.
	 */
	class nullprefixeduint {
		public:
			/*!
			 * @brief The count of zeros prefixing this number.
			 */
			mpz_class zeros = 0;
			
			/*!
			 * @brief The actual value.
			 */
			mpz_class value = 0;
			
			nullprefixeduint();
			
			/*!
			 * @brief Sets this to a specific value.
			 */
			nullprefixeduint(int n);
			
			/*!
			 * @brief Converts a string to a zero-prefixed unsigned integer.
			 */
			nullprefixeduint(const char*& o);
			
			/*!
			 * @brief Converts a string to a zero-prefixed unsigned integer.
			 */
			nullprefixeduint& operator=(const char*& o);
			
			/*!
			 * @brief Sets this to a specific value.
			 */
			nullprefixeduint& operator=(int n);
			
			/*!
			 * @brief Converts this to a C style string.
			 */
			operator char*() const;
			
			/*!
			 * @brief Converts this to a cpm string.
			 */
			operator string() const;
			
			/*!
			 * @brief Returns whether this > o.
			 * 
			 * @param o The other number to compare.
			 * @return Whether this is greater.
			 */
			bool operator>(const nullprefixeduint& o) const;
			
			/*!
			 * @brief Returns whether this < o.
			 * 
			 * @param o The other number to compare.
			 * @return Whether this is smaller.
			 */
			bool operator<(const nullprefixeduint& o) const;
			
			/*!
			 * @brief Whether two unsigned zero-prefixed numbers are the same.
			 * 
			 * Here, 01 == 001. The count of zeros is fully ignored.
			 */
			bool operator==(const nullprefixeduint& o) const;
			
			/*!
			 * @brief Whether two unsigned zero-prefixed numbers aren't the same.
			 * 
			 * Here, 01 == 001. The count of zeros is fully ignored.
			 */
			bool operator!=(const nullprefixeduint& o) const;
			
			/*!
			 * @brief Returns whether this > o or this == o.
			 * 
			 * @param o The other number to compare.
			 * @return Whether this is greater or equal.
			 */
			bool operator>=(const nullprefixeduint& o) const;
			
			/*!
			 * @brief Returns whether this < o or this == o.
			 * 
			 * @param o The other number to compare.
			 * @return Whether this is smaller or equal.
			 */
			bool operator<=(const nullprefixeduint& o) const;
	};
}

#endif
