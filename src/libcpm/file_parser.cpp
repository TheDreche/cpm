/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_STDINT_H
# include <stdint.h>
#endif

#include "cpm/file_parser_correctness.hpp"

namespace cpm {
	bool file_parser_correctness::syntax_error() {
		if(syntax_error_count < UINT8_MAX) {
			++syntax_error_count;
		}
		if(syntax_error_count == UINT8_MAX) {
			return true;
		}
		return false;
	}
	void file_parser_correctness::variable_use() {
		if(used_variables < UINT8_MAX) {
			++used_variables;
		}
	}
}
