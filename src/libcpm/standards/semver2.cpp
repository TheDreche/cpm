/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_STDINT_H
# include <stdint.h>
#endif
#ifdef HAVE_STRING_H
# include <string.h>
#endif

#include "semver2.hpp"
#include "joinstrings.hpp"
#include "util.hpp"
#include "version.hpp"

namespace cpm {
	namespace standards {
		const void* semver2::stdIdentifier() const {
			const static char a = 'a';
			return &a;
		}
		
		bool semver2::operator==(const cpm::version& o) const {
			if(this->stdIdentifier() == o.stdIdentifier()) {
				const semver2* s = (semver2*)&o;
				return this->major == s->major && this->minor == s->minor && this->patch == s->patch;
			} else {
				return false;
			}
		}
		
		bool semver2::operator >(const cpm::version& o) const {
			if(this->stdIdentifier() == o.stdIdentifier()) {
				const semver2* f = this;
				const semver2* s = (semver2*)&o;
				return f->major > s->major? true :
					f->major < s->major? false :
					f->minor > s->minor? true :
					f->minor < s->minor? false :
					f->patch > s->patch? true : false;
			} else {
				return false;
			}
		}
		
		bool semver2::operator <(const cpm::version& o) const {
			if(this->stdIdentifier() == o.stdIdentifier()) {
				const semver2* f = this;
				const semver2* s = (semver2*)&o;
				return f->major < s->major? true :
					f->major > s->major? false :
					f->minor < s->minor? true :
					f->minor > s->minor? false :
					f->patch < s->patch? true : false;
			} else {
				return false;
			}
		}
		
		char* semver2::toCharArray() const {
			if(release == NULL || *release == '\0') {
				if(build == NULL || *build == '\0') {
					return cpm::format("%ju.%ju.%ju", this->major, this->minor, this->patch);
				} else {
					return cpm::format("%ju.%ju.%ju+%s", this->major, this->minor, this->patch, this->build);
				}
			} else {
				if(build == NULL || *build == '\0') {
					return cpm::format("%ju.%ju.%ju-%s", this->major, this->minor, this->patch, this->release);
				} else {
					return cpm::format("%ju.%ju.%ju-%s+%s", this->major, this->minor, this->patch, this->release, this->build);
				}
			}
		}
	}
}
