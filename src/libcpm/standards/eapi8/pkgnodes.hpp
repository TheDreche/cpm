/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_EAPI7_PKGNODES_HPP
#define CPM_EAPI7_PKGNODES_HPP

#include <bitset>
#include <vector>

#include "flags.hpp"
#include "standards/common_pkgnode.hpp"
#include "string.hpp"
#include "names.hpp"
#include "pass_log.hpp"
#include "package.hpp"
#include "pkgnode.hpp"

namespace cpm {
	namespace standards {
		namespace eapi8 {
			class repository;
			class category;
			class package;
			
			class repository_form;
			
			/*!
			 * @brief A form for an eapi8 repository.
			 * 
			 * @sa cpm::standards::eapi8::repository
			 */
			class repository_form {
				public:
					/*!
					 * @brief The special directory for profiles.
					 * 
					 * Should always be profiles.
					 */
					const char* dir_profiles = "profiles";
					
					/*!
					 * @brief The special directory for licenses.
					 * 
					 * Should always be licenses.
					 */
					const char* dir_licenses = "licenses";
					
					/*!
					 * @brief The special directory for eclasses.
					 * 
					 * Should always be eclass.
					 */
					const char* dir_eclass = "eclass";
					
					/*!
					 * @brief The special directory for metadata.
					 * 
					 * Should always be metadata.
					 */
					const char* dir_metadata = "metadata";
					
					/*!
					 * @brief Where the data is stored locally.
					 */
					const char* location;
					
					/*!
					 * @brief The upstream name.
					 */
					repo_name nameUpstream;
					
					/*!
					 * @copybrief cpm::standards::eapi8::repository::strict
					 * 
					 * @copydoc cpm::standards::eapi8::repository::strict
					 */
					bool strict = true;
					
					/*!
					 * @copybrief cpm::standards::eapi8::repository::extensions
					 * 
					 * @copydoc cpm::standards::eapi8::repository::extensions
					 */
					bool extensions = true;
			};
			
			/*!
			 * @brief An eapi8 repository.
			 */
			class repository : public cpm::common_repository {
				public:
					virtual repository* clone() const final override;
					
				private:
					/*!
					 * @brief Address for stdIdentifier.
					 * 
					 * @sa stdIdentifier()const
					 */
					const static bool repoStandard;
					
					/*!
					 * @brief The special directory for profiles.
					 * 
					 * Should always be profiles.
					 */
					string dir_profiles;
					
					/*!
					 * @brief The special directory for licenses.
					 * 
					 * Should always be licenses.
					 */
					string dir_licenses;
					
					/*!
					 * @brief The special directory for eclasses.
					 * 
					 * Should always be eclass.
					 */
					string dir_eclass;
					
					/*!
					 * @brief The special directory for metadata.
					 * 
					 * Should always be metadata.
					 */
					string dir_metadata;
					
					/*!
					 * @brief Anti-alaysing fields.
					 */
					enum fields : uint8_t {
						CHILDREN,
						MAX = CHILDREN
					};
					
					/*!
					 * @brief Whether a @ref fields is evaluated.
					 */
					mutable cpm::flags<fields, fields::MAX> initialized_members;
					
					/*!
					 * @brief The categories in this repository.
					 */
					mutable std::vector<category> children;
					
					/*!
					 * @brief Initialize @ref children.
					 */
					void initialize_children(pass_log&& p) const;
					
					/*!
					 * @brief Checks whether a child directory is a category directory.
					 * 
					 * It implies that n is the name of a subdirectory of @ref local_files.
					 * 
					 * @param n The name to check.
					 * 
					 * @return Whether n is a category directory.
					 */
					bool isCategory(const char* n) const;
				public:
					/*!
					 * @brief Whether to be more standard conform.
					 * 
					 * I plan on adding some extensions to the standard or changing minor
					 * details. When this is true, all behaviours will be like the standard
					 * specifies.
					 * 
					 * Whether plain extensions which aren't specified by the standard
					 * should be used can be specified independently in @ref extensions.
					 * 
					 * 	Thus, both Portage and other package managers can change
					 * 	aspects of their behaviour not defined here without worry
					 * 	of incompatibilities with any particular repository.
					 * 
					 * Currently, there is one behaviour changed if set to false:
					 * 
					 * 	It is not required that a directory exists
					 * 	for each category provided by the repository.
					 * 	A category directory that does not exist
					 * 	shall be considered equivalent to an empty
					 * 	category (and by extension, a package manager
					 * 	may treat an empty category as a category that
					 * 	does not exist).
					 * 
					 * I'm not a big fan of this, as this is not how one
					 * expects it to work:
					 * 
					 * - If you create a directory, it is there. Even if
					 *   it is empty.
					 * - If, e.g. in Python, you define a list:
					 *   
					 *   	a = []
					 *   
					 *   then there is a list, as you can see:
					 *   
					 *   	a.append("abc")
					 *   
					 *   None means it doesn't exist.
					 * 
					 * However, treating everything as empty is also
					 * different:
					 * 
					 * - If you try to save something in a not existing
					 *   folder, you get asked to create it first.
					 *   You wouldn't get asked this if it existed and
					 *   would just be empty.
					 * - If, in Python, you have nothing:
					 *   
					 *   	a = None
					 *   
					 *   you can't append to it:
					 *   
					 *   	a.append("abc") # ERROR
					 * 
					 * So, this is counter-intuitive. It also wastes
					 * some (little) performance, as there has to be
					 * checked every time. Because of these reasons,
					 * I don't want to force strict compliance.
					 * 
					 * @sa extensions
					 */
					bool strict = true;
					
					/*!
					 * @brief Whether to enable extensions.
					 * 
					 * 	Thus, both Portage and other package managers can change
					 * 	aspects of their behaviour not defined here without worry
					 * 	of incompatibilities with any particular repository.
					 * 
					 * This mskes it possible to add extensions. Extensions usually
					 * are additional files getting parsed, containing extra settings.
					 * 
					 * This says whether to enable these.
					 * 
					 * @sa strict
					 */
					bool extensions = true;
					
					/*!
					 * @brief Where the data is stored locally.
					 */
					string local_files;
					
					/*!
					 * @brief Create a repository out of a filled form.
					 */
					repository(const repository_form& f);
					
					~repository();
					
					const void* stdIdentifier() const override;
					
					/*!
					 * @brief Compare 2 repositories.
					 * 
					 * Currently, only the path to the local data is compared.
					 * 
					 * @param other The @ref repository to compare to.
					 * @return True when equal, false otherwise.
					 */
					bool operator==(const repository& other) const;
					bool operator==(const cpm::pkgnode& other) const final override;
					
					std::vector<cpm::pkgnode*> getChildren(pass_log&& p) override;
					
					std::vector<const cpm::pkgnode*> getChildren(pass_log&& p) const override;
					
					std::vector<cpm::package*> getPackages(pass_log&& p) override;
					
					std::vector<const cpm::package*> getPackages(pass_log&& p) const override;
			};
			
			/*!
			 * @brief An eapi8 package category.
			 */
			class category : public cpm::common_pkgnode {
				public:
					virtual category* clone() const final override;
					
				private:
					/*!
					 * @brief Address for stdIdentifier.
					 * 
					 * @sa stdIdentifier()const
					 */
					const static bool categoryStandard;
					
					category(pass_log&& p, const repository* repo, const category_name& n);
					
					/*!
					 * @brief Anti-alaysing fields.
					 */
					enum fields : uint8_t {
						PACKAGES,
						MAX = PACKAGES
					};
					
					/*!
					 * @brief Whether a @ref fields is evaluated.
					 */
					mutable cpm::flags<fields, fields::MAX> initialized_members;
					
					/*!
					 * @brief The packages in this @ref category.
					 */
					mutable std::vector<package> packages;
					
					/*!
					 * @brief Initialize @ref packages.
					 */
					void initialize_packages(pass_log&& p) const;
					
					/*!
					 * @brief Checks whether a child directory is a package directory.
					 * 
					 * It implies that n is the name of a subdirectory of the category
					 * directory (`top->local_files / name`).
					 * 
					 * @param n The name to check.
					 * 
					 * @return Whether n is a package directory.
					 */
					bool isPackage(const char* n) const;
					
					friend repository;
				public:
					~category();
					
					/*!
					 * @brief The repository containing this category.
					 */
					const repository* top;
					
					const void* stdIdentifier() const override;
					
					/*!
					 * @brief Compare 2 categories.
					 * 
					 * Currently, only checks whether the repository is the same and
					 * their upstream name.
					 * 
					 * @param other The @ref category to compare to.
					 * @return True if the same, false otherwise.
					 * 
					 * @sa cpm::standards::eapi8::repository::operator==(const cpm::standards::eapi8::repository& other)const
					 */
					bool operator==(const category& other) const;
					bool operator==(const cpm::pkgnode& other) const final override;
					
					std::vector<cpm::pkgnode*> getChildren(pass_log&& p) override;
					std::vector<const cpm::pkgnode*> getChildren(pass_log&& p) const override;
					
					std::vector<cpm::package*> getPackages(pass_log&& p) override;
					std::vector<const cpm::package*> getPackages(pass_log&& p) const override;
					
					const pkgnode* getParent() const override;
			};
			
			/*!
			 * @brief An eapi8 package.
			 */
			class package : public common_package {
				public:
					virtual package* clone() const final override;
					
				private:
					package(pass_log&& p, const category* c, const package_name& n);
					
					friend category;
				public:
					~package();
					
					/*!
					 * @brief Checks whether an ebuild is a valid ebuild for this package.
					 * 
					 * According to the standard, it has to be named by the scheme
					 * pkgname-version.ebuild. If the package is called `example`, the
					 * name `example-1.5.23_p4.ebuild` is valid.
					 * 
					 * @param n The file name to check.
					 * @return Whether the name is valid for this package.
					 */
					bool isValidEbuildName(const char* n);
					
					/*!
					 * @brief The category containing this package.
					 */
					const category* cat;
					
					const void* stdIdentifier() const override;
			};
		}
	}
}

#endif
