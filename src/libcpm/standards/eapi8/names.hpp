/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_STD_EAPI7_NAMES_HPP
#define CPM_STD_EAPI7_NAMES_HPP

#include "standards/common_name.hpp"

namespace cpm {
	namespace standards {
		namespace eapi8 {
			/*!
			 * @brief Checks whether a category name is valid.
			 * 
			 * Currently not part of the public API.
			 * 
			 * @param s The string to check.
			 */
			bool category_name_check(const char* s);
			
			/*!
			 * @brief Checks whether a package name is valid.
			 * 
			 * Currently not part of the public API.
			 * 
			 * @param s The string to check.
			 */
			bool package_name_check(const char* s);
			
			/*!
			 * @brief Checks whether a slot name is valid.
			 * 
			 * Currently not part of the public API.
			 * 
			 * @param s The string to check.
			 */
			bool slot_name_check(const char* s);
			
			/*!
			 * @brief Checks whether a use flag name is valid.
			 * 
			 * Currently not part of the public API.
			 * 
			 * @param s The string to check.
			 */
			bool use_name_check(const char* s);
			
			/*!
			 * @brief Checks whether a repository name is valid.
			 * 
			 * Currently not part of the public API.
			 * 
			 * @param s The string to check.
			 */
			bool repo_name_check(const char* s);
			
			/*!
			 * @brief Checks whether a license name is valid.
			 * 
			 * Currently not part of the public API.
			 * 
			 * @param s The string to check.
			 */
			bool license_name_check(const char* s);
			
			/*!
			 * @brief Checks whether a branch name is valid.
			 * 
			 * The specification says keyword, but for me the name
			 * `branch` fits much better.
			 * 
			 * Currently not part of the public API.
			 * 
			 * @param s The string to check.
			 */
			bool branch_name_check(const char* s);
			
			/*!
			 * @brief Checks whether a eapi name is valid.
			 * 
			 * Currently not part of the public API.
			 * 
			 * @param s The string to check.
			 */
			bool eapi_name_check(const char* s);
			
			/*!
			 * @brief eapi8 category name standard.
			 * 
			 * Currently not part of the public API.
			 */
			class category_name : public cpm::common_name<&category_name_check> {
				public:
					using cpm::common_name<&category_name_check>::common_name;
					virtual category_name* clone() const final override;
			};
			
			/*!
			 * @brief eapi8 package name standard.
			 * 
			 * Currently not part of the public API.
			 */
			class package_name : public cpm::common_name<&package_name_check> {
				public:
					using cpm::common_name<&package_name_check>::common_name;
					virtual package_name* clone() const final override;
			};
			
			/*!
			 * @brief eapi8 slot name standard.
			 * 
			 * Currently not part of the public API.
			 */
			class slot_name : public cpm::common_name<&slot_name_check> {
				public:
					using cpm::common_name<&slot_name_check>::common_name;
					virtual slot_name* clone() const final override;
			};
			
			/*!
			 * @brief eapi8 use flag name standard.
			 * 
			 * Currently not part of the public API.
			 */
			class use_name : public cpm::common_name<&use_name_check> {
				public:
					using cpm::common_name<&use_name_check>::common_name;
					virtual use_name* clone() const final override;
			};
			
			/*!
			 * @brief eapi8 repository name standard.
			 * 
			 * Currently not part of the public API.
			 */
			class repo_name : public cpm::common_name<&repo_name_check> {
				public:
					using cpm::common_name<&repo_name_check>::common_name;
					virtual repo_name* clone() const final override;
			};
			
			/*!
			 * @brief eapi8 license name standard.
			 * 
			 * Currently not part of the public API.
			 */
			class license_name : public cpm::common_name<&license_name_check> {
				public:
					using cpm::common_name<&license_name_check>::common_name;
					virtual license_name* clone() const final override;
			};
			
			/*!
			 * @brief branch name standard.
			 * 
			 * The specification says keyword, but for me the name
			 * `branch` fits much better.
			 * 
			 * Currently not part of the public API.
			 */
			class branch_name : public cpm::common_name<&branch_name_check> {
				public:
					using cpm::common_name<&branch_name_check>::common_name;
					virtual branch_name* clone() const final override;
			};
			
			/*!
			 * @brief eapi8 eapi name standard.
			 * 
			 * Currently not part of the public API.
			 */
			class eapi_name : public cpm::common_name<&eapi_name_check> {
				public:
					using cpm::common_name<&eapi_name_check>::common_name;
					virtual eapi_name* clone() const final override;
			};
		};
	}
}

#endif
