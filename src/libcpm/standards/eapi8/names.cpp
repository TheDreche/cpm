/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_STDINT_H
# include <stdint.h>
#endif

#include "names.hpp"
#include "charset.hpp"
#include "new.hpp"
#include "standards/quick_name.hpp"
#include "versions.hpp"

namespace cpm {
	namespace standards {
		namespace eapi8 {
			bool category_name_check(const char* s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_.-";
				return quick_name_isValid(s, a, b, 1);
			}
			
			bool package_name_check(const char* s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_-";
				if(!(quick_name_isValid(s, a, b))) {
					return false;
				}
				cpm::standards::eapi8::version_form f;
				for(const char* c = s; *c != '\0'; ++c) {
					if(*c == '-') {
						f.str = c+1;
						try {
							cpm::standards::eapi8::version _ = f;
						} catch(cpm::error_nostdsupport) {
							continue;
						}
						return false;
					}
				}
				return true;
			}
			
			bool slot_name_check(const char* s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_.-";
				return quick_name_isValid(s, a, b, 1);
			}
			
			bool use_name_check(const char* s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_@-";
				return quick_name_isValid(s, a, b, 1);
			}
			
			bool repo_name_check(const char* s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";
				return quick_name_isValid(s, a, b, 1) || !package_name_check(s);
			}
			
			bool license_name_check(const char* s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_.-";
				return quick_name_isValid(s, a, b, 1);
			}
			
			bool branch_name_check(const char* s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-";
				return quick_name_isValid(s, a, b, 1) || !package_name_check(s);
			}
			
			bool eapi_name_check(const char* s) {
				const static cpm::charset a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
				const static cpm::charset b = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+_.-";
				return quick_name_isValid(s, a, b, 1);
			}
			
			//
			// clone() methods
			category_name* category_name::clone() const {
				return new(cpm::tag) category_name(*this);
			}
			
			package_name* package_name::clone() const {
				return new(cpm::tag) package_name(*this);
			}
			
			slot_name* slot_name::clone() const {
				return new(cpm::tag) slot_name(*this);
			}
			
			use_name* use_name::clone() const {
				return new(cpm::tag) use_name(*this);
			}
			
			repo_name* repo_name::clone() const {
				return new(cpm::tag) repo_name(*this);
			}
			
			license_name* license_name::clone() const {
				return new(cpm::tag) license_name(*this);
			}
			
			branch_name* branch_name::clone() const {
				return new(cpm::tag) branch_name(*this);
			}
			
			eapi_name* eapi_name::clone() const {
				return new(cpm::tag) eapi_name(*this);
			}
		}
	}
}
