/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_STD_EAPI7_VERSIONS_HPP
#define CPM_STD_EAPI7_VERSIONS_HPP

#include <vector>

#include <gmpxx.h>

#include "version.hpp"
#include "joinstrings.hpp"
#include "util.hpp"

namespace cpm {
	namespace standards {
		namespace eapi8 {
			class version;
			class version_form;
		}
	}
}

namespace cpm {
	namespace standards {
		/*!
		 * @brief The eapi 8 package manager specification.
		 * 
		 * @sa https://wiki.gentoo.org/wiki/Project:Package_Manager_Specification
		 */
		namespace eapi8 {
			/*!
			 * @brief A form for eapi8 versions.
			 * 
			 * Currently not part of the public API.
			 * 
			 * This can convert a string to a version, as this is possible.
			 */
			class version_form {
				public:
					/*!
					 * @brief The string to convert.
					 * 
					 * If you have a std::string instead of a char*, just
					 * save its .data() here and convert it.
					 */
					const char* str;
			};
			
			/*!
			 * @brief Check an eapi8 version.
			 * 
			 * Currently not part of the public API.
			 * 
			 * @param f The filled form to check.
			 * 
			 * @return Whether the given version_form contains valid data.
			 */
			bool check_version(const version_form& f);
			
			/*!
			 * @brief An eapi8 version.
			 * 
			 * Currently not part of the public API.
			 */
			class version : public cpm::version {
				public:
					/*!
					 * @brief An eapi8 version suffix.
					 * 
					 * The standard allows suffixes. Each one begins with an underscore, followed by a suffix type and a unsigned unlimted number.
					 * 
					 * Suffix types:
					 * - `_alpha`
					 * - `_beta`
					 * - `_pre`
					 * - `_rc`
					 * - `_p`
					 * 
					 * Examples:
					 * - `_rc4`
					 * - `_pre`
					 */
					class suffix {
						public:
							/*!
							 * @brief A suffix type.
							 */
							enum type {
								_ALPHA, ///< Alpha version
								_BETA, ///< Beta version
								_PRE, ///< Pre release
								_RC, ///< Release candidate
								_P ///< Patch
							} str; ///< The type of this suffix
							
							/*!
							 * @brief The number after the suffix type.
							 */
							nullprefixeduint value;
							
							/*!
							 * @brief Whether the suffix has a value.
							 */
							bool hasvalue = false;
							
							/*!
							 * @brief Convert this suffix to a string.
							 */
							operator char*() const {
								return joinstrings(
									str == _ALPHA ? "_alpha" :
									str == _BETA  ? "_beta"  :
									str == _PRE   ? "_pre"   :
									str == _RC    ? "_rc"    :
									str == _P     ? "_p" : "<error>",
									hasvalue? (const char*) (string) (char*) value : "",
									NULL
								);
							}
							
							/*!
							 * @brief Convert this suffix to a @ref cpm::string.
							 */
							operator string() const {
								return string((char*&&) *this);
							}
					};
					
					/*!
					 * @brief The numbers in the number part.
					 *
					 * The package manager specification specifies a version starts with
					 * a number part. It consists out of numbers separated by single dots.
					 * All of these numbers have to be unsigned.
					 * 
					 * This contains these numbers, in order.
					 */
					std::vector<nullprefixeduint> numberpart;
					
					/*!
					 * @brief The version sufixes.
					 * 
					 * The package manager specification specifies that a version can have
					 * sufixes.
					 * 
					 * @sa cpm::standards::eapi8::version::suffix
					 */
					std::vector<suffix> sufixes;
					
					/*!
					 * @brief The single letter.
					 * 
					 * The package manager specification specifies there can be a
					 * single letter between the number part and the sufixes. It
					 * is saved here.
					 * 
					 * If here the null character is saved, there is no character.
					 */
					char c = '\0';
					
					/*!
					 * @brief The revision number.
					 * 
					 * The package manager specification specifies that after the sufixes,
					 * `-r<number>` can be added to the end. If none is given, 0 is assumed.
					 */
					mpz_class revision = 0;
					
					/*!
					 * @brief Saves whether originally there was a revision number.
					 */
					bool hasrevision = false;
					
					/*!
					 * @brief Default constructor.
					 * 
					 * Initializes invalid version.
					 */
					version();
					
					/*!
					 * @brief Converts a filled version form to an actual version.
					 * 
					 * @sa version_form
					 */
					version(const version_form f);
					
					virtual version* clone() const final override;
					
					const void* stdIdentifier() const final override;
					
					/*!
					 * @brief Converts this version to a string.
					 * 
					 * The memory has to be freed by the caller.
					 */
					operator char*() const;
					bool operator> (const cpm::version& o) const final override;
					bool operator==(const cpm::version& o) const final override;
					bool operator< (const cpm::version& o) const final override;
			};
		}
	}
}


#endif
