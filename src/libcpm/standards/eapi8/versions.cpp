/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#ifdef HAVE_STDINT_H
# include <stdint.h>
#endif

#include <gmpxx.h>

#include "versions.hpp"
#include "charset.hpp"
#include "errors.hpp"
#include "new.hpp"
#include "joinstrings.hpp"
#include "util.hpp"

namespace {
	enum result : uint_fast8_t {
		SMALLER,
		EQUAL,
		GREATER
	};
	
	// All of the following algorithms are translated directly from the specification
	result alg7(const cpm::standards::eapi8::version* first, const cpm::standards::eapi8::version* second) {
		mpz_class ar = first->hasrevision? first->revision : 0;
		mpz_class br = second->hasrevision? second->revision : 0;
		if(ar > br) {
			return GREATER;
		} else if(ar < br) {
			return SMALLER;
		}
		return EQUAL;
	}
	result alg6(const cpm::standards::eapi8::version::suffix& a, const cpm::standards::eapi8::version::suffix& b) {
		if(a.str == b.str) {
			cpm::nullprefixeduint asi = a.hasvalue? a.value : 0;
			cpm::nullprefixeduint bsi = b.hasvalue? b.value : 0;
			if(asi > bsi) {
				return GREATER;
			} else if(asi < bsi) {
				return SMALLER;
			}
		} else if(a.str > b.str) {
			return GREATER;
		} else if(a.str < b.str) {
			return SMALLER;
		}
		return EQUAL;
	}
	result alg5(const cpm::standards::eapi8::version* first, const cpm::standards::eapi8::version* second) {
		size_t asn = first->sufixes.size();
		size_t bsn = second->sufixes.size();
		result state;
		for(size_t i = 0; i < asn && i < bsn; ++i) {
			if((state = alg6(first->sufixes[i], second->sufixes[i])) != EQUAL) {
				return state;
			}
		}
		if(asn > bsn) {
			if(first->sufixes.back().str == cpm::standards::eapi8::version::suffix::_P) {
				return GREATER;
			} else {
				return SMALLER;
			}
		} else if(asn < bsn) {
			if(second->sufixes.back().str == cpm::standards::eapi8::version::suffix::_P) {
				return SMALLER;
			} else {
				return GREATER;
			}
		}
		return EQUAL;
	}
	result alg4(const cpm::standards::eapi8::version* first, const cpm::standards::eapi8::version* second) {
		if(first->c > second->c) {
			return GREATER;
		} else if(first->c < second->c) {
			return SMALLER;
		}
		return EQUAL;
	}
	result alg3(const cpm::nullprefixeduint& f, const cpm::nullprefixeduint& o) {
		if(f.zeros > 0 || o.zeros > 0) {
			cpm::nullprefixeduint a = f;
			cpm::nullprefixeduint b = o;
			while(a.value % 10 == 0) {
				a.value /= 10;
			}
			while(b.value % 10 == 0) {
				b.value /= 10;
			}
			cpm::string s = a;
			cpm::string t = b;
			
			int_fast8_t c = cpm::strasciicmp(s, t);
			
			if(c > 0) {
				return GREATER;
			} else if(c < 0) {
				return SMALLER;
			}
		} else {
			if(f > o) {
				return GREATER;
			} else if(f < o) {
				return SMALLER;
			}
		}
		return EQUAL;
	}
	result alg2(const cpm::standards::eapi8::version* first, const cpm::standards::eapi8::version* second) {
		if(first->numberpart[0] > second->numberpart[0]) {
			return GREATER;
		} else if(first->numberpart[0] < second->numberpart[0]) {
			return SMALLER;
		}
		size_t ann = first->numberpart.size();
		size_t bnn = second->numberpart.size();
		result state;
		for(size_t i = 1; i < ann && i < bnn; ++i) {
			if((state = alg3(first->numberpart[i], second->numberpart[i])) != EQUAL) {
				return state;
			}
		}
		if(ann > bnn) {
			return GREATER;
		} else if(ann < bnn) {
			return SMALLER;
		}
		return EQUAL;
	}
	result alg1(const cpm::standards::eapi8::version* first, const cpm::standards::eapi8::version* second) {
		result state = EQUAL;
#define alg_(n) if((state = alg ## n (first, second)) != EQUAL) {return state;}
		alg_(2);
		alg_(4);
		alg_(5);
		alg_(7);
#undef alg_
		return EQUAL;
	}
}

namespace cpm {
	namespace standards {
		namespace eapi8 {
			bool check_version(const version_form& f) {
				const static charset digits = "0123456789";
				const char* at = f.str;
				bool needNumber = true;
				// Number part
				while(digits.contains(*at) && *at != '\0') {
					++at;
					needNumber = false;
					if(*at == '.') {
						++at;
						needNumber = true;
					}
				}
				if(needNumber) {
					return false;
				}
				// Letter
				const static charset alphabet = "abcdefghijklmnopqrstuvwxyz";
				if(alphabet.contains(*at) && *at != '\0') {
					++at;
				}
				// suffixes
				while(*at == '_') {
					switch(*++at) {
						case 'a':
							if(!strprefix_mv(at, "alpha")) {
								return false;
							}
							break;
						case 'b':
							if(!strprefix_mv(at, "beta")) {
								return false;
							}
							break;
						case 'p':
							if(!strprefix_mv(at, "pre")) {
								// p alone is a valid suffix
								++at;
							}
							break;
						case 'r':
							if(!strprefix_mv(at, "rc")) {
								return false;
							}
							break;
						default:
							return false;
					}
					while(digits.contains(*at) && *at != '\0') {
						++at;
					}
				}
				// -r<number>
				if(strprefix_mv(at, "-r")) {
					if(!digits.contains(*at) || *at == '\0') {
						return false;
					}
					while(digits.contains(*at) && *at != '\0') {
						++at;
					}
				}
				return *at == '\0';
			}
			
			version::version() {}
			
			version::version(const version_form f) {
				const char* at = f.str;
				while(true) {
					const char* atbefore = at;
					numberpart.emplace_back(at);
					if(at == atbefore) {
						// Numberpart ends in . or doesn't exist.
						// Not allowed by the specification.
						throw error_nostdsupport();
					}
					if(*at == '.') {
						++at;
					} else {
						break;
					}
				}
				const static charset allowed_chars = "abcdefghijklmnopqrstuvwxyz";
				if(allowed_chars.contains(*at) && *at != '\0') {
					c = *at;
					++at;
				} else {
					c = '\0';
				}
				while(*at == '_') {
					suffix s;
					switch(*++at) {
						case 'a':
							if(strprefix_mv(at, "alpha")) {
								s.str = suffix::_ALPHA;
							} else {
								throw error_nostdsupport();
							}
							break;
						case 'b':
							if(strprefix_mv(at, "beta")) {
								s.str = suffix::_BETA;
							} else {
								throw error_nostdsupport();
							}
							break;
						case 'p':
							if(strprefix_mv(at, "pre")) {
								s.str = suffix::_PRE;
							} else {
								// _p alone is a valid suffix
								++at;
								s.str = suffix::_P;
							}
							break;
						case 'r':
							if(strprefix_mv(at, "rc")) {
								s.str = suffix::_RC;
							} else {
								throw error_nostdsupport();
							}
							break;
						default:
							throw error_nostdsupport();
					}
					const char* atbefore = at;
					s.value = at;
					if(at == atbefore) {
						s.hasvalue = false;
						s.value = 0;
					} else {
						s.hasvalue = true;
					}
					sufixes.emplace_back(std::move(s));
				}
				hasrevision = false;
				if(*at == '-') {
					if(*++at == 'r') {
						++at;
						hasrevision = true;
						revision = strtompz(at);
					}
				}
				if(*at != '\0') {
					throw error_nostdsupport();
				}
			}
			
			version* version::clone() const {
				return new(cpm::tag) version(*this);
			}
			
			const void* version::stdIdentifier() const {
				static bool a;
				return &a;
			};
			
			version::operator char*() const {
				string r;
				for(size_t i = 0; i < numberpart.size(); ++i) {
					string add = numberpart[i];
					r += add;
					if(i + 1 < numberpart.size()) {
						r += ".";
					}
				}
				char c_add[2] = {'\0', '\0'};
				c_add[0] = c;
				r += c_add;
				for(const suffix& i : sufixes) {
					r += i;
				}
				if(hasrevision) {
					r += joinstrings("-r", revision.get_str().c_str(), NULL);
				}
				return r;
			}
			
			bool version::operator< (const cpm::version& o) const {
				if(stdIdentifier() != o.stdIdentifier()) {
					return false;
				}
				return alg1(this, (eapi8::version*)&o) == SMALLER;
			}
			
			bool version::operator==(const cpm::version& o) const {
				if(stdIdentifier() != o.stdIdentifier()) {
					return false;
				}
				return alg1(this, (eapi8::version*)&o) == EQUAL;
			}
			
			bool version::operator> (const cpm::version& o) const {
				if(stdIdentifier() != o.stdIdentifier()) {
					return false;
				}
				return alg1(this, (eapi8::version*)&o) == GREATER;
			}
		}
	}
}
