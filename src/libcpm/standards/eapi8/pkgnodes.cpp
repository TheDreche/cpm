/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "config.hpp"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#ifdef HAVE_STDLIB_H
# include <stdlib.h>
#endif
#ifdef HAVE_SYS_STAT_H
# include <sys/stat.h>
#endif
#ifdef HAVE_SYS_TYPES_H
# include <sys/types.h>
#endif

#include "pkgnodes.hpp"
#include "joinstrings.hpp"
#include "logging.hpp"
#include "names.hpp"
#include "new.hpp"
#include "standards/common_name.hpp" // common_name_form
#include "standards/common_pkgnode.hpp"
#include "util.hpp"

#include "versions.hpp"

namespace {
	// value is never read, just address
	const static bool eapi8addr = false;
	
	class autoclose_dir {
		public:
			DIR* self = NULL;
			autoclose_dir() {}
			autoclose_dir(const char* path) {
				int errno_save = errno;
				errno = 0;
				self = opendir(path);
				int errno_throw = errno;
				errno = errno_save;
				if(self == NULL) {
					throw cpm::error_errno(errno_throw);
				}
			}
			autoclose_dir(DIR* self) : self(self) {}
			~autoclose_dir() {
				int errno_save = errno;
				if(self != NULL) {
					closedir(self);
				}
				errno = errno_save;
			}
			autoclose_dir& operator=(const char* path) {
				int errno_save = errno;
				if(self != NULL) {
					closedir(self);
					self = NULL;
				}
				errno = 0;
				self = opendir(path);
				int errno_throw = errno;
				errno = errno_save;
				if(self == NULL) {
					throw cpm::error_errno(errno_throw);
				}
				return *this;
			}
			autoclose_dir& operator=(DIR* new_self) {
				int errno_save = errno;
				if(self != NULL) {
					closedir(self);
					self = NULL;
				}
				self = new_self;
				if(self == NULL) {
					throw cpm::error_errno(errno_save);
				}
				errno = errno_save;
				
				return *this;
			}
			operator DIR*() {
				return self;
			}
	};
	
	class strict_empty_error {};
}

namespace cpm {
	namespace standards {
		namespace eapi8 {
			const bool repository::repoStandard = false;
			repository* repository::clone() const {
				return new(cpm::tag) repository(*this);
			};
			void repository::initialize_children(pass_log&& p) const {
				if(!initialized_members[CHILDREN]){
					log log = LOG_INIT(p);
					int errno_save = errno;
					errno = 0;
					size_t local_files_len = strlen(local_files);
					autoclose_dir dir = (const char*) local_files;
					dirent* entry;
					struct stat s;
					while((entry = readdir(dir)) != NULL) {
						bool isDir = false;
						if(entry->d_type == DT_UNKNOWN) {
							string p = joinstrings(local_files, "/", entry->d_name, NULL);
							if(stat(p, &s) != 0) {
								auto errno_tmp = errno;
								errno = errno_save;
								throw error_errno(errno_tmp);
							}
							isDir = S_ISDIR(s.st_mode);
						} else {
							isDir = entry->d_type == DT_DIR;
						}
						if(isDir && isCategory(entry->d_name)) {
							try {
								children.push_back(category(LOG_PASS(log), this, (const char*) entry->d_name));
							} catch(strict_empty_error) {}
						}
					}
					errno = errno_save;
				}
			};
			bool repository::isCategory(const char* n) const {
				if(strlen(n) == 0) {
					return false;
				} else if(n[0] == '.') {
					return false;
				} else if(strcmp(n, dir_profiles) == 0) {
					return false;
				} else if(strcmp(n, dir_licenses) == 0) {
					return false;
				} else if(strcmp(n, dir_eclass) == 0) {
					return false;
				} else if(strcmp(n, dir_metadata) == 0) {
					return false;
				} else {
					return true;
				}
			}
			repository::repository(const repository_form& form) :
				cpm::common_repository(&form.nameUpstream),
				dir_profiles(form.dir_profiles),
				dir_licenses(form.dir_licenses),
				dir_eclass(form.dir_eclass),
				dir_metadata(form.dir_metadata),
				local_files(form.location),
				strict(form.strict),
				extensions(form.extensions)
			{
				// Remove trailing slash
				if(local_files.back() == '/') {
					local_files.pop_back();
				}
			}
			repository::~repository() {}
			const void* repository::stdIdentifier() const {
				return &repoStandard;
			}
			bool repository::operator==(const pkgnode& other) const {
				if(stdIdentifier() == other.stdIdentifier()) {
					return *this == *(repository*)&other;
				} else {
					return false;
				}
			}
			bool repository::operator==(const repository& other) const {
				return this->local_files == other.local_files;
			}
			std::vector<cpm::pkgnode*> repository::getChildren(pass_log&& p) {
				log log = LOG_INIT(p);
				initialize_children(LOG_PASS(log));
				std::vector<cpm::pkgnode*> r;
				r.reserve(children.size());
				for(category& i : children) {
					r.push_back(&i);
				}
				return r;
			}
			std::vector<const cpm::pkgnode*> repository::getChildren(pass_log&& p) const {
				log log = LOG_INIT(p);
				initialize_children(LOG_PASS(log));
				std::vector<const cpm::pkgnode*> r;
				r.reserve(children.size());
				for(const category& i : children) {
					r.push_back(&i);
				}
				return r;
			}
			std::vector<cpm::package*> repository::getPackages(pass_log&& p) {
				return std::vector<cpm::package*>();
			}
			std::vector<const cpm::package*> repository::getPackages(pass_log&& p) const {
				return std::vector<const cpm::package*>();
			}
			
			category* category::clone() const {
				return new(cpm::tag) category(*this);
			}
			const bool category::categoryStandard = false;
			category::~category() {}
			void category::initialize_packages(pass_log&& p) const {
				if(!initialized_members[PACKAGES]){
					log log = LOG_INIT(p);
					int errno_save = errno;
					errno = 0;
					string myname = nameUpstream->toCharArray();
					string local_files = joinstrings(
						top->local_files,
						"/",
						(const char*) myname,
						NULL
					);
					size_t local_files_len = strlen(local_files);
					autoclose_dir dir = (const char*) local_files;
					dirent* entry;
					struct stat s;
					while((entry = readdir(dir)) != NULL) {
						bool isDir = false;
						if(entry->d_type == DT_UNKNOWN) {
							string p = joinstrings(local_files, "/", entry->d_name, NULL);
							if(stat(p, &s) != 0) {
								auto errno_tmp = errno;
								errno = errno_save;
								throw error_errno(errno_tmp);
							}
							isDir = S_ISDIR(s.st_mode);
						} else {
							isDir = entry->d_type == DT_DIR;
						}
						if(isDir && isPackage(entry->d_name)) {
							try {
								packages.push_back(package(LOG_PASS(log), this, (const char*) entry->d_name));
							} catch(strict_empty_error) {}
						}
					}
					errno = errno_save;
				}
			};
			bool category::isPackage(const char* n) const {
				if(strlen(n) == 0) {
					return false;
				} else if(n[0] == '.') {
					return false;
				} else if(strcmp(n, "CVS") == 0) {
					return false;
				} else {
					return true;
				}
			}
			category::category(pass_log&& p, const repository* repo, const category_name& n) : top(repo), common_pkgnode(&n) {
				if(top->strict) {
					log log = LOG_INIT(p);
					LOG_MESSAGE(log, DEBUG, "Category %s::%s\n", (const char*) (string) nameUpstream->toCharArray(), (const char*) (string) top->nameUpstream->toCharArray());
					int errno_save = errno;
					errno = 0;
					// Empty categories don't exist in strict mode
					string my_path = joinstrings(top->local_files, "/", (const char*) (string) nameUpstream->toCharArray(), NULL);
					autoclose_dir d = (const char*) my_path;
					dirent* entry;
					bool valid = false;
					while((entry = readdir(d)) != NULL) {
						bool isDir = false;
						if(entry->d_type == DT_UNKNOWN) {
							string p = joinstrings(my_path, "/", entry->d_name, NULL);
							struct stat s;
							if(stat(p, &s) != 0) {
								auto errno_tmp = errno;
								errno = errno_save;
								throw error_errno(errno_tmp);
							}
							isDir = S_ISDIR(s.st_mode);
						} else {
							isDir = entry->d_type == DT_DIR;
						}
						if(isDir) {
							common_name_form _form = {
								.name = entry->d_name
							};
							try {
								package_name _name = _form;
								try {
									package _pkg(LOG_PASS(log), this, _name);
								} catch(strict_empty_error) {
									continue;
								}
							} catch(error_nostdsupport) {
								continue;
							}
							valid = true;
							break;
						}
					}
					errno = errno_save;
					if(!valid) {
						throw strict_empty_error();
					}
				}
			}
			const void* category::stdIdentifier() const {
				return &categoryStandard;
			}
			bool category::operator==(const cpm::pkgnode& other) const {
				if(this->stdIdentifier() == other.stdIdentifier()) {
					return *this == *(category*)&other;
				} else {
					return false;
				}
			}
			bool category::operator==(const category& other) const {
				return *this->top == *other.top && this->nameUpstream == other.nameUpstream;
			}
			std::vector<cpm::pkgnode*> category::getChildren(pass_log&& p) {
				return std::vector<cpm::pkgnode*>();
			}
			std::vector<const cpm::pkgnode*> category::getChildren(pass_log&& p) const {
				return std::vector<const cpm::pkgnode*>();
			}
			std::vector<cpm::package*> category::getPackages(pass_log&& p) {
				log log = LOG_INIT(p);
				initialize_packages(LOG_PASS(log));
				std::vector<cpm::package*> r;
				r.reserve(packages.size());
				for(package& p : packages) {
					r.push_back((cpm::package*)&p);
				}
				return r;
			}
			std::vector<const cpm::package*> category::getPackages(pass_log&& p) const {
				log log = LOG_INIT(p);
				initialize_packages(LOG_PASS(log));
				std::vector<const cpm::package*> r;
				r.reserve(packages.size());
				for(package& p : packages) {
					r.push_back((const cpm::package*)&p);
				}
				return r;
			}
			const pkgnode* category::getParent() const {
				return top;
			}
			
			// package
			bool package::isValidEbuildName(const char* n) {
				string myname = nameUpstream->toCharArray();
				size_t n_len = strlen(n);
				// First check whether it actually ends in `.ebuild`
				if(n_len < myname.size() + 1 /* - */ + 7 /* .ebuild */) {
					// Name is too short to be able to be valid
					return false;
				}
				if(strcmp(n + n_len - 7, ".ebuild") != 0) {
					return false;
				}
				if(!strprefix(n, myname)) {
					return false;
				}
				const char* check_at = n + myname.size();
				if(*check_at != '-') {
					return false;
				}
				++check_at;
				size_t version_len = ((n + n_len) - check_at) - 7 /* .ebuild */;
				string version_str(check_at, version_len);
				version_form f = {
					.str = version_str
				};
				return check_version(f);
			}
			package* package::clone() const {
				return new(cpm::tag) package(*this);
			}
			package::package(pass_log&& p, const category* c, const package_name& n) : cat(c), common_package(&n) {
				if(cat->top->strict) {
					log log = LOG_INIT(p);
					int errno_save = errno;
					errno = 0;
					// packages without versions don't exist in strict mode
					string my_path = joinstrings(cat->top->local_files, "/", (const char*) (string) cat->nameUpstream->toCharArray(), "/", (const char*) (string) nameUpstream->toCharArray(), NULL);
					autoclose_dir dir = opendir(my_path);
					dirent* entry;
					bool valid = false;
					while((entry = readdir(dir)) != NULL) {
						bool isFile = false;
						if(entry->d_type == DT_UNKNOWN) {
							string p = joinstrings(my_path, "/", entry->d_name);
							struct stat s;
							if(stat(p, &s) != 0) {
								auto errno_tmp = errno;
								errno = errno_save;
								throw error_errno(errno_tmp);
							}
							isFile = S_ISREG(s.st_mode) || S_ISLNK(s.st_mode);
						} else {
							isFile = entry->d_type == DT_REG || entry->d_type == DT_LNK;
						}
						if(isFile && isValidEbuildName(entry->d_name)) {
							valid = true;
							break;
						}
					}
					if(!valid) {
						LOG_MESSAGE(log, UPSTREAM_WARN,
							"(strict mode) Invalid package directory %s%s%s%s%s\n",
							(const char*) cat->top->local_files,
							"/",
							(const char*) (string) cat->nameUpstream->toCharArray(),
							"/",
							(const char*) (string) nameUpstream->toCharArray());
						throw strict_empty_error();
					}
					errno = errno_save;
				}
			}
			package::~package() {}
			const void* package::stdIdentifier() const {
				return &eapi8addr;
			}
		}
	}
}
