/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_SEMVER2_HPP
#define CPM_SEMVER2_HPP

#include "config.hpp"

#include <stdint.h>

#include "version.hpp"

namespace cpm {
	namespace standards {
		class semver2;
		
		/*!
		 * @brief A container which contains the data for a semantic version 2.
		 * 
		 * Just used internally, **DO NOT USE ELSEWHERE**!
		 * 
		 * @sa https://semver.org
		 */
		class semver2 : public version {
			public:
				/*!
				 * @brief The major version.
				 * 
				 * @sa https://semver.org
				 */
				uintmax_t major;
				
				/*!
				 * @brief The minor version.
				 * 
				 * @sa https://semver.org
				 */
				uintmax_t minor;
				
				/*!
				 * @brief The patch version.
				 * 
				 * @sa https://semver.org
				 */
				uintmax_t patch;
				
				/*!
				 * @brief The release information.
				 * 
				 * If empty, none is there.
				 * 
				 * @sa https://semver.org
				 */
				const char* release = "";
				
				/*!
				 * @brief The build information.
				 * 
				 * If empty, none is there.
				 * 
				 * @sa https://semver.org
				 */
				const char* build = "";
				
				virtual const void* stdIdentifier() const override;
				
				virtual bool operator==(const cpm::version& o) const override;
				
				virtual bool operator >(const cpm::version& o) const override;
				
				virtual bool operator <(const cpm::version& o) const override;
				
				/*!
				 * @brief Convert to a human-readable string.
				 * 
				 * Should be used for output.
				 */
				char* toCharArray() const;
		};
	}
}

#endif
