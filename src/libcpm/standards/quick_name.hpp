/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_STD_QUICK_NAME_HPP
#define CPM_STD_QUICK_NAME_HPP

#include <errno.h>
#include <string.h>

#include "charset.hpp"

namespace cpm {
	/*!
	 * @brief A common checking function.
	 * 
	 * This is based upon a common concept:
	 * To be valid,
	 * - the first character has to be in a set of characters
	 * - and the second one has to be in another set of characters,
	 *   usually containing more characters.
	 * 
	 * This also has an optional parameter, specifying the minimum length.
	 * 
	 * This is actually a utility function. To use it, return the result of this in the
	 * check() function of your name standard class. In order to make it faster, you
	 * may make all parameters static variables.
	 * 
	 * 	bool check(const char* s) {
	 * 		const static charset f = "abc...";
	 * 		const static charset o = "abc...";
	 * 		return quick_name_isValid(s, f, o, 1);
	 * 	}
	 * 
	 * @param s The string to check.
	 * @param first The @ref charset in which the first character should be.
	 * @param other The @ref charset in which all except the first character should be.
	 * @param minlen The minimum length of a name. 1 by default (to forbid empty names).
	 */
	bool quick_name_isValid(const char* s, const cpm::charset first, const cpm::charset other, uint_fast8_t minlen = 1);
}

#endif
