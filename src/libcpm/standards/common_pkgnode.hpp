/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CPM_COMMON_PKGNODE_HPP
#define CPM_COMMON_PKGNODE_HPP

#include "abstract.hpp"
#include "name.hpp"
#include "repository.hpp"
#include "package.hpp"
#include "pkgnode.hpp"

namespace cpm {
	/*!
	 * @brief A common repository standard.
	 * 
	 * It implements some commonly used things.
	 * 
	 * @sa cpm::common_pkgnode
	 */
	class common_repository : public repository {
		public:
			/*!
			 * @brief The upstream name of this pkgnode.
			 */
			abstract<name> nameUpstream;
			
			/*!
			 * @brief Construct a repository with a name.
			 * 
			 * @param nameUpstream The upstream name.
			 */
			common_repository(const name* nameUpstream);
			
			virtual common_repository* clone() const override = 0;
			
			virtual name* nameUpstreamShow(name* called = NULL) const final override;
			
			virtual char* nameMatchUpstreamShow() const final override;
	};
	
	/*!
	 * @brief A common pkgnode standard.
	 * 
	 * It implements some commonly used things.
	 * 
	 * @sa cpm::common_repository
	 */
	class common_pkgnode : public pkgnode {
		public:
			/*!
			 * @brief The upstream name of this pkgnode.
			 */
			abstract<name> nameUpstream;
			
			/*!
			 * @brief Construct a pkgnode with a name.
			 * 
			 * @param nameUpstream The upstream name.
			 */
			common_pkgnode(const name* nameUpstream);
			
			virtual common_pkgnode* clone() const override = 0;
			
			virtual name* nameUpstreamShow(name* called = NULL) const final override;
			
			virtual char* nameMatchUpstreamShow() const final override;
	};
	
	/*!
	 * @brief A common package standard.
	 * 
	 * It implements some commonly used things.
	 * 
	 * @sa cpm::common_repository
	 */
	class common_package : public cpm::package {
		public:
			/*!
			 * @brief The upstream name of this package.
			 */
			abstract<name> nameUpstream;
			
			/*!
			 * @brief Construct a package with a name.
			 * 
			 * @param nameUpstream The upstream name.
			 */
			common_package(const name* nameUpstream);
			
			virtual common_package* clone() const override = 0;
			
			virtual name* nameUpstreamShow(name* called = NULL) const final override;
			
			virtual char* nameMatchUpstreamShow() const final override;
	};
}

#endif
