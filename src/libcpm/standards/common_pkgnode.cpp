/* Copyright (C) 2021 Dreche
 * 
 * This file is part of cpm.
 * 
 * cpm is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * cpm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with cpm.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common_pkgnode.hpp"
#include "name.hpp"

namespace cpm {
	common_repository::common_repository(const name* nameUpstream) : nameUpstream(nameUpstream->clone()) {}
	name* common_repository::nameUpstreamShow(name* called) const {
		return nameUpstream->clone();
	}
	char* common_repository::nameMatchUpstreamShow() const {
		return nameUpstream->toCharArray();
	}
	
	common_pkgnode::common_pkgnode(const name* nameUpstream) : nameUpstream(nameUpstream->clone()) {}
	name* common_pkgnode::nameUpstreamShow(name* called) const {
		return nameUpstream->clone();
	}
	char* common_pkgnode::nameMatchUpstreamShow() const {
		return nameUpstream->toCharArray();
	}
	
	common_package::common_package(const name* nameUpstream) : nameUpstream(nameUpstream->clone()) {}
	name* common_package::nameUpstreamShow(name* called) const {
		return nameUpstream->clone();
	}
	char* common_package::nameMatchUpstreamShow() const {
		return nameUpstream->toCharArray();
	}
}
